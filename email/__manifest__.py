# -*- coding: utf-8 -*-
{
    'name': "Gửi Email",

    'summary': """Gửi thay đổi chuyển tiền""",

    'description': """
        Long description of module's purpose
    """,

    'author': "kantan JSC",
    'website': "http://www.yourcompany.com",
    'sequence': 1,
    'category': 'email',
    'version': '0.1',

    'depends': ['base', 'mail', 'contacts'],

    'data': [
        'security/user_groups.xml',
        'security/ir.model.access.csv',
        'views/trading.xml',
        'views/report.xml',
        'views/sequence.xml',
        # 'views/rate.xml',
        'views/temp_mail.xml',
        'static/xml/templates.xml',
        'data/data.xml',
        # 'report/report.xml',
    ],
    'installable': True,
    'auto_install': True,
    'application': True
}
