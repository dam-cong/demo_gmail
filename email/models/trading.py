# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import datetime
from . import funtion


class ExchangeRate(models.Model):
    _name = 'email.rate'
    _rec_name = 'exchange_rate'
    _order = 'id desc'

    exchange_rate = fields.Float(string='Tỷ giá JPY-VND', required=True)
    rate_date = fields.Datetime(string='Ngày tháng', required=True, default=lambda *a: datetime.now())
    rate_note = fields.Text(string="Ghi chú")


class Email(models.Model):
    _name = 'email.email'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _rec_name = 'trading_code'
    _description = 'Giao dịch'
    _order = 'id desc'

    def _balance_after_transfers_japan(self):
        email = self.env['email.email'].search([('status', '=', True)])
        sum_receive_money_japan = 0
        sum_transfers_japan = 0
        if email:
            for item in email:
                sum_receive_money_japan += item.receive_money_japan
                sum_transfers_japan += item.transfers_japan
        return float(sum_receive_money_japan - sum_transfers_japan)

    def _balance_after_transfers_vietnam(self):
        email = self.env['email.email'].search([('status', '=', True)])
        sum_receive_money_vietnam = 0
        sum_transfers_vietnam = 0
        if email:
            for item1 in email:
                sum_receive_money_vietnam += item1.receive_money_vietnam
                sum_transfers_vietnam += item1.transfers_vietnam
        return float(sum_receive_money_vietnam - sum_transfers_vietnam)

    trading_code = fields.Char(string='Mã giao dịch')
    status = fields.Boolean(string='Hủy bỏ giao dịch', default='True', track_visibility='onchange')
    traiding_exchange_rate = fields.Float(string='Tỷ giá JPY-VND', track_visibility='onchange')
    source_money = fields.Boolean(string='Tiền nguồn', track_visibility='onchange')
    trading_date = fields.Date(string='Ngày giao dịch (Ngày)', required=True, default=lambda *a: datetime.now().date(),
                               track_visibility='onchange')
    receive_money_japan = fields.Integer(string='Tiền Yên nhận về (Yên)', track_visibility='onchange')
    receive_money_japan_officer = fields.Many2one(comodel_name='res.users', string='Cán bộ')
    transfers_japan = fields.Integer(string='Tiền Yên chuyển đi (Yên)', track_visibility='onchange')
    transfers_japan_officer = fields.Many2one(comodel_name='res.users', string='Cán bộ')
    receive_money_vietnam = fields.Integer(string='Tiền Việt nhận về (VND)', digits=(15, 0),
                                           track_visibility='onchange')
    receive_money_vietnam_officer = fields.Many2one(comodel_name='res.users', string='Cán bộ')
    transfers_vietnam = fields.Integer(string='Tiền Việt chuyển đi (VND)', track_visibility='onchange')
    transfers_vietnam_officer = fields.Many2one(comodel_name='res.users', string='Cán bộ')
    balance_after_transfers_japan = fields.Float(string='Số dư sau giao dịch (Yên)', store=True,
                                                 default=_balance_after_transfers_japan)
    balance_after_transfers_vietnam = fields.Float(string='Số dư sau giao dịch (VND)', store=True,
                                                   default=_balance_after_transfers_vietnam)
    costs_incurred_japan = fields.Integer(string='Phí khác (Yên)')
    costs_incurred_vietnam = fields.Integer(string='Phí khác (VND)')

    plan_money_japan = fields.Integer(string='Dự kiến (Yên)', compute='_plan_money_japan')
    plan_money_vietnam = fields.Integer(string='Dự kiến (VND)', compute='_plan_money_vietnam')

    send_money_japan = fields.Boolean(string='Chuyển Yên', track_visibility='onchange')
    send_money_vietnam = fields.Boolean(string='Chuyển Việt', track_visibility='onchange')

    trading_note = fields.Text(string='Ghi chú', track_visibility='onchange')
    trading_attach = fields.Many2many('ir.attachment', string="Đính kèm")

    @api.one
    @api.depends('traiding_exchange_rate', 'receive_money_japan', 'costs_incurred_japan')
    def _plan_money_vietnam(self):
        if self.traiding_exchange_rate:
            self.plan_money_vietnam = (
                                              self.receive_money_japan + self.costs_incurred_japan) * self.traiding_exchange_rate
        else:
            pass

    @api.one
    @api.depends('traiding_exchange_rate', 'receive_money_vietnam', 'costs_incurred_vietnam')
    def _plan_money_japan(self):
        if self.traiding_exchange_rate:
            self.plan_money_japan = (
                                            self.receive_money_vietnam + self.costs_incurred_vietnam) / self.traiding_exchange_rate
        else:
            pass

    @api.onchange('send_money_vietnam')
    def _onchange_send_money_vietnam(self):
        if self.send_money_vietnam:
            self.transfers_vietnam = self.plan_money_vietnam
        else:
            self.transfers_vietnam = 0
            print('Chưa chuyển')

    @api.onchange('send_money_japan')
    def _onchange_send_money_japan(self):
        if self.send_money_japan:
            self.transfers_japan = self.plan_money_japan
        else:
            self.transfers_japan = 0
            print('Chưa chuyển')

    @api.onchange('receive_money_japan')
    def _onchange_receive_money_japan(self):

        if self.receive_money_japan:
            self.balance_after_transfers_japan = self._balance_after_transfers_japan() + self.receive_money_japan
        else:
            self.balance_after_transfers_japan = self._balance_after_transfers_japan()

    @api.onchange('transfers_japan')
    def _onchange_transfers_japan(self):
        if self.transfers_japan:
            self.balance_after_transfers_japan = self._balance_after_transfers_japan() - self.transfers_japan
        else:
            self.balance_after_transfers_japan = self._balance_after_transfers_japan()

    @api.onchange('receive_money_vietnam')
    def _onchange_receive_money_vietnam(self):
        if self.receive_money_vietnam:
            self.balance_after_transfers_vietnam = self._balance_after_transfers_vietnam() + self.receive_money_vietnam
        else:
            self.balance_after_transfers_vietnam = self._balance_after_transfers_vietnam()

    @api.onchange('transfers_vietnam')
    def _onchange_transfers_vietnam(self):
        if self.transfers_vietnam:
            self.balance_after_transfers_vietnam = self._balance_after_transfers_vietnam() - self.transfers_vietnam
        else:
            self.balance_after_transfers_vietnam = self._balance_after_transfers_vietnam()

    @api.model
    def create(self, values):
        email_list = self.env['email.email'].search([('status', '=', True)])
        if email_list:
            for item in email_list:
                item.write({'balance_after_transfers_japan': 0, 'balance_after_transfers_vietnam': 0})
        else:
            print("Email không có dữ liệu")

        if values.get('trading_code', 'New') == 'New':
            values['trading_code'] = self.env['ir.sequence'].next_by_code(
                'self.service') or 'New'

        email = super(Email, self).create(values)
        # numbers = sum(c.isdigit() for c in str(email.id))
        # email['trading_code'] = 'CT' + '0' * (5 - numbers) + str(email.id)
        self.cron_do_email(email)
        return email

    @api.multi
    def write(self, values):
        email = super(Email, self).write(values)
        numbers = sum(c.isdigit() for c in str(self.id))
        trading_code = 'CT' + '0' * (5 - numbers) + str(self.id)
        if (values != {'balance_after_transfers_japan': 0, 'balance_after_transfers_vietnam': 0}):
            if (values != {'trading_code': trading_code}):
                self.cron_do_email(self)
        else:
            print(values)
        return email

    @api.multi
    def cron_do_email(self, noidung):
        sql = "SELECT SUM(receive_money_japan) AS receive_money_japan, SUM(transfers_japan) AS transfers_japan, SUM(receive_money_vietnam) AS receive_money_vietnam, SUM(transfers_vietnam) AS transfers_vietnam FROM public.email_email;"
        self._cr.execute(sql)
        tong = self._cr.fetchall()

        string = ''
        string += '<b><i style="color: black;">Kính gửi: </i></b>'
        string += '<span style="color: black;">Các nhân viên liên quan,</span><br/>'
        string += '<span style="color: black;">Thông báo về các giao dịch chuyển tiền</span><br/>'
        string += '<span style="color: black;">Mã giao dịch: </span><i style="color: red;">' + str(
            noidung.trading_code) + '</i><span style="color: black;"> - Ngày : </span><i style="color: red;">' + str(
            fields.Date.today().strftime('%d/%m/%Y')) + '</i><br/><br/>'
        string += '<table style="width:100%; border: 0px solid black; border-collapse: collapse">'
        string += '<tr>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#66d9ff; width:10%;">Tỷ giá JPY - VND</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#66d9ff; width:15%;">Ngày tháng</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#66d9ff; width:15%;">Tiền Yên nhận được</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#66d9ff; width:15%;">Tiền Yên chuyển đi</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#66d9ff; width:15%;">Tiền Việt nhận được</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#66d9ff; width:15%;">Tiền Việt chuyển đi</th>'
        string += '<th style="font-size:10px;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#66d9ff; width:15%;">Ghi chú</th>'
        string += '</tr><tr>'
        string += '<th style="font-size:10px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#ffff00; padding: 5px;" colspan = 2>Tổng</th>'
        string += '<th style="font-size:10px;vertical-align:top;text-align: right;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#ffff00; padding: 5px;">' + funtion.format(
            tong[0][0]) + '</td>'
        string += '<th style="font-size:10px;vertical-align:top;text-align: right;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#ffff00; padding: 5px;">' + funtion.format(
            tong[0][1]) + '</td>'
        string += '<th style="font-size:10px;vertical-align:top;text-align: right;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#ffff00; padding: 5px;">' + funtion.format(
            tong[0][2]) + '</td>'
        string += '<th style="font-size:10px;vertical-align:top;text-align: right;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#ffff00; padding: 5px;">' + funtion.format(
            tong[0][3]) + '</td>'
        string += '<th style="font-size:10px;vertical-align:top;text-align: right;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman; background-color:#ffff00; padding: 5px;"></td>'
        string += '</tr><tr>'
        string += '<td style="font-size:10px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;padding: 5px;">' + funtion.checknone(
            noidung.traiding_exchange_rate) + '</td>'
        string += '<td style="font-size:10px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;padding: 5px;">' + funtion.dateconvert(
            noidung.trading_date) + '</td>'
        string += '<td style="font-size:10px;vertical-align:top;text-align: right;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;padding: 5px;">' + funtion.format(
            noidung.receive_money_japan) + '</td>'
        string += '<td style="font-size:10px;vertical-align:top;text-align: right;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;padding: 5px;">' + funtion.format(
            noidung.transfers_japan) + '</td>'
        string += '<td style="font-size:10px;vertical-align:top;text-align: right;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;padding: 5px;">' + funtion.format(
            noidung.receive_money_vietnam) + '</td>'
        string += '<td style="font-size:10px;vertical-align:top;text-align: right;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;padding: 5px;">' + funtion.format(
            noidung.transfers_vietnam) + '</td>'
        string += '<td style="font-size:10px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;padding: 5px;">' + funtion.checknone(
            noidung.trading_note) + '</td>'
        string += '</tr><tr>'
        string += '<td style="font-size:10px;vertical-align:top;text-align: center;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;padding: 5px;" colspan = 2>Cán bộ</td>'
        string += '<td style="font-size:10px;vertical-align:top;text-align: right;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;padding: 5px;">' + funtion.checknone(
            noidung.receive_money_japan_officer.name) + '</td>'
        string += '<td style="font-size:10px;vertical-align:top;text-align: right;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;padding: 5px;">' + funtion.checknone(
            noidung.transfers_japan_officer.name) + '</td>'
        string += '<td style="font-size:10px;vertical-align:top;text-align: right;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;padding: 5px;">' + funtion.checknone(
            noidung.receive_money_vietnam_officer.name) + '</td>'
        string += '<td style="font-size:10px;vertical-align:top;text-align: right;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;padding: 5px;">' + funtion.checknone(
            noidung.transfers_vietnam_officer.name) + '</td>'
        string += '<td style="font-size:10px;vertical-align:top;text-align: left;border: 1px solid black; border-collapse: collapse; color:#000000; font-family: TimesNewRoman;padding: 5px;"></td>'
        string += '</tr>'
        string += '</table>'

        print(string)

        mail = self.env.ref('email.example_email_template')
        origin_mail = mail.body_html

        if string:
            mail.body_html = origin_mail.replace('{data_body}', string)
        else:
            string = '<i style="color: red;">Không lấy được thông tin giao dịch</i>'
            mail.body_html = origin_mail.replace('{data_body}', string)

        template = self.env.ref('email.example_email_template')
        ketqua = template.send_mail(template.id, force_send=True)

        print(ketqua)

        template.body_html = origin_mail
        return ketqua
