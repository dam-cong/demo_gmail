# -*- coding: utf-8 -*-

from odoo import models, fields, api
import datetime


def checknone(data):
    if data:
        return str(data)
    else:
        return str('-')


def format(self):
    if self:
        return str('{:,.0f}'.format(self))
    else:
        return str('-')


def dateconvert(self):
    if self:
        return str(self.day) + '/' + str(self.month) + '/' + str(self.year)
    else:
        return str('-')
