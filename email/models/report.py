# -*- coding: utf-8 -*-

from odoo import models, fields, api


class ReportUser(models.Model):
    _name = 'report.user'
    _rec_name = 'trading_code'

    trading_code = fields.Char(string='Mã giao dịch')
    trading_date = fields.Date(string='Ngày giao dịch')
    money_japan = fields.Integer(string='Tiền Yên')
    money_vietnam = fields.Integer(string='Tiền Việt')


class Report(models.Model):
    _name = 'email.report'
    _rec_name = 'report_user'

    report_user = fields.Many2one(comodel_name='res.users', string='Cán bộ')
    report_trading = fields.Many2many(comodel_name='report.user', string='Danh sách giao dịch')

    @api.onchange('report_user')
    def onchange_report_user(self):
        if self.report_user:
            receive_money_japan = self.env['email.email'].search(
                [('receive_money_japan_officer', '=', self.report_user.id)])

            transfers_japan = self.env['email.email'].search([('transfers_japan_officer', '=', self.report_user.id)])

            receive_money_vietnam = self.env['email.email'].search(
                [('receive_money_vietnam_officer', '=', self.report_user.id)])

            transfers_vietnam = self.env['email.email'].search(
                [('transfers_vietnam_officer', '=', self.report_user.id)])

            vals = []
            if receive_money_japan:
                for danhsach in receive_money_japan:
                    vals_row = {}
                    vals_row['trading_code'] = danhsach.trading_code
                    vals_row['trading_date'] = danhsach.trading_date
                    vals_row['money_japan'] = danhsach.receive_money_japan
                    vals_row['money_vietnam'] = 0
                    tts = self.env['report.user'].create(vals_row)
                    vals.append(tts.id)

            if transfers_japan:
                for danhsach in transfers_japan:
                    vals_row = {}
                    vals_row['trading_code'] = danhsach.trading_code
                    vals_row['trading_date'] = danhsach.trading_date
                    vals_row['money_japan'] = 0 - danhsach.transfers_japan
                    vals_row['money_vietnam'] = 0
                    tts = self.env['report.user'].create(vals_row)
                    vals.append(tts.id)

            if receive_money_vietnam:
                for danhsach in receive_money_vietnam:
                    vals_row = {}
                    vals_row['trading_code'] = danhsach.trading_code
                    vals_row['trading_date'] = danhsach.trading_date
                    vals_row['money_japan'] = 0
                    vals_row['money_vietnam'] = danhsach.receive_money_vietnam
                    tts = self.env['report.user'].create(vals_row)
                    vals.append(tts.id)

            if transfers_vietnam:
                for danhsach in transfers_vietnam:
                    vals_row = {}
                    vals_row['trading_code'] = danhsach.trading_code
                    vals_row['trading_date'] = danhsach.trading_date
                    vals_row['money_japan'] = 0
                    vals_row['money_vietnam'] = 0 - danhsach.receive_money_vietnam
                    tts = self.env['report.user'].create(vals_row)
                    vals.append(tts.id)

            self.report_trading = [(6, 0, vals)]
