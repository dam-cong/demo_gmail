# -*- coding: utf-8 -*-
from odoo import http


class Lylich(http.Controller):
    @http.route('/lylich/lylich/', auth='user', type='json')
    def hello(self):
        return {
            'html': """<div>    
                             <div class="form-group col-md-6">
                                 <label for="timedefault" class="control-label">Thời Gian Học: </label>
                                 <input type="text" class="form-control" name="timedefault" placeholder="Nhập thời gian học">
                             </div>
                        </div>"""
        }

#     @http.route('/lylich/lylich/objects/', auth='public')
#     def list(self, **kw):
#         return http.request.render('lylich.listing', {
#             'root': '/lylich/lylich',
#             'objects': http.request.env['lylich.lylich'].search([]),
#         })

#     @http.route('/lylich/lylich/objects/<model("lylich.lylich"):obj>/', auth='public')
#     def object(self, obj, **kw):
#         return http.request.render('lylich.object', {
#             'object': obj
#         })
