# -*- coding: utf-8 -*-

from odoo import models, fields, api


class lylich(models.Model):
    _name = 'lylich.lylich'
    _rec_name = 'name'

    name = fields.Char(string='Họ và tên')
    age = fields.Integer(string='Tuổi')
    user_id = fields.Many2one(comodel_name='res.users', string='Người dùng')
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100
