# -*- coding: utf-8 -*-
{
    'name': "Lý lịch",
    'depends': ['base'],
    'data': [
        'security/user_groups.xml',
        'security/ir.model.access.csv',
        'views/views.xml',
        'views/tree_view_asset.xml',
    ],
    'qweb': [
        'static/src/xml/tree_view_button.xml'
    ],
    'installable': True,
    'auto_install': True,
    'application': True
}
