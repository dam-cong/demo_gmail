# -*- coding: utf-8 -*-

from odoo import models, fields, api


class thicu(models.Model):
    _name = 'thicu.thicu'

    name = fields.Char("Họ và tên")
    date = fields.Date("Ngày tháng năm sinh")
    order = fields.Char("Đơn hàng")
    date_strat = fields.Date("Ngày trúng tuyển")
    nganhnghe = fields.Char("Ngành nghề")
    congty = fields.Char("Công ty phái cử")
    status = fields.Char("Trạng thái")

#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100
