(function() {
  var callWithJQuery;

  callWithJQuery = function(pivotModule) {
    if (typeof exports === "object" && typeof module === "object") {
      return pivotModule(require("jquery"));
    } else if (typeof define === "function" && define.amd) {
      return define(["jquery"], pivotModule);
    } else {
      return pivotModule(jQuery);
    }
  };

  callWithJQuery(function($) {
    var viFmt, viFmtInt, viFmtPct, nf, tpl;
    nf = $.pivotUtilities.numberFormat;
    tpl = $.pivotUtilities.aggregatorTemplates;
    viFmt = nf({
      thousandsSep: " ",
      decimalSep: ","
    });
    viFmtInt = nf({
      digitsAfterDecimal: 0,
      thousandsSep: " ",
      decimalSep: ","
    });
    viFmtPct = nf({
      digitsAfterDecimal: 1,
      scaler: 100,
      suffix: "%",
      thousandsSep: " ",
      decimalSep: ","
    });
    return $.pivotUtilities.locales.vi = {
      localeStrings: {
        renderError: "Đã có lỗi xảy ra khi hiển thị dữ liệu.",
        computeError: "Đã có lỗi xảy ra khi tính toán dữ liệu.",
        uiRenderError: "Đã có lỗi xảy ra khi hiển thị giao diện người dùng.",
        selectAll: "Chọn tất cả",
        selectNone: "Bỏ chọn",
        tooMany: "(quá nhiều dữ liệu để hiển thị)",
        filterResults: "Lọc dữ liệu",
        totals: "Tổng",
        vs: "với",
        by: "bỏi",
        apply: "Áp dụng",
        cancel: "Hủy"
      },
      aggregators: {
        "Đếm": tpl.count(viFmtInt),
        "Đếm các giá trị là duy nhất": tpl.countUnique(viFmtInt),
        "Danh sách các giá trị là duy nhất": tpl.listUnique(", "),
        "Tổng": tpl.sum(viFmt),
        "Tổng số nguyên": tpl.sum(viFmtInt),
        "Trung bình cộng": tpl.average(viFmt),
        "Trung bình": tpl.median(viFmt),
        "Phương sai": tpl["var"](1, viFmt),
        "Độ lệch chuyển": tpl.stdev(1, viFmt),
        "Tối thiểu": tpl.min(viFmt),
        "Tối đa": tpl.max(viFmt),
        "Đầu tiền": tpl.first(viFmt),
        "Cuối cùng": tpl.last(viFmt),
        "Tổng tên tổng": tpl.sumOverSum(viFmt),
        "Giới hạn trên 80%": tpl.sumOverSumBound80(true, viFmt),
        "Giới hạn dưới 80%": tpl.sumOverSumBound80(false, viFmt),
        "Tổng của tổng": tpl.fractionOf(tpl.sum(), "total", viFmtPct),
        "Tổng của hàng": tpl.fractionOf(tpl.sum(), "row", viFmtPct),
        "Tổng của cột": tpl.fractionOf(tpl.sum(), "col", viFmtPct),
        "Đếm của tổng": tpl.fractionOf(tpl.count(), "total", viFmtPct),
        "Đếm các hàng": tpl.fractionOf(tpl.count(), "row", viFmtPct),
        "Đếm các cột": tpl.fractionOf(tpl.count(), "col", viFmtPct)
      },
      renderers: {
        "Bảng": $.pivotUtilities.renderers["Table"],
        "Bảng biểu đồ cột": $.pivotUtilities.renderers["Table Barchart"],
        "Bản đồ nhiệt": $.pivotUtilities.renderers["Heatmap"],
        "Bản đồ nhiệt theo dòng": $.pivotUtilities.renderers["Row Heatmap"],
        "Bản đồ nhiệt theo cột": $.pivotUtilities.renderers["Col Heatmap"]
      }
    };
  });

}).call(this);