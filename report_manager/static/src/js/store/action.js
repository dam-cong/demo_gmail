odoo.define('report_manager.Action', function(require){

    let actionRoot = require('report_manager.ActionRoot');
    let actionParam = require('report_manager.ActionParam');
    let actionArgument = require('report_manager.ActionArgument');
    let actionTableDesign = require('report_manager.ActionTableDesign');
    let actionMenu = require('report_manager.ActionMenu');
    let actionFilter = require('report_manager.ActionFilter');

    let action = Object.assign({},
        actionRoot,
        actionParam,
        actionArgument,
        actionTableDesign,
        actionMenu,
        actionFilter);

    return action;

});