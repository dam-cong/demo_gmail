odoo.define('report_manager.ReducerArgument', function(require){

    var Action = require('report_manager.Action');

    const reducerArgument = function(state, action){
        switch(action.type) {
            case Action.UPDATE_ARGUMENTS:
                return updateArguments(state, action);
            case Action.ADD_ARGUMENT_ITEM:
                return addArgumentItem(state, action);
            case Action.DELETE_ARGUMENT_ITEM:
                return deleteArgumentItem(state, action);
            case Action.UPDATE_ARGUMENT_ITEM_CHECK:
                return updateArgumentItemCheck(state, action);
            case Action.UPDATE_ARGUMENT_ITEM_REQUIRE:
                return updateArgumentItemRequire(state, action);
            case Action.UPDATE_ARGUMENT_ITEM_NAME:
                return updateArgumentItemName(state, action);
            case Action.UPDATE_ARGUMENT_ITEM_DATE_FORMAT:
                return updateArgumentItemDateFormat(state, action);
            case Action.UPDATE_ARGUMENT_ITEM_DATA_TYPE:
                return updateArgumentItemDataType(state, action);
            case Action.UPDATE_ARGUMENT_ITEM_DATA_MULTI:
                return updateArgumentItemDataMulti(state, action);
            case Action.UPDATE_ARGUMENT_ITEM_DATA_TO_STRING:
                return updateArgumentItemDataToString(state, action);
            case Action.UPDATE_ARGUMENT_ITEM_SEARCH_LIKE:
                return updateArgumentItemSearchLike(state, action);
            case Action.UPDATE_ARGUMENT_ITEM_MODEL:
                return updateArgumentItemModel(state, action);
            case Action.UPDATE_SELECTED_ARGUMENT:
                return updateSelectedArgument(state, action);
            case Action.ADD_OPTION_ITEM:
                return addOptionItem(state, action);
            case Action.DELETE_OPTION_ITEM:
                return deleteOptionItem(state, action);
            case Action.UPDATE_OPTION_ITEM_NAME:
                return updateOptionItemName(state, action);
            case Action.UPDATE_OPTION_ITEM_VALUE:
                return updateOptionItemValue(state, action);
            case Action.UPDATE_OPTION_ITEM_DEFAULT:
                return updateOptionItemDefault(state, action);
            case Action.ADD_DOMAIN_ITEM:
                return addDomainItem(state, action);
            case Action.DELETE_DOMAIN_ITEM:
                return deleteDomainItem(state, action);
            case Action.UPDATE_DOMAIN_ITEM_FIELD:
                return updateDomainItemField(state, action);
            case Action.UPDATE_DOMAIN_ITEM_VALUE:
                return updateDomainItemValue(state, action);
            case Action.UPDATE_DOMAIN_ITEM_OPERATOR:
                return updateDomainItemOperator(state, action);
            case Action.UPDATE_DOMAIN_ITEM_ARGUMENT:
                return updateDomainItemArgument(state, action);
            default:
                return state
        }
    };

    const updateArguments = function(state, action){
        return {
            ...state,
            configs: {
                ...state.configs,
                arguments: action.arguments
            }
        }
    }

    const addArgumentItem = function(state, action){
        let arguments = state.configs.arguments || [];
        let argument_id = 'argument_'+_.random(0, 2147483647);
        arguments.push({id: argument_id,
                        name: argument_id,
                        check: false,
                        data_type: '',
                        source: null});
        return {
            ...state,
            configs: {
                ...state.configs,
                arguments: arguments
            }
        }
    }

    const deleteArgumentItem = function(state, action){
        let arguments = [...state.configs.arguments];
        arguments = arguments.filter(function(argument){
            return !argument.check;
        });
        return {
            ...state,
            configs: {
                ...state.configs,
                arguments: arguments
            }
        }
    }

    const updateArgumentItemCheck = function(state, action){
        let arguments = state.configs.arguments || [];
        arguments.forEach(function(argument){
            if(argument.id === action.id){
                argument.check = action.check
            }
        });
        return {
            ...state,
            configs: {
                ...state.configs,
                arguments: arguments
            }
        }
    }

    const updateArgumentItemRequire = function(state, action){
        let arguments = state.configs.arguments || [];
        arguments.forEach(function(argument){
            if(argument.id === action.id){
                argument.require = action.require;
            }
        });
        return {
            ...state,
            configs: {
                ...state.configs,
                arguments: arguments
            }
        }
    }

    const updateArgumentItemName = function(state, action){
        let arguments = state.configs.arguments || [];
        arguments.forEach(function(argument){
            if(argument.id === action.id){
                argument.name = action.name
            }
        });
        return {
            ...state,
            configs: {
                ...state.configs,
                arguments: arguments
            }
        }
    }

    const updateArgumentItemDateFormat = function(state, action){
        let arguments = state.configs.arguments || [];
        arguments.forEach(function(argument){
            if(argument.id === action.id){
                argument.format = action.format
            }
        });
        return {
            ...state,
            configs: {
                ...state.configs,
                arguments: arguments
            }
        }
    }

    const updateArgumentItemDataType = function(state, action){
        let arguments = state.configs.arguments || [];
        arguments.forEach(function(argument){
            if(argument.id === action.id){
                argument.data_type = action.data_type
            }
        });
        return {
            ...state,
            configs: {
                ...state.configs,
                arguments: arguments
            }
        }
    }

    const updateArgumentItemDataMulti = function(state, action){
        let arguments = state.configs.arguments || [];
        arguments.forEach(function(argument){
            if(argument.id === action.id){
                argument.multi = action.multi
            }
        });
        return {
            ...state,
            configs: {
                ...state.configs,
                arguments: arguments
            }
        }
    }

    const updateArgumentItemDataToString = function(state, action){
        let arguments = state.configs.arguments || [];
        arguments.forEach(function(argument){
            if(argument.id === action.id){
                argument.to_string = action.to_string
            }
        });
        return {
            ...state,
            configs: {
                ...state.configs,
                arguments: arguments
            }
        }
    }

    const updateArgumentItemSearchLike = function(state, action){
        let arguments = state.configs.arguments || [];
        arguments.forEach(function(argument){
            if(argument.id === action.id){
                argument.search_like = action.search_like
            }
        });
        return {
            ...state,
            configs: {
                ...state.configs,
                arguments: arguments
            }
        }
    }

    const updateArgumentItemModel = function(state, action){
        let arguments = state.configs.arguments || [];
        arguments.forEach(function(argument){
            if(argument.id === action.id){
                argument.model = action.model
            }
        });
        return {
            ...state,
            configs: {
                ...state.configs,
                arguments: arguments
            }
        }
    }

    const updateSelectedArgument = function(state, action){
        return {
            ...state,
            configs: {
                ...state.configs,
                selected_argument: action.argument
            }
        }
    }

    const addOptionItem = function(state, action){
        let arguments = state.configs.arguments || [];
        arguments.forEach(function(argument){
            if(argument.id === action.id){
                argument.options = argument.options || [];
                argument.options.push({
                    id: 'option_'+_.random(0, 2147483647),
                    name: '',
                    value: '',
                    default: false,
                    check: false,
                })
            }
        });
        return {
            ...state,
            configs: {
                ...state.configs,
                arguments: arguments
            }
        }
    }

    const deleteOptionItem = function(state, action){
        let arguments = state.configs.arguments || [];
        arguments.forEach(function(argument){
            if(argument.id === action.id){
                let options = argument.options.filter(function(option){
                    return option.id !== action.option_id;
                });
                argument.options = options;
            }
        });
        return {
            ...state,
            configs: {
                ...state.configs,
                arguments: arguments
            }
        }
    }

    const updateOptionItemName = function(state, action){
        let arguments = state.configs.arguments || [];
        arguments.forEach(function(argument){
            if(argument.id === action.id){
                let options = argument.options || [];
                options.forEach(function(option){
                    if(option.id === action.option_id){
                        option.name = action.name
                    }
                });
                argument.options = options;
            }
        });
        return {
            ...state,
            configs: {
                ...state.configs,
                arguments: arguments
            }
        }
    }

    const updateOptionItemValue = function(state, action){
        let arguments = state.configs.arguments || [];
        arguments.forEach(function(argument){
            if(argument.id === action.id){
                let options = argument.options || [];
                options.forEach(function(option){
                    if(option.id === action.option_id){
                        option.value = action.value
                    }
                });
                argument.options = options;
            }
        });
        return {
            ...state,
            configs: {
                ...state.configs,
                arguments: arguments
            }
        }
    }

    const updateOptionItemDefault = function(state, action){
        let arguments = state.configs.arguments || [];
        arguments.forEach(function(argument){
            let options = argument.options || [];
            options.forEach(function(option){
                option.default = option.id === action.option_id;
            });
            argument.options = options;
        });
        return {
            ...state,
            configs: {
                ...state.configs,
                arguments: arguments
            }
        }
    }

    const addDomainItem = function(state, action){
        let arguments = state.configs.arguments || [];
        arguments.forEach(function(argument){
            if(argument.id === action.id){
                argument.domains = argument.domains || [];
                argument.domains.push({
                    id: 'domain_'+_.random(0, 2147483647),
                    field: '',
                    operator: '',
                    value: '',
                    argument: {}
                })
            }
        });
        return {
            ...state,
            configs: {
                ...state.configs,
                arguments: arguments
            }
        }
    }

    const deleteDomainItem = function(state, action){
        let arguments = state.configs.arguments || [];
        arguments.forEach(function(argument){
            if(argument.id === action.id){
                let domains = argument.domains.filter(function(domain){
                    return domain.id !== action.domain_id;
                });
                argument.domains = domains;
            }
        });
        return {
            ...state,
            configs: {
                ...state.configs,
                arguments: arguments
            }
        }
    }

    const updateDomainItemField = function(state, action){
        let arguments = state.configs.arguments || [];
        arguments.forEach(function(argument){
            if(argument.id === action.id){
                let domains = argument.domains || [];
                domains.forEach(function(domain){
                    if(domain.id === action.domain_id){
                        domain.field = action.field
                    }
                });
                argument.domains = domains;
            }
        });
        return {
            ...state,
            configs: {
                ...state.configs,
                arguments: arguments
            }
        }
    }

    const updateDomainItemValue = function(state, action){
        let arguments = state.configs.arguments || [];
        arguments.forEach(function(argument){
            if(argument.id === action.id){
                let domains = argument.domains || [];
                domains.forEach(function(domain){
                    if(domain.id === action.domain_id){
                        domain.value = action.value
                    }
                });
                argument.domains = domains;
            }
        });
        return {
            ...state,
            configs: {
                ...state.configs,
                arguments: arguments
            }
        }
    }

    const updateDomainItemOperator = function(state, action){
        let arguments = state.configs.arguments || [];
        arguments.forEach(function(argument){
            if(argument.id === action.id){
                let domains = argument.domains || [];
                domains.forEach(function(domain){
                    if(domain.id === action.domain_id){
                        domain.operator = action.operator
                    }
                });
                argument.domains = domains;
            }
        });
        return {
            ...state,
            configs: {
                ...state.configs,
                arguments: arguments
            }
        }
    }

    const updateDomainItemArgument = function(state, action){
        let arguments = state.configs.arguments || [];
        arguments.forEach(function(argument){
            if(argument.id === action.id){
                let domains = argument.domains || [];
                domains.forEach(function(domain){
                    if(domain.id === action.domain_id){
                        domain.argument = action.argument
                    }
                });
                argument.domains = domains;
            }
        });
        return {
            ...state,
            configs: {
                ...state.configs,
                arguments: arguments
            }
        }
    }

    return reducerArgument;

});