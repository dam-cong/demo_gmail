odoo.define('report_manager.ReducerParam', function(require){

    var Action = require('report_manager.Action');

    const reducerParam = function(state, action){
        switch(action.type) {
            case Action.UPDATE_PARAMS:
                return updateParams(state, action);
            case Action.ADD_PARAM_ITEM:
                return addParamItem(state, action);
            case Action.DELETE_PARAM_ITEM:
                return deleteParamItem(state, action);
            case Action.UPDATE_PARAM_ITEM_NAME:
                return updateParamItemName(state, action);
            case Action.UPDATE_PARAM_ITEM_KEY:
                return updateParamItemKey(state, action);
            case Action.UPDATE_PARAM_ITEM_CHECK:
                return updateParamItemCheck(state, action);
            case Action.UPDATE_PARAM_ITEM_ARGUMENT:
                return updateParamItemArgument(state, action);
            default:
                return state
        }
    };

    const addParamItem = function(state, action){
        let params = state.configs.params || [];
        let param_id = 'param_'+_.random(0, 2147483647);
        params.push({id: param_id,
                     name: param_id,
                     check: false,
                     argument: null,
                     require: false});
        return {
            ...state,
            configs: {
                ...state.configs,
                params: params
            }
        }
    }

    const updateParamItemCheck = function(state, action){
        let params = state.configs.params || [];
        params.forEach(function(param){
            if(param.id === action.id){
                param.check = action.check
            }
        });
        return {
            ...state,
            configs: {
                ...state.configs,
                params: params
            }
        }
    }

    const updateParamItemName = function(state, action){
        let params = state.configs.params || [];
        params.forEach(function(param){
            if(param.id === action.id){
                param.name = action.name
            }
        });
        return {
            ...state,
            configs: {
                ...state.configs,
                params: params
            }
        }
    }

    const updateParamItemKey = function(state, action){
        let params = state.configs.params || [];
        params.forEach(function(param){
            if(param.id === action.id){
                param.key = action.key
            }
        });
        return {
            ...state,
            configs: {
                ...state.configs,
                params: params
            }
        }
    }

    const deleteParamItem = function(state, action){
        let params = state.configs.params || [];
        params = params.filter(function(param){
            return !param.check;
        });
        return {
            ...state,
            configs: {
                ...state.configs,
                params: params
            }
        }
    }

    const updateParams = function(state, action){
        return {
            ...state,
            configs: {
                ...state.configs,
                params: action.params
            }
        }
    }

    const updateParamItemArgument = function(state, action){
        let params = state.configs.params || [];
        params.forEach(function(param){
            if(param.id === action.id){
                param.argument = action.argument
            }
        });
        return {
            ...state,
            configs: {
                ...state.configs,
                params: params
            }
        }
    }

    return reducerParam;

});