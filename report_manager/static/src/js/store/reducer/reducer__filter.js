odoo.define('report_manager.ReducerFilter', function(require) {

    var Action = require('report_manager.Action');

    const reducerFilter = function (state, action) {
        switch (action.type) {
            case Action.APPLY_FILTER:
                return applyFilter(state, action);
            case Action.UPDATE_FILTER_ITEM:
                return updateFilterItem(state, action);
            default:
                return state
        }
    };

    const applyFilter = function (state, action) {
        return {
            ...state,
            configs: {
                ...state.configs,
                filters: action.filters
            }
        }
    }

    const updateFilterItem = function(state, action){
        let filters = state.configs.filters || [];
        let check = filters.some(function(filter){
            return filter.id === action.id
        });
        if(!check){
            filters.push({
                id: action.id,
                value: action.value
            })
        } else {
            filters.forEach(function (filter) {
                if(filter.id === action.id){
                    filter.value = action.value;
                }
            });
        }
        return {
            ...state,
            configs: {
                ...state.configs,
                filters: filters
            }
        }
    }

    return reducerFilter;

});