odoo.define('report_manager.ReducerRoot', function(require){

    var Action = require('report_manager.Action');

    var rootState = {
        name: 'Mới',
        configs: {
            sql: '',
            sql_columns: [],
            display_fragment: 'fragment_sql',
            display_tab: 'tab_design_table',
            params: [],
            arguments: [],
            selected_argument: {},
            table: [],
            selected_cell: {},
            menus: [],
            filters: [],
            pivot_config: {},
            birt: {},
        }
    };


    const reducerRoot = function(state, action){
        switch(action.type) {
            case Action.UPDATE_STORE:
                return updateStore(state, action);
            case Action.UPDATE_RECORD_NAME:
                return updateRecordName(state, action);
            case Action.UPDATE_SQL:
                return updateSQL(state, action);
            case Action.UPDATE_DISPLAY_FRAGMENT:
                return updateDisplayFragment(state, action);
            case Action.UPDATE_DISPLAY_TAB:
                return updateDisplayTab(state, action);
            case Action.UPDATE_SQL_COLUMNS:
                return updateSqlColumns(state, action);
            case Action.UPDATE_SQL_COLUMN_ITEM_NAME:
                return updateSqlColumnItemName(state, action);
            case Action.UPDATE_SQL_COLUMN_ITEM_GROUPS:
                return updateSqlColumnItemGroups(state, action);
            case Action.APPLY_PIVOT_CONFIG:
                return applyPivotConfig(state, action);
            case Action.UPDATE_BIRT_REPORT_LINK:
                return updateBirtReportLink(state, action);
            case Action.UPDATE_BIRT_REPORT_METHOD:
                return updateBirtReportMethod(state, action);
            default:
                return state
        }
    };

    const updateStore = function(state, action){
        return action.data || rootState;
    }

    const updateRecordName = function(state, action){
        return {
            ...state,
            name: action.name
        };
    }

    const updateSQL = function(state, action){
        return {
            ...state,
            configs: {
                ...state.configs,
                sql: action.sql
            }
        }
    }

    const updateDisplayFragment = function(state, action){
        return {
            ...state,
            configs: {
                ...state.configs,
                display_fragment: action.display_fragment
            }
        }
    }

    const updateDisplayTab = function(state, action){
        return {
            ...state,
            configs: {
                ...state.configs,
                display_tab: action.display_tab
            }
        }
    }

    const updateSqlColumns = function(state, action){
        return {
            ...state,
            configs: {
                ...state.configs,
                sql_columns: action.sql_columns
            }
        }
    }

    const updateSqlColumnItemName = function(state, action){
        let sql_columns = state.configs.sql_columns || [];
        sql_columns.forEach(function (column) {
            if(column.key === action.key){
                column.name = action.name;
            }
        });
        return {
            ...state,
            configs: {
                ...state.configs,
                sql_columns: sql_columns
            }
        }
    }

    const updateSqlColumnItemGroups = function(state, action){
        let sql_columns = state.configs.sql_columns || [];
        sql_columns.forEach(function (column) {
            if(column.key === action.key){
                column.groups = action.groups;
            }
        });
        return {
            ...state,
            configs: {
                ...state.configs,
                sql_columns: sql_columns
            }
        }
    }

    const applyPivotConfig = function (state, action) {
        return {
            ...state,
            configs: {
                ...state.configs,
                pivot_config: action.pivot_config
            }
        }
    }

    const updateBirtReportLink = function(state, action){
        let birt = state.configs.birt;
        birt.link = action.link;
        return {
            ...state,
            configs: {
                ...state.configs,
                birt: birt
            }
        }
    }

    const updateBirtReportMethod = function(state, action){
        let birt = state.configs.birt || {};
        birt.method = action.method;
        return {
            ...state,
            configs: {
                ...state.configs,
                birt: birt
            }
        }
    }

    return reducerRoot;

});