odoo.define('report_manager.ReducerTableDesign', function(require){

    var Action = require('report_manager.Action');

    const reducerTableDesign = function(state, action){
        switch(action.type) {
            case Action.INIT_TABLE_DATA:
                return initTableData(state, action);
            case Action.UPDATE_TABLE_DATA:
                return updateTableData(state, action);
            case Action.UPDATE_TABLE_CELL_HEAD_NAME:
                return updateTableCellHeadName(state, action);
            case Action.UPDATE_TABLE_COLUMN_SQL_COLUMN:
                return updateTableColumnSqlColumn(state, action);
            case Action.UPDATE_SELECTED_CELLS:
                return updateSelectedCells(state, action);
            default:
                return state
        }
    }

    const updateSelectedCells = function(state, action){
        return {
            ...state,
            configs: {
                ...state.configs,
                selected_cells: action.selected_cells
            }
        }
    }

    const updateTableCellHeadName = function(state, action){
        let table = state.configs.table.table || [];
        _.map(table, function(row){
            _.map(row, function(cell){
                if(cell.id === action.id){
                    cell.name = action.name;
                }
                return cell;
            });
            return row;
        });
        return {
            ...state,
            configs: {
                ...state.configs,
                table: {
                    ...state.configs.table,
                    table: table
                }
            }
        }
    }

    const updateTableColumnSqlColumn = function(state, action){
        let table = state.configs.table.table || [];
        _.map(table, function(row){
            _.map(row, function(cell){
                if(cell.col_id === action.col_id){
                    cell.name = action.sql_column ? action.sql_column.name || '' : '';
                    cell.sql_column = action.sql_column;
                }
                return cell;
            });
            return row;
        });
        return {
            ...state,
            configs: {
                ...state.configs,
                table: {
                    ...state.configs.table,
                    table: table
                }
            }
        }
    }

    const initTableData = function(state, action){
        return {
            ...state,
            configs: {
                ...state.configs,
                table: action.table
            }
        }
    }

    const updateTableData = function(state, action){
        return {
            ...state,
            configs: {
                ...state.configs,
                table: action.table
            }
        }
    }

    return reducerTableDesign
});