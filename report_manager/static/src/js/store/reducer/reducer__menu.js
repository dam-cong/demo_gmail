odoo.define('report_manager.ReducerMenu', function(require) {

    var Action = require('report_manager.Action');

    const reducerMenu = function (state, action) {
        switch (action.type) {
            case Action.ADD_MENU_ITEM:
                return addMenuItem(state, action);
            case Action.DELETE_MENU_ITEM:
                return deleteMenuItem(state, action);
            case Action.UPDATE_MENU_ITEM_CHECK:
                return updateMenuItemCheck(state, action);
            case Action.UPDATE_MENU_ITEM_NAME:
                return updateMenuItemName(state, action);
            case Action.UPDATE_MENU_ITEM_PARENT:
                return updateMenuItemParent(state, action);
            case Action.UPDATE_MENU_ITEM_GROUPS:
                return updateMenuItemGroups(state, action);
            case Action.UPDATE_MENU_ITEM_REPORT_TYPES:
                return updateMenuItemReportTypes(state, action);
            default:
                return state
        }
    };

    const addMenuItem = function(state, action){
        let menus = state.configs.menus || [];
        menus.push({id: 'menu_'+_.random(0, 2147483647),
                    name: state.name,
                    parent_id: null,
                    groups: [],
                    check: false});
        return {
            ...state,
            configs: {
                ...state.configs,
                menus: menus
            }
        }
    }

    const deleteMenuItem = function(state, action){
        let menus = [...state.configs.menus];
        menus = menus.filter(function(menu){
            return !menu.check;
        });
        return {
            ...state,
            configs: {
                ...state.configs,
                menus: menus
            }
        }
    }

    const updateMenuItemCheck = function(state, action){
        let menus = state.configs.menus || [];
        menus.forEach(function(menu){
            if(menu.id === action.id){
                menu.check = action.check
            }
        });
        return {
            ...state,
            configs: {
                ...state.configs,
                menus: menus
            }
        }
    }

    const updateMenuItemName = function(state, action){
        let menus = state.configs.menus || [];
        menus.forEach(function(menu){
            if(menu.id === action.id){
                menu.name = action.name
            }
        });
        return {
            ...state,
            configs: {
                ...state.configs,
                menus: menus
            }
        }
    }

    const updateMenuItemParent = function(state, action){
        let menus = state.configs.menus || [];
        menus.forEach(function(menu){
            if(menu.id === action.id){
                menu.parent = action.parent
            }
        });
        return {
            ...state,
            configs: {
                ...state.configs,
                menus: menus
            }
        }
    }

    const updateMenuItemGroups = function(state, action){
        let menus = state.configs.menus || [];
        menus.forEach(function(menu){
            if(menu.id === action.id){
                menu.groups = action.groups
            }
        });
        return {
            ...state,
            configs: {
                ...state.configs,
                menus: menus
            }
        }
    }

    const updateMenuItemReportTypes = function (state, action) {
        let menus = state.configs.menus || [];
        menus.forEach(function(menu){
            if(menu.id === action.id){
                menu.report_types = action.report_types
            }
        });
        return {
            ...state,
            configs: {
                ...state.configs,
                menus: menus
            }
        }
    }

    return reducerMenu;

});