odoo.define('report_manager.Reducer', function(require){

    let reducerRoot = require('report_manager.ReducerRoot');
    let reducerParam = require('report_manager.ReducerParam');
    let reducerArgument = require('report_manager.ReducerArgument');
    let reducerTableDesign = require('report_manager.ReducerTableDesign');
    let reducerMenu = require('report_manager.ReducerMenu');
    let reducerFilter = require('report_manager.ReducerFilter');

    let reducer = Redux.reduceReducers({},
        reducerRoot,
        reducerParam,
        reducerArgument,
        reducerTableDesign,
        reducerMenu,
        reducerFilter);

    return reducer;
});