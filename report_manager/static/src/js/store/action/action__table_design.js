odoo.define('report_manager.ActionTableDesign', function(require) {

    const INIT_TABLE_DATA = 'INIT_TABLE_DATA';
    const UPDATE_TABLE_DATA = 'UPDATE_TABLE_DATA';
    const UPDATE_SELECTED_CELLS = 'UPDATE_SELECTED_CELLS';
    const UPDATE_TABLE_CELL_HEAD_NAME = 'UPDATE_TABLE_CELL_HEAD_NAME';
    const UPDATE_TABLE_COLUMN_SQL_COLUMN = 'UPDATE_TABLE_COLUMN_SQL_COLUMN';

    const COLUMN_IDS = ['fix_col', 'col_'+_.random(0, 2147483647)];
    const HEAD_ROW_IDS = ['fix_row', 'row_'+_.random(0, 2147483647)];
    const BODY_ROW_IDS = ['row_'+_.random(0, 2147483647)];
    const FOOT_ROW_IDS = ['row_'+_.random(0, 2147483647)];

    let actionTableDesign = {
        INIT_TABLE_DATA: INIT_TABLE_DATA,
        UPDATE_TABLE_DATA: UPDATE_TABLE_DATA,
        UPDATE_SELECTED_CELLS: UPDATE_SELECTED_CELLS,
        UPDATE_TABLE_CELL_HEAD_NAME: UPDATE_TABLE_CELL_HEAD_NAME,
        UPDATE_TABLE_COLUMN_SQL_COLUMN: UPDATE_TABLE_COLUMN_SQL_COLUMN,
        COLUMN_IDS: COLUMN_IDS,
        HEAD_ROW_IDS: HEAD_ROW_IDS,
        BODY_ROW_IDS: BODY_ROW_IDS,
        FOOT_ROW_IDS: FOOT_ROW_IDS
    }

    return actionTableDesign

});