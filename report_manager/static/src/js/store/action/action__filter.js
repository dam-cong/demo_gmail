odoo.define('report_manager.ActionFilter', function(require) {

    const APPLY_FILTER = 'APPLY_FILTER';
    const UPDATE_FILTER_ITEM = 'UPDATE_FILTER_ITEM';

    let actionFilter = {
        APPLY_FILTER: APPLY_FILTER,
        UPDATE_FILTER_ITEM: UPDATE_FILTER_ITEM
    }

    return actionFilter;

});