odoo.define('report_manager.Store', function(require){

    var reducer = require('report_manager.Reducer');

    return Redux.createStore(reducer, {});

});