odoo.define('report_manager.ReportNotification', function (require) {
    "use strict";

    var Notification = require('web.Notification');

    var ReportNotification = Notification.extend({
        _autoCloseDelay: 4000,
        init: function (parent, params) {
            this._super.apply(this, arguments);
            if (this.type === 'success') {
                this.icon = 'fa-check';
                this.className += ' o_success';
            }
            if (this.type === 'notify') {
                this.icon = 'fa-info';
                this.className += ' o_notify';
            }
        }
    });

    return ReportNotification;

});