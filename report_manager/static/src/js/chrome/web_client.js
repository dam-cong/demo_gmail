odoo.define('report_manager.WebClient', function (require) {
    "use strict";

    var WebClient = require('web.WebClient');
    var data_manager = require('web.data_manager');

    return WebClient.include({
        on_menu_clicked: function (ev) {
            var self = this;
            return this.menu_dp.add(data_manager.load_action(ev.data.action_id))
                .then(function (result) {
                    return self.action_mutex.exec(function () {
                        var completed = $.Deferred();
                        $.when(self._openMenu(result, {
                            clear_breadcrumbs: true,
                            menu_id: ev.data.id
                        })).always(function () {
                            completed.resolve();
                        });

                        setTimeout(function () {
                            completed.resolve();
                        }, 2000);

                        return completed;
                    });
                }).then(function () {
                    self.$el.removeClass('o_mobile_menu_opened');
                });
        }
    });

});