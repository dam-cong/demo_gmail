odoo.define('report_manager.TabViewBirt', function(require) {

    var TabBasic = require('report_manager.TabBasic');
    var BirtReport = require('report_manager.BirtReport');
    var Action = require('report_manager.Action');


    var TabViewBirt = TabBasic.extend({
        name: 'tab_view_birt',
        toolbar: false,
        vertical: true,
        content_style: 'height: 100%;',
        init: function(view){
            this._super(view);
            this.store.subscribe(this, 'toggleTabDisplay', [Action.UPDATE_DISPLAY_TAB]);
        },
        renderTabContent: function () {
            this.addTabContentItem(new BirtReport(this))
        }
    });

    return TabViewBirt;

});