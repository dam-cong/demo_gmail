odoo.define('report_manager.BoxArgumentItemPropertyOptions', function(require) {

    var BoxBasic = require('report_manager.BoxBasic');
    var ButtonAddOptionItem = require('report_manager.ButtonAddOptionItem');
    var OptionTable = require('report_manager.OptionTable');


    var BoxArgumentItemPropertyOptions = BoxBasic.extend({
        class: 'box__option-table w-100',
        toolbar: true,
        vertical: true,
        content_style: 'height: auto',
        init: function(controller, argument){
            this._super.apply(this, arguments);
            this.argument = argument;
            this.options = argument.options || [];
        },
        renderBoxToolbar: function () {
            this.addBoxToolbarItem(new ButtonAddOptionItem(this.controller, this.argument));
        },
        renderBoxContent: function(){
            this.$el.find('.box__context').html('');
            this.addBoxContentItem(new OptionTable(this.controller, this.argument, this.options));
        },
    });

    return BoxArgumentItemPropertyOptions
});