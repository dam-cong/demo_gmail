odoo.define('report_manager.FragmentDesign', function(require) {

    var FragmentBasic = require('report_manager.FragmentBasic');
    var ButtonSwitchTab = require('report_manager.ButtonSwitchTab');
    var TabDesignTable = require('report_manager.TabDesignTable');
    var TabDesignPivot = require('report_manager.TabDesignPivot');
    var TabDesignBirt = require('report_manager.TabDesignBirt');
    var TabDesignPerspective = require('report_manager.TabDesignPerspective');
    var Action = require('report_manager.Action');

    var FragmentDesign = FragmentBasic.extend({
        name: 'fragment_design',
        toolbar: true,
        vertical: true,
        content_style: 'height: calc(100% - 60px);',
        init: function(controller){
            this._super(controller);
            let types = [Action.UPDATE_DISPLAY_FRAGMENT,
                Action.UPDATE_STORE];
            this.store.subscribe(this, 'toggleFragmentDisplay', types);
        },
        renderFragmentToolbar: function () {
            this.addToolbarItem(new ButtonSwitchTab(this.controller, 'fa fa-table', 'Bảng', 'tab_design_table'));
            this.addToolbarItem(new ButtonSwitchTab(this.controller, 'fa fa-bar-chart', 'Pivot', 'tab_design_pivot'));
            this.addToolbarItem(new ButtonSwitchTab(this.controller, 'fa fa-area-chart', 'Birt', 'tab_design_birt'));
            this.addToolbarItem(new ButtonSwitchTab(this.controller, 'fa fa-area-chart', 'Perspective', 'tab_design_perspective'));
        },
        renderFragmentContent: function(){
            this.addFragmentContentItem(new TabDesignTable(this.controller));
            this.addFragmentContentItem(new TabDesignPivot(this.controller));
            this.addFragmentContentItem(new TabDesignBirt(this.controller));
            this.addFragmentContentItem(new TabDesignPerspective(this.controller));
        }
    });

    return FragmentDesign;

});