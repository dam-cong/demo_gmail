odoo.define('report_manager.FragmentSQL', function(require){

    var FragmentBasic = require('report_manager.FragmentBasic');
    var ButtonSaveSql = require('report_manager.ButtonSaveSql');
    var BoxSqlEditor = require('report_manager.BoxSqlEditor');
    var BoxSqlColumn = require('report_manager.BoxSqlColumn');
    var Action = require('report_manager.Action');

    var FragmentSQL = FragmentBasic.extend({
        name: 'fragment_sql',
        toolbar: true,
        vertical: false,
        content_style: 'height: calc(100% - 60px);',
        init: function(controller){
            this._super.apply(this, arguments);
            let types = [Action.UPDATE_DISPLAY_FRAGMENT,
                Action.UPDATE_STORE];
            this.store.subscribe(this, 'toggleFragmentDisplay', types);
        },
        renderFragmentToolbar: function(){
            this.addToolbarItem(new ButtonSaveSql(this.controller, this));
        },
        renderFragmentContent: function(){
            this.boxSqlEditor = new BoxSqlEditor(this.controller);
            this.addFragmentContentItem(this.boxSqlEditor);
            this.addFragmentContentItem(new BoxSqlColumn(this.controller, this));
        },
    });

    return FragmentSQL;

});