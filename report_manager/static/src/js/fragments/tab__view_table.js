odoo.define('report_manager.TabViewTable', function(require) {

    var TabBasic = require('report_manager.TabBasic');
    var Action = require('report_manager.Action');

    var rpc = require('web.rpc');
    var session = require('web.session');


    var TabViewTable = TabBasic.extend({
        name: 'tab_view_table',
        vertical: true,
        content_style: 'height: 100%;',
        init: function(view){
            this.view = view;
            this._super(view);
            this.configs = view.configs;
            this.tableData = [];
            this.cells = [];
            this.store.subscribe(this, 'toggleTabDisplay', [Action.UPDATE_DISPLAY_TAB]);
            this.store.subscribe(this, 'reloadTableData', [Action.APPLY_FILTER]);
        },
        reloadTableData: function(){
            if(!this.isTabDisplay()){
                return;
            }
            let self = this;
            let params = this.getFiltersParams();
            let offset_limit = [0, 30];
            let config_id = self.controller.action.context.id;
            $.blockUI({'message': '<img src="/web/static/src/img/spin.png" class="fa-pulse"/>'});
            rpc.query({
                model: 'report.config',
                method: 'exec_query',
                args: [[config_id], params, offset_limit]
            }).then(function(response){
                if(Array.isArray(response)){
                    self.tableData = response;
                    self.renderTabContent();
                }
                $.unblockUI();
            }).then(function () {
                $.unblockUI();
            });
        },
        renderTabContent: function () {
            let self = this;
            let contentEl = this.$el.find('.tab__content');
            let html = '<table class="table table-hover table-striped table-bordered table-sm" id="table-data"><thead>';
            let head_row_ids = this.configs.table.head_row_ids || [];
            let tableConfigs = this.configs.table.table || {};
            if(_.isEmpty(tableConfigs)){
                return;
            }
            for(let i = 1; i < head_row_ids.length; i++){
                let row = tableConfigs[i].slice(1, tableConfigs[i].length);
                if(self.cells.length === 0){
                    self.cells = row;
                }
                html += '<tr>';
                    html += '<th style="min-width: 1cm; width: 1cm">STT</th>';
                row.forEach(function(cell){
                    let style = {
                        'background-color': cell.backgroundColor,
                        'color': cell.color,
                        'font-size': cell.fontSize
                    }
                    html += '<th style="'+style+'">'+cell.name+'</th>';
                });
                html += '</tr>';
            }
            html += '</thead><tbody>';
            this.tableData.forEach(function(row){
                html += '<tr>';
                self.cells.forEach(function(cell){
                    html += '<td>'+(row[cell.sql_column.key] || '')+'</td>';
                });
                html += '</tr>';
            });
            html += '</tbody></table>';
            contentEl.html(html);
            this.initTableData();
        },
        initTableData: function(){
            let self = this;
            let params = this.getFiltersParams();
            let config_id = self.controller.action.context.id;
            let contentEl = this.$el.find('.tab__content');
            let dom = "<'row'<'col-sm-12 col-md-5'B><'col-sm-12 col-md-7 text-right'i>>" +
                      "<'row'<'col-sm-12 col-md-5'l><'col-sm-12 col-md-7'p>>" +
                      "<'row'<'col-sm-12 table-view'tr>>" +
                      "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>";
            let columns = [{data: 'row_number'}];
            this.cells.forEach(function (cell) {
                columns.push({data: cell.sql_column.key})
            });
            this.tableData = contentEl.find('#table-data').DataTable({
                dom: dom,
                paging: true,
                fixedHeader: true,
                fixedColumns: true,
                lengthMenu: [[30, 50, -1], [30, 50, "Tất cả"]],
                language: {
                    "sProcessing":   "Đang xử lý...",
                    "sLengthMenu":   "Xem _MENU_ mục",
                    "sZeroRecords":  "Không tìm thấy dòng nào phù hợp",
                    "sInfo":         "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
                    "sInfoEmpty":    "Đang xem 0 đến 0 trong tổng số 0 mục",
                    "sInfoFiltered": "(được lọc từ _MAX_ mục)",
                    "sInfoPostFix":  "",
                    "sSearch":       "Tìm:",
                    "sUrl":          "",
                    "oPaginate": {
                        "sFirst":    "Đầu",
                        "sPrevious": "Trước",
                        "sNext":     "Tiếp",
                        "sLast":     "Cuối"
                    }
                },
                buttons: [
                    {
                        text: 'Xuất Excel',
                        className: 'btn-primary btn-sm',
                        action: function(e, dt, node, config){
                            $.blockUI({'message': '<img src="/web/static/src/img/spin.png" class="fa-pulse"/>'});
                            let columns = [];
                            self.cells.forEach(function(cell){
                                columns.push([cell.sql_column.key, cell.sql_column.name]);
                            });
                            let payload = {id: config_id,
                                           params: params,
                                           columns: columns}
                            session.get_file({
                                url: '/report_manager/datatable/export/xlsx',
                                data: {data: JSON.stringify(payload)},
                                complete: $.unblockUI(),
                                error: (error) => this.call('crash_manager', 'rpc_error', error),
                            });
                        }
                    }
                ],
                serverSide: true,
                fnServerData: function(sSource, aoData, fnCallback, oSettings ){
                    let search = '';
                    let orders = [];
                    let order = '';
                    let offset = 0;
                    let limit = 30;
                    aoData.forEach(function (item) {
                        if(item.name === 'search'){
                            // search = item.value;
                        }
                        if(item.name === 'start'){
                            offset = item.value;
                        }
                        if(item.name === 'length'){
                            limit = item.value;
                        }
                        if(item.order === 'order'){
                            item.value.forEach(function(item){
                                orders.push('%s %s' % (columns[item.column], columns[item.dir]))
                            });
                            order = orders.join(',');
                        }
                    });
                    let opts = [search, order, offset, limit]
                    oSettings.jqXHR = rpc.query({
                        model: 'report.config',
                        method: 'exec_query',
                        args: [[config_id], params, opts]
                    }).then(function(response){
                        let draw = aoData.filter(function (item) {
                            return item.name = 'draw'
                        })
                        let recordsTotal = (response[0] || {}).records_total || 0;
                        let tbData = {
                            "draw": draw[0].value,
                            "recordsTotal": recordsTotal,
                            "recordsFiltered": recordsTotal,
                            "data": response
                        }
                        fnCallback(tbData);
                    });
                },
                columns: columns,
                ordering: false,
                columnDefs: [{
                    searchable: false,
                    orderable: false,
                    targets: 0
                }],
                order: [[ 1, 'asc' ]]
            });
        }
    });

    return TabViewTable;

});

