odoo.define('report_manager.BoxFilterView', function(require) {

    var BoxBasic = require('report_manager.BoxBasic');
    var SelectorFilterModel = require('report_manager.SelectorFilterModel');
    var SelectorFilterOption = require('report_manager.SelectorFilterOption');
    var InputFilterString = require('report_manager.InputFilterString');
    var InputFilterNumber = require('report_manager.InputFilterNumber');
    var InputFilterDate = require('report_manager.InputFilterDate');
    var InputFilterDatetime = require('report_manager.InputFilterDatetime');
    var ButtonApplyFilter = require('report_manager.ButtonApplyFilter');

    var rpc = require('web.rpc');


    var BoxFilterView = BoxBasic.extend({
        class: 'box__filter-view w-100 h-100',
        vertical: true,
        content_class: 'flex-wrap align-content-start',
        init: function(view, configs){
            this._super(view);
            this.configs = configs;
        },
        renderBoxContent: function(){
            let self = this;
            self.addBoxContentItem(new ButtonApplyFilter(this));
            this.configs.arguments.forEach(function(arg){
                switch (arg.data_type.value) {
                    case 'string':
                        self.addBoxContentItem(new InputFilterString(self, arg));
                        break;
                    case 'number':
                        self.addBoxContentItem(new InputFilterNumber(self, arg));
                        break;
                    case 'date':
                        self.addBoxContentItem(new InputFilterDate(self, arg));
                        break;
                    case 'datetime':
                        self.addBoxContentItem(new InputFilterDatetime(self, arg));
                        break;
                    case 'options':
                        self.addBoxContentItem(new SelectorFilterOption(self, arg));
                        break;
                    case 'model':
                        self.renderFilterModel(arg);
                        break;
                }
            });
        },
        renderFilterModel: async function(arg){
            let self = this;
            if(!arg.model || !arg.model.value){
                console.error('Argument not select model');
                return;
            }
            await rpc.query({
                model: 'ir.model',
                method: 'search_read',
                domain: [['id', '=', arg.model.value]],
                fields: ['model']
            }, {
                shadow: true,
            }).then(function(response){
                arg.model.model = response[0].model;
                self.addBoxContentItem(new SelectorFilterModel(self, arg));
            });
        }
    });

    return BoxFilterView
});