odoo.define('report_manager.FragmentBasic', function(require){

    var WidgetBasic = require('report_manager.WidgetBasic');

    var FragmentBasic = WidgetBasic.extend({
        template: 'RConfigFragment',
        display: false,
        name: false,
        toolbar: false,
        vertical: false,
        class: '',
        content_class: '',
        content_style: '',
        init: function(controller){
            this._super(controller);
            if(!this.name){
                this.name = 'fragment_'+_.random(0, 2147483647);
            }
        },
        renderElement: function(){
            this._super.apply(this, arguments);
            this.renderFragmentToolbar();
            this.renderFragmentContent();
            this.toggleFragmentDisplay();
        },
        renderFragmentToolbar: function () {

        },
        addToolbarItem: function(item){
            let toolbarEl = this.$el.find('.fragment__toolbar');
            item.appendTo(toolbarEl)
        },
        renderFragmentContent: function (){

        },
        addFragmentContentItem: function(item){
            let contentEl = this.$el.find('.fragment__content');
            item.appendTo(contentEl)
        },
        toggleFragmentDisplay: function(){
            this.display = this.isFragmentDisplay();
            if(!this.$el){
                return;
            }
            if(this.display){
                this.$el.addClass('display');
            } else {
                this.$el.removeClass('display');
            }
        },
        isFragmentDisplay: function () {
            let configs = this.store.getState().configs;
            return configs && configs.hasOwnProperty('display_fragment') && configs.display_fragment == this.name;
        }
    });

    return FragmentBasic;

});