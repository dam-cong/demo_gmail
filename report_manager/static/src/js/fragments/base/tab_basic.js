odoo.define('report_manager.TabBasic', function(require){

    var WidgetBasic = require('report_manager.WidgetBasic');

    var TabBasic = WidgetBasic.extend({
        template: 'RConfigTab',
        toolbar: false,
        display: false,
        name: false,
        class: '',
        content_class: '',
        content_style: '',
        init: function(controller){
            this._super(controller);
            if(!this.name){
                this.name = 'tab_'+_.random(0, 2147483647);
            }
        },
        renderElement: function(){
            this._super.apply(this, arguments);
            this.renderTabToolbar();
            this.renderTabContent();
            this.toggleTabDisplay();
        },
        renderTabToolbar: function () {

        },
        addTabToolbarItem: function(item){
            let toolbarEl = this.$el.find('.tab__toolbar');
            item.appendTo(toolbarEl)
        },
        renderTabContent: function (){

        },
        addTabContentItem: function(item){
            let contentEl = this.$el.find('.tab__content');
            item.appendTo(contentEl)
        },
        toggleTabDisplay: function(){
            this.display = this.isTabDisplay();
            if(!this.$el){
                return;
            }
            if(this.display){
                this.$el.addClass('display');
            } else {
                this.$el.removeClass('display');
            }
        },
        isTabDisplay: function () {
            let configs = this.store.getState().configs;
            return configs && configs.hasOwnProperty('display_tab') && configs.display_tab == this.name;
        }
    });

    return TabBasic;

});