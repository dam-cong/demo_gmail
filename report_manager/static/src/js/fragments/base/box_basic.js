odoo.define('report_manager.BoxBasic', function(require){

    var WidgetBasic = require('report_manager.WidgetBasic');

    var BoxBasic = WidgetBasic.extend({
        template: 'RConfigBox',
        name: false,
        toolbar: false,
        vertical: false,
        class: '',
        content_class: '',
        content_style: '',
        init: function(controller){
            this._super(controller);
            if(!this.name){
                this.name = 'box_'+_.random(0, 2147483647);
            }
        },
        renderElement: function(){
            this._super.apply(this, arguments);
            this.renderBoxToolbar();
            this.renderBoxContent();
        },
        renderBoxToolbar: function () {

        },
        addBoxToolbarItem: function(item){
            let toolbarEl = this.$el.find('.box__toolbar');
            item.appendTo(toolbarEl)
        },
        renderBoxContent: function (){

        },
        addBoxContentItem: function(item){
            let contentEl = this.$el.find('.box__content');
            item.appendTo(contentEl)
        }
    });

    return BoxBasic;

});