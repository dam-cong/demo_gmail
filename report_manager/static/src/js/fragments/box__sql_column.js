odoo.define('report_manager.BoxSqlColumn', function(require) {

    var BoxBasic = require('report_manager.BoxBasic');
    var SqlColumnTable = require('report_manager.SqlColumnTable');
    var Action = require('report_manager.Action');


    var BoxSqlColumn = BoxBasic.extend({
        class: 'box__sql-column w-45',
        toolbar: false,
        content_style: 'height: 100%;',
        init: function(controller, fragment){
            this._super(controller);
            this.fragment = fragment;
            let types = [Action.UPDATE_DISPLAY_FRAGMENT,
                Action.UPDATE_STORE];
            this.store.subscribe(this, 'reRenderBoxContent', types)
        },
        reRenderBoxContent: function(){
            this.$el.find('.box__content').html('');
            this.renderBoxContent();
        },
        renderBoxContent: function(){
            this.addBoxContentItem(new SqlColumnTable(this.controller, this.fragment))
        }
    });

    return BoxSqlColumn;
});