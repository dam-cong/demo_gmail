odoo.define('report_manager.FragmentParam', function(require){

    var FragmentBasic = require('report_manager.FragmentBasic');
    var ButtonAddParamItem = require('report_manager.ButtonAddParamItem');
    var ButtonDeleteParamItem = require('report_manager.ButtonDeleteParamItem');
    var ParamTable = require('report_manager.ParamTable');
    var Action = require('report_manager.Action');

    var FragmentParam = FragmentBasic.extend({
        name: 'fragment_param',
        toolbar: true,
        vertical: true,
        content_style: 'height: calc(100% - 60px);',
        init: function(controller){
            this._super(controller);
            let types = [Action.UPDATE_DISPLAY_FRAGMENT,
                Action.UPDATE_SQL,
                Action.UPDATE_STORE];
            this.store.subscribe(this, 'toggleFragmentDisplay', types);
        },
        renderFragmentToolbar: function () {
            this.addToolbarItem(new ButtonAddParamItem(this.controller));
            this.addToolbarItem(new ButtonDeleteParamItem(this.controller));
        },
        renderFragmentContent: function(){
            this.addFragmentContentItem(new ParamTable(this.controller, this));
        }
    });

    return FragmentParam;

});