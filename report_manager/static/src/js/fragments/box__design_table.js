odoo.define('report_manager.BoxDesignTable', function(require) {

    var BoxBasic = require('report_manager.BoxBasic');
    var TableDesign = require('report_manager.TableDesign');
    var Action = require('report_manager.Action');


    var BoxDesignTable = BoxBasic.extend({
        class: 'box__design',
        init: function(controller){
            this._super(controller);
            let types = [Action.UPDATE_DISPLAY_FRAGMENT,
                Action.UPDATE_DISPLAY_TAB];
            this.store.subscribe(this, 'renderTableDesign', types);
        },
        renderElement: function(){
            this._super.apply(this, arguments);
            this.renderTableDesign();
            this.initTableResizable();
        },
        renderTableDesign: function(){
            let boxDesignEl = this.$el.find('.box__content');
            boxDesignEl.html('');
            let tableDesign = new TableDesign(this.controller);
            tableDesign.appendTo(boxDesignEl);
        },
        initTableResizable: function(){
            this.$el.resizable({
                handles: 's',
                minHeight: 250
            });
        },
    });

    return BoxDesignTable
});