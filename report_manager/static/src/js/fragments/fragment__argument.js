odoo.define('report_manager.FragmentArgument', function(require) {

    var FragmentBasic = require('report_manager.FragmentBasic');
    var BoxArgumentTable = require('report_manager.BoxArgumentTable');
    var BoxArgumentItemProperty = require('report_manager.BoxArgumentItemProperty');
    var Action = require('report_manager.Action');

    var FragmentArgument = FragmentBasic.extend({
        name: 'fragment_argument',
        toolbar: false,
        vertical: true,
        content_style: 'height: calc(100% - 30px);',
        init: function(controller){
            this._super(controller);
            let types = [Action.UPDATE_DISPLAY_FRAGMENT,
                Action.UPDATE_STORE];
            this.store.subscribe(this, 'toggleFragmentDisplay', types);
        },
        renderFragmentContent: function(){
            this.addFragmentContentItem(new BoxArgumentTable(this.controller, this));
            this.addFragmentContentItem(new BoxArgumentItemProperty(this.controller, this));
        }
    })

    return FragmentArgument;

});