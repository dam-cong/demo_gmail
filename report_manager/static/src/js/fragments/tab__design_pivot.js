odoo.define('report_manager.TabDesignPivot', function(require) {

    var TabBasic = require('report_manager.TabBasic');
    var ButtonApplyPivot = require('report_manager.ButtonApplyPivot');
    var Action = require('report_manager.Action');


    var TabDesignPivot = TabBasic.extend({
        template: 'RConfigTabDesignPivot',
        name: 'tab_design_pivot',
        toolbar: true,
        vertical: true,
        content_style: 'height: calc(100% - 40px);',
        init: function(controller){
            this._super(controller);
            let types = [Action.UPDATE_DISPLAY_TAB];
            this.store.subscribe(this, 'toggleTabDisplay', types);
        },
        renderElement: function(){
            this._super.apply(this, arguments);
            this.initPivot();
        },
        renderTabToolbar: function(){
            this.addTabToolbarItem(new ButtonApplyPivot(this.controller));
        },
        toggleTabDisplay: function(){
            this._super.apply(this, arguments);
            this.initPivot();
        },
        initPivot: function () {
            let configs = this.store.getState().configs;
            let renderers = $.extend($.pivotUtilities.renderers, $.pivotUtilities.subtotal_renderers);
            let dataClass = $.pivotUtilities.SubtotalPivotData;
            let defaultConfigs = {
                dataClass: dataClass,
                renderers: renderers,
                rendererName: "Table With Subtotal",
                rendererOptions: {
                    arrowExpanded: "[+]",
                    arrowCollapsed: "[-]"
                }
            }
            let pivot_config = _.extend(configs.pivot_config || {}, defaultConfigs);
            let sql_columns = configs.sql_columns || [];
            let configRow = {}
            sql_columns.forEach(function (col) {
                configRow[col.name] = col.key;
            });
            let pivotConfigs = [configRow];

            this.$el.find('#pivot-design').pivotUI(pivotConfigs, pivot_config, true);
        },
    });

    return TabDesignPivot;

});