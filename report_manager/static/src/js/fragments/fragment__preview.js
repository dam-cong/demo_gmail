odoo.define('report_manager.FragmentPreview', function(require) {

    var FragmentBasic = require('report_manager.FragmentBasic');
    var ButtonSwitchTab = require('report_manager.ButtonSwitchTab');
    var TabDesignTable = require('report_manager.TabDesignTable');
    var Action = require('report_manager.Action');

    var FragmentPreview = FragmentBasic.extend({
        name: 'fragment_preview',
        toolbar: true,
        vertical: true,
        content_style: 'height: calc(100% - 60px);',
        init: function(controller){
            this._super(controller);
            let types = [Action.UPDATE_DISPLAY_FRAGMENT,
                Action.UPDATE_STORE];
            this.store.subscribe(this, 'toggleFragmentDisplay', types);
        },
        renderFragmentToolbar: function () {
            this.addToolbarItem(new ButtonSwitchTab(this.controller, 'fa fa-table', 'Bảng', 'tab_design_table'));
        },
        renderFragmentContent: function(){
            this.addFragmentContentItem(new TabDesignTable(this.controller));
        }
    });

    return FragmentPreview;

});