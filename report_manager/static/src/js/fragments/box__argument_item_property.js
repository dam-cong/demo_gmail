odoo.define('report_manager.BoxArgumentItemProperty', function(require) {

    var core = require('web.core');
    var BoxBasic = require('report_manager.BoxBasic');
    var Action = require('report_manager.Action');
    var InputArgumentItemName = require('report_manager.InputArgumentItemName');
    var InputArgumentItemDateFormat = require('report_manager.InputArgumentItemDateFormat');
    var SelectorArgumentItemDataType = require('report_manager.SelectorArgumentItemDataType');
    var SelectorArgumentItemModel = require('report_manager.SelectorArgumentItemModel');
    var CheckboxArgumentItemDataMulti = require('report_manager.CheckboxArgumentItemDataMulti');
    var CheckboxArgumentItemDataToString = require('report_manager.CheckboxArgumentItemDataToString');
    var CheckboxArgumentItemSearchLike = require('report_manager.CheckboxArgumentItemSearchLike');
    var BoxArgumentItemPropertyOptions = require('report_manager.BoxArgumentItemPropertyOptions');
    var BoxArgumentItemPropertyDomains = require('report_manager.BoxArgumentItemPropertyDomains');

    var _lt = core._lt;

    var BoxArgumentItemProperty = BoxBasic.extend({
        class: 'box__argument-item-property w-65',
        toolbar: false,
        vertical: true,
        content_class: 'flex-row flex-wrap align-content-start',
        content_style: 'height: 100;',
        init: function(controller, fragment){
            this._super.apply(this, arguments);
            this.fragment = fragment;
            let types = [Action.UPDATE_DISPLAY_FRAGMENT,
                Action.UPDATE_SELECTED_ARGUMENT,
                Action.UPDATE_ARGUMENT_ITEM_DATA_TYPE,
                Action.UPDATE_DISPLAY_FRAGMENT,
                Action.UPDATE_STORE]
            this.store.subscribe(this, 'reRenderBoxContent', types);
        },
        reRenderBoxContent: function(){
            this.$el.find('.box__content').html('');
            this.renderBoxContent();
        },
        renderBoxContent: function(){
            this._super();
            let arguments = this.store.getState().configs.arguments;
            let argument = this.store.getState().configs.selected_argument;
            let resetArgument = true;
            arguments.forEach(function (item) {
                if(item.id === argument.id){
                    resetArgument = false;
                    argument = item;
                }
            });
            if(resetArgument){
                argument = null;
            }
            if(argument){
                this.addBoxContentItem(new InputArgumentItemName(this.controller, argument,
                    {label: _lt('Argument name'), class: 'mb-3 w-50'}));
                this.addBoxContentItem(new SelectorArgumentItemDataType(this.controller, argument,
                    {label: _lt('Data type'), vertical: true, class: 'mb-3 w-50'}));

                let data_type = argument.data_type ? argument.data_type.value : '';
                switch (data_type) {
                    case 'model':
                        this.renderPropertyFieldsDataTypeModel(argument);
                        break;
                    case 'options':
                        this.renderPropertyFieldsDataTypeOptions(argument);
                        break;
                    case 'string':
                        this.renderPropertyFieldsDataString(argument);
                        break;
                    case 'date':
                    case 'datetime':
                        this.renderPropertyFieldsDataDateDateTime(argument);
                        break;
                }
            } else{
                this.$el.find('.box__content').html('');
            }
        },
        renderPropertyFieldsDataTypeModel: function(argument){
            this.addBoxContentItem(new SelectorArgumentItemModel(this.controller, argument,
                {label: _lt('Model data'), vertical: true, class: 'mb-3 w-50'}));
            this.addBoxContentItem(new CheckboxArgumentItemDataMulti(this.controller, argument,
                {label: _lt('Multi value'), vertical: true, class: 'mb-3 w-50'}));
            this.addBoxContentItem(new CheckboxArgumentItemDataToString(this.controller, argument,
                {label: _lt('Parse data to string'), vertical: true, class: 'mb-3 w-50'}));
            this.addBoxContentItem(new BoxArgumentItemPropertyDomains(this.controller, argument));
        },
        renderPropertyFieldsDataTypeOptions: function(argument){
            this.addBoxContentItem(new BoxArgumentItemPropertyOptions(this.controller, argument));
        },
        renderPropertyFieldsDataDateDateTime: function(argument){
            this.addBoxContentItem(new InputArgumentItemDateFormat(this.controller, argument,
                {label: _lt('Output date format'), class: 'mb-3 w-50'}));
        },
        renderPropertyFieldsDataString: function(argument){
            this.addBoxContentItem(new CheckboxArgumentItemSearchLike(this.controller, argument,
                {label: _lt('Search like'), class: 'mb-3 w-50'}));
        }
    });

    return BoxArgumentItemProperty
});