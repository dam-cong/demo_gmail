odoo.define('report_manager.BoxArgumentTable', function(require) {

    var BoxBasic = require('report_manager.BoxBasic');
    var ButtonAddArgumentItem = require('report_manager.ButtonAddArgumentItem');
    var ButtonDeleteArgumentItem = require('report_manager.ButtonDeleteArgumentItem');
    var ArgumentTable = require('report_manager.ArgumentTable');


    var BoxArgumentTable = BoxBasic.extend({
        class: 'box__argument-table w-35',
        toolbar: true,
        vertical: true,
        content_style: 'height: calc(100% - 30px);',
        init: function(controller, fragment){
            this._super.apply(this, arguments);
            this.fragment = fragment;
        },
        renderBoxToolbar: function () {
            this.addBoxToolbarItem(new ButtonAddArgumentItem(this.controller));
            this.addBoxToolbarItem(new ButtonDeleteArgumentItem(this.controller));
        },
        renderBoxContent: function(){
            this.addBoxContentItem(new ArgumentTable(this.controller, this.fragment));
        }
    });

    return BoxArgumentTable
});