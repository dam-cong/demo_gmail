odoo.define('report_manager.TabDesignTable', function(require) {

    var TabBasic = require('report_manager.TabBasic');
    var BoxDesignTable = require('report_manager.BoxDesignTable');
    var Action = require('report_manager.Action');


    var TabDesignTable = TabBasic.extend({
        template: 'RConfigTabTableDesign',
        name: 'tab_design_table',
        vertical: true,
        content_style: 'height: calc(100% - 30px);',
        init: function(controller){
            this._super(controller);
            let types = [Action.UPDATE_DISPLAY_TAB];
            this.store.subscribe(this, 'toggleTabDisplay', types);
        },
        renderElement: function(){
            this._super.apply(this, arguments);
            this.renderTabItems();
        },
        renderTabItems: function(){
            let tabEl = this.$el.find('.tab');
            tabEl.html('');
            let boxDesignTable = new BoxDesignTable(this.controller);
            boxDesignTable.appendTo(tabEl);
        },
    });

    return TabDesignTable;

});