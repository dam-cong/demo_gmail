odoo.define('report_manager.BoxSqlEditor', function(require) {

    var BoxBasic = require('report_manager.BoxBasic');
    var SqlCodeEditor = require('report_manager.SqlCodeEditor');
    var Action = require('report_manager.Action');


    var BoxSqlEditor = BoxBasic.extend({
        class: 'box__sql-editor w-55',
        content_style: 'height: 100%;',
        init: function(controller){
            this._super(controller);
            let types = [Action.UPDATE_DISPLAY_FRAGMENT,
                Action.UPDATE_STORE];
            this.store.subscribe(this, 'reRenderSqlEditor', types)
        },
        reRenderSqlEditor: function(){
            try{
                this.editor.refreshEditor();
            } catch (e){

            }
        },
        renderBoxContent: function(){
            this.editor = new SqlCodeEditor(this.controller);
            this.addBoxContentItem(this.editor);
        }
    });

    return BoxSqlEditor
});