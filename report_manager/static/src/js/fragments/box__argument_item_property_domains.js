odoo.define('report_manager.BoxArgumentItemPropertyDomains', function(require) {

    var BoxBasic = require('report_manager.BoxBasic');
    var ButtonAddDomainItem = require('report_manager.ButtonAddDomainItem');
    var DomainTable = require('report_manager.DomainTable');


    var BoxArgumentItemPropertyDomains = BoxBasic.extend({
        class: 'box__domain-table w-100',
        toolbar: true,
        vertical: true,
        content_style: 'height: auto',
        init: function(controller, argument){
            this._super.apply(this, arguments);
            this.argument = argument;
            this.domains = argument.domains || [];
        },
        renderBoxToolbar: function () {
            this.addBoxToolbarItem(new ButtonAddDomainItem(this.controller, this.argument));
        },
        renderBoxContent: function(){
            this.$el.find('.box__context').html('');
            this.addBoxContentItem(new DomainTable(this.controller, this.argument, this.domains));
        },
    });

    return BoxArgumentItemPropertyDomains
});