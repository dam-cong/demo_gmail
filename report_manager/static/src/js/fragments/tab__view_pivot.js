odoo.define('report_manager.TabViewPivot', function(require) {

    var TabBasic = require('report_manager.TabBasic');
    var ButtonApplyPivot = require('report_manager.ButtonApplyPivot');
    var Action = require('report_manager.Action');

    var rpc = require('web.rpc');


    var TabViewPivot = TabBasic.extend({
        name: 'tab_view_pivot',
        toolbar: false,
        vertical: true,
        content_style: 'height: 100%;',
        init: function(view){
            this.view = view;
            this._super(view);
            this.configs = view.configs;
            this.pivotData = [];
            this.store.subscribe(this, 'toggleTabDisplay', [Action.UPDATE_DISPLAY_TAB]);
            this.store.subscribe(this, 'reloadPivot', [Action.APPLY_FILTER]);
        },
        renderElement: function(){
            this._super.apply(this, arguments);
            this.$el.find('.tab__content').html('<div id="pivot-view"></div>');
            this.loadPivotTable();
        },
        renderTabToolbar: function(){
            this.addTabToolbarItem(new ButtonApplyPivot(this.controller));
        },
        toggleTabDisplay: function(){
            this._super.apply(this, arguments);
            this.reloadPivot();
        },
        initPivot: function () {
            if(!this.isTabDisplay()){
                return;
            }
            let self = this;
            let configs = this.store.getState().configs;
            let renderers = $.extend($.pivotUtilities.renderers, $.pivotUtilities.subtotal_renderers);
            let dataClass = $.pivotUtilities.SubtotalPivotData;
            let defaultConfigs = {
                dataClass: dataClass,
                renderers: renderers,
                rendererName: "Table With Subtotal",
                rendererOptions: {
                    arrowExpanded: "→",
                    arrowCollapsed: "←"
                }
            }
            let pivot_config = _.extend(configs.pivot_config || {}, defaultConfigs);
            const renamePivotData = function(injectRecord){
                let sql_columns = configs.sql_columns || [];
                self.pivotData.map(function (item) {
                    let newItem = {}
                    sql_columns.forEach(function (col) {
                        newItem[col.name] = item[col.key];
                    });
                    injectRecord(newItem);
                })
            };
            this.$el.find('#pivot-view').pivotUI(renamePivotData, pivot_config, true);
        },
        reloadPivot: function(){
            this.loadPivotTable();
        },
        loadPivotTable: function(){
            if(!this.isTabDisplay()){
                return;
            }
            let self = this;
            let params = this.getFiltersParams();
            let config_id = self.controller.action.context.id;
            let opts = [false, false, false, false];
            $.blockUI({'message': '<img src="/web/static/src/img/spin.png" class="fa-pulse"/>'});
            rpc.query({
                model: 'report.config',
                method: 'exec_query',
                args: [[config_id], params, opts]
            }).then(function(response){
                if(Array.isArray(response)){
                    self.pivotData = response;
                    self.initPivot();
                }
                $.unblockUI();
            }).then(function(){
                $.unblockUI();
            });
        }
    });

    return TabViewPivot;

});