odoo.define('report_manager.FragmentMenu', function(require){

    var FragmentBasic = require('report_manager.FragmentBasic');
    var ButtonAddMenuItem = require('report_manager.ButtonAddMenuItem');
    var ButtonDeleteMenuItem = require('report_manager.ButtonDeleteMenuItem');
    var MenuTable = require('report_manager.MenuTable');
    var Action = require('report_manager.Action');

    var FragmentMenu = FragmentBasic.extend({
        name: 'fragment_menu',
        toolbar: true,
        vertical: true,
        content_style: 'height: calc(100% - 60px);',
        init: function(controller){
            this._super.apply(this, arguments);
            let types = [Action.UPDATE_DISPLAY_FRAGMENT,
                Action.UPDATE_STORE];
            this.store.subscribe(this, 'toggleFragmentDisplay', types);
        },
        renderElement: function(){
            this._super.apply(this, arguments);
        },
        renderFragmentToolbar: function () {
            this.addToolbarItem(new ButtonAddMenuItem(this.controller));
            this.addToolbarItem(new ButtonDeleteMenuItem(this.controller));
        },
        renderFragmentContent: function(){
            this.addFragmentContentItem(new MenuTable(this.controller, this));
        },
    });

    return FragmentMenu;

});