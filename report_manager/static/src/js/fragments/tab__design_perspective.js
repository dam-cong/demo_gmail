odoo.define('report_manager.TabDesignPerspective', function(require) {

    var TabBasic = require('report_manager.TabBasic');
    var Perspective = require('report_manager.Perspective');
    var Action = require('report_manager.Action');


    var TabDesignPerspective = TabBasic.extend({
        name: 'tab_design_perspective',
        toolbar: false,
        vertical: true,
        content_style: 'height: 100%',
        init: function(controller){
            this._super(controller);
            let types = [Action.UPDATE_DISPLAY_TAB];
            this.store.subscribe(this, 'toggleTabDisplay', types);
        },
        renderTabContent: function(){
            this.addTabContentItem(new Perspective(this));
        }
    });

    return TabDesignPerspective;

});