odoo.define('report_manager.TabDesignBirt', function(require) {

    var TabBasic = require('report_manager.TabBasic');
    var InputBirtReportLink = require('report_manager.InputBirtReportLink');
    var SelectorBirtReportMethod = require('report_manager.SelectorBirtReportMethod');
    var Action = require('report_manager.Action');


    var TabDesignBirt = TabBasic.extend({
        name: 'tab_design_birt',
        vertical: true,
        content_class: 'flex-column',
        content_style: 'height: 100%',
        init: function(controller){
            this._super(controller);
            let types = [Action.UPDATE_DISPLAY_TAB];
            this.store.subscribe(this, 'toggleTabDisplay', types);
        },
        renderTabContent: function () {
            let birt = this.store.getState().configs.birt || {};
            this.addTabContentItem(new InputBirtReportLink(this.controller, birt.link || ''));
            this.addTabContentItem(new SelectorBirtReportMethod(this.controller, birt.method || {}));
        }
    });

    return TabDesignBirt;

});