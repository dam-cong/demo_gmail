odoo.define('report_manager.WidgetBasic', function(require){

    var Widget = require('web.Widget');

    var WidgetBasic = Widget.extend({
        init: function(controller){
            this.controller = controller;
            this.store = controller.store;
            this._super.apply(this, arguments);
        },
        mapPropsToState: function(props){
            let self = this;
            let propKeys = Object.keys(props);
            propKeys.forEach(function(key){
                self[key] = props[key];
            });
        },
        removeUnicode: function(str) {
            str = str || '';
            str = str.toLowerCase();
            str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a");
            str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e");
            str = str.replace(/ì|í|ị|ỉ|ĩ/g,"i");
            str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o");
            str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u");
            str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y");
            str = str.replace(/đ/g,"d");
            str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g," ");
            str = str.replace(/ + /g," ");
            str = str.trim();
            return str;
        },
        getFiltersParams: function(){
            let params = [];
            let configs = this.store.getState().configs;
            let config_params = configs.params || [];
            let config_filters = configs.filters || [];
            config_params.forEach(function(param){
                config_filters.forEach(function(filter){
                    try {
                        if (param.argument.id === filter.id) {
                            params.push(filter.value);
                        }
                    } catch (e){
                        console.error(e);
                    }
                });
            });
            return params;
        }
    });

    return WidgetBasic;

});