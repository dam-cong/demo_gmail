odoo.define('report_manager.OptionTable', function(require){

    var WidgetBasic = require('report_manager.WidgetBasic');
    var OptionItem = require('report_manager.OptionItem');
    var Action = require('report_manager.Action');

    var OptionTable = WidgetBasic.extend({
        template: 'RConfigOptionTable',
        init: function(controller, argument, options){
            this._super(controller);
            this.argument = argument;
            this.options = options;
            let types = [Action.ADD_OPTION_ITEM,
                Action.DELETE_OPTION_ITEM];
            this.store.subscribe(this, 'reRenderOptionTableItems', types);
        },
        renderElement: function(){
            this._super.apply(this, arguments);
            this.renderOptionTableItems();
        },
        renderOptionTableItems: function(){
            let self = this;
            let bodyEl = this.$el.find('tbody');
            bodyEl.html('');
            this.options.forEach(function(option){
                let item = new OptionItem(self.controller, self.argument, option);
                item.appendTo(bodyEl);
            });
        },
        reRenderOptionTableItems: function(){
            let configs = this.store.getState().configs;
            let argument = configs.selected_argument;
            let arguments = configs.arguments.filter(function(arg){
                return arg.id === argument.id;
            })
            this.argument = arguments[0];
            this.options = this.argument.options;
            this.renderOptionTableItems();
        }
    });

    return OptionTable;

});
