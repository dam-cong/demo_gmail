odoo.define('report_manager.SqlColumnTable', function(require){

    var WidgetBasic = require('report_manager.WidgetBasic');
    var SqlColumnItem = require('report_manager.SqlColumnItem');
    var Action = require('report_manager.Action');

    var SqlColumnTable = WidgetBasic.extend({
        template: 'RConfigSqlColumnTable',
        init: function(controller, fragment){
            this._super(controller);
            this.fragment = fragment;
            let types = [Action.UPDATE_SQL_COLUMNS];
            this.store.subscribe(this, 'reRenderTableItems', types);
        },
        renderElement: function(){
            this._super.apply(this, arguments);
            this.renderTableItems();
        },
        reRenderTableItems: function(){
            if(this.fragment.isFragmentDisplay()){
                this.renderTableItems();
            }
        },
        renderTableItems: function(){
            let self = this;
            let columnsEl = this.$el.find('tbody')
            let sql_columns = this.store.getState().configs.sql_columns || [];
            columnsEl.html('');
            sql_columns.forEach(function(column){
                let item = new SqlColumnItem(self.controller, column);
                item.appendTo(columnsEl);
            });
        }
    });

    return SqlColumnTable;

});