odoo.define('report_manager.DomainItem', function(require){

    var WidgetBasic = require('report_manager.WidgetBasic');
    var InputDomainItemField = require('report_manager.InputDomainItemField');
    var InputDomainItemOperator = require('report_manager.InputDomainItemOperator');
    var SelectorDomainItemArgument = require('report_manager.SelectorDomainItemArgument');
    var InputDomainItemValue = require('report_manager.InputDomainItemValue');
    var ButtonDeleteDomainItem = require('report_manager.ButtonDeleteDomainItem');

    var DomainItem = WidgetBasic.extend({
        template: 'RConfigDomainItem',
        init: function(controller, argument, domain){
            this.argument = argument;
            this.domain = domain;
            this._super(controller);
        },
        renderElement: function(){
            this._super.apply(this, arguments);

            let fieldEl = this.$el.find('.domain-item__field');
            let inputDomainItemField = new InputDomainItemField(this.controller, this.argument, this.domain);
            inputDomainItemField.appendTo(fieldEl);

            let operatorEl = this.$el.find('.domain-item__operator');
            let inputDomainItemOperator = new InputDomainItemOperator(this.controller, this.argument, this.domain);
            inputDomainItemOperator.appendTo(operatorEl);

            let argumentEl = this.$el.find('.domain-item__argument');
            let selectorDomainItemArgument = new SelectorDomainItemArgument(this.controller, this.argument, this.domain);
            selectorDomainItemArgument.appendTo(argumentEl);

            let nameEl = this.$el.find('.domain-item__value');
            let inputDomainItemValue = new InputDomainItemValue(this.controller, this.argument, this.domain);
            inputDomainItemValue.appendTo(nameEl);

            let removeEl = this.$el.find('.domain-item__remove');
            let buttonDeleteDomainItem = new ButtonDeleteDomainItem(this.controller, this.argument, this.domain);
            buttonDeleteDomainItem.appendTo(removeEl);
        }
    })

    return DomainItem;

});