odoo.define('report_manager.MenuItem', function(require){

    var WidgetBasic = require('report_manager.WidgetBasic');
    var CheckboxMenuItemCheck = require('report_manager.CheckboxMenuItemCheck');
    var InputMenuItemName = require('report_manager.InputMenuItemName');
    var SelectorMenuItemParent = require('report_manager.SelectorMenuItemParent');
    var SelectorMenuItemGroups = require('report_manager.SelectorMenuItemGroups');
    var SelectorMenuItemReportTypes = require('report_manager.SelectorMenuItemReportTypes');

    var MenuItem = WidgetBasic.extend({
        template: 'RConfigMenuItem',
        init: function(controller, menu){
            this._super(controller);
            this.menu = menu;
        },
        renderElement: function(){
            this._super.apply(this, arguments);
            let checkEl = this.$el.find('.menu-item__check');
            let checkboxMenuItemCheck = new CheckboxMenuItemCheck(this.controller, this.menu);
            checkboxMenuItemCheck.appendTo(checkEl);

            let nameEl = this.$el.find('.menu-item__name');
            let inputMenuItemName = new InputMenuItemName(this.controller, this.menu);
            inputMenuItemName.appendTo(nameEl);

            let parentEl = this.$el.find('.menu-item__parent');
            let selectorMenuItemParent = new SelectorMenuItemParent(this.controller, this.menu);
            selectorMenuItemParent.appendTo(parentEl);

            let groupEl = this.$el.find('.menu-item__groups');
            let selectorMenuItemGroups = new SelectorMenuItemGroups(this.controller, this.menu);
            selectorMenuItemGroups.appendTo(groupEl);

            let reportTypesEl = this.$el.find('.menu-item__report-types');
            let selectorMenuItemReportTypes = new SelectorMenuItemReportTypes(this.controller, this.menu);
            selectorMenuItemReportTypes.appendTo(reportTypesEl);
        }
    })


    return MenuItem;

});