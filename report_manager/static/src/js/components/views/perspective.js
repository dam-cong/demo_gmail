odoo.define('report_manager.Perspective', function(require){

    var WidgetBasic = require('report_manager.WidgetBasic');
    var Action = require('report_manager.Action');

    var rpc = require('web.rpc');


    var Perspective = WidgetBasic.extend({
        template: 'RConfigPerspective',
        name: 'perspective',
        init: function(tab){
            this.tab = tab;
            this._super(tab);
            this.data = [];
            this.isInit = false;
            let types = [Action.UPDATE_DISPLAY_TAB];
            this.store.subscribe(this, 'reloadPerspective', types);
        },
        renderElement: function(){
            this._super.apply(this, arguments);
            this.initPerspective();
        },
        initPerspective: function(){
            if(!this.tab.isTabDisplay()){
                return;
            }
            this.isInit = true;
            document.dispatchEvent(new CustomEvent('InitPerspectiveView', {bubbles: true, detail: []}));
        },
        reloadPerspective: function(){
            this.loadPerspective();
        },
        updatePerspective: function(){
            if(!this.tab.isTabDisplay()){
                return;
            }
            let configs = this.store.getState().configs || {};
            let sql_columns = configs.sql_columns || [];
            let dataCopy = [];
            this.data.forEach(function (item) {
                let newItem = {}
                sql_columns.forEach(function (col) {
                    newItem[col.name] = item[col.key];
                });
                dataCopy.push(newItem);
            });
            // let event = 'UpdatePerspectiveView'
            // if(!this.isInit){
            //     this.isInit = true;
            let event = 'InitPerspectiveView';
            // }
            document.dispatchEvent(new CustomEvent(event, {bubbles: true, detail: dataCopy}));
        },
        loadPerspective: function(){
            if(!this.tab.isTabDisplay()){
                return;
            }
            let self = this;
            let params = this.getFiltersParams();
            let config_id = this.store.getState().id;
            let opts = [false, false, false, false];
            $.blockUI({'message': '<img src="/web/static/src/img/spin.png" class="fa-pulse"/>'});
            rpc.query({
                model: 'report.config',
                method: 'exec_query',
                args: [[config_id], params, opts]
            }).then(function(response){
                if(Array.isArray(response)){
                    self.data = response;
                    self.updatePerspective();
                }
                $.unblockUI();
            }).then(function(){
                $.unblockUI();
            });
        }
    });

    return Perspective;

});