odoo.define('report_manager.SqlColumnItem', function(require){

    var WidgetBasic = require('report_manager.WidgetBasic');
    var InputSqlColumnItemName = require('report_manager.InputSqlColumnItemName');
    var SelectorSqlColumnItemGroups = require('report_manager.SelectorSqlColumnItemGroups');

    var SqlColumnItem = WidgetBasic.extend({
        template: 'RConfigSqlColumnItem',
        init: function(controller, column){
            this.column = column;
            this._super(controller);
        },
        renderElement: function(){
            this._super.apply(this, arguments);

            let keyEl = this.$el.find('.column-item__key');
            keyEl.html(this.column.key);

            let nameEl = this.$el.find('.column-item__name');
            let inputSqlColumnItemName = new InputSqlColumnItemName(this.controller, this.column);
            inputSqlColumnItemName.appendTo(nameEl);

            let groupsEl = this.$el.find('.column-item__groups');
            let selectorSqlColumnItemGroups = new SelectorSqlColumnItemGroups(this.controller, this.column);
            selectorSqlColumnItemGroups.appendTo(groupsEl);
        }
    })

    return SqlColumnItem;

});