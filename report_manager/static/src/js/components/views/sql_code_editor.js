odoo.define('report_manager.SqlCodeEditor', function(require) {

    var WidgetBasic = require('report_manager.WidgetBasic');

    var SqlCodeEditor = WidgetBasic.extend({
        template: 'RConfigSqlCodeEditor',
        old_sql: null,
        sql: '',
        init: function(controller){
            this.editor = null;
            this._super.apply(this, arguments);
        },
        renderElement: function(){
            this.refreshSqlData();
            this._super.apply(this, arguments);
        },
        start: function(){
            this.initEditor();
            this._super.apply(this, arguments);
        },
        refreshEditor: function(){
            this.refreshSqlData();
            this.$el.css({'height': '100%'});
            if(this.sql !== this.old_sql){
                this.editor.setValue(this.sql);
                this.old_sql = this.sql;
            }
        },
        refreshSqlData: function(){
            try {
                let configs = this.store.getState().configs
                this.sql = '';
                if(configs.hasOwnProperty('sql')){
                    this.sql = decodeURIComponent(atob(configs.sql));
                }
            } catch (e) {
            }
        },
        initEditor: function(){
            this.editor = ace.edit(this.$el.find('#sql-code-editor')[0]);
            this.editor.session.setMode("ace/mode/sql");
            this.editor.setOptions({
                highlightGutterLine: true,
                showLineNumbers: true,
                showGutter: true,
                displayIndentGuides: true,
                theme: 'ace/theme/ambiance'
            });
            this.editor.setValue(this.sql);
        },
    });

    return SqlCodeEditor;

});