odoo.define('report_manager.BirtReport', function(require){

    var WidgetBasic = require('report_manager.WidgetBasic');
    var Action = require('report_manager.Action');

    var BirtReport = WidgetBasic.extend({
        template: 'RMBirtReportView',
        name: 'birt_report',
        link: '',
        init: function(tab){
            this.tab = tab;
            this._super(tab);
            let types = [Action.APPLY_FILTER];
            this.store.subscribe(this, 'reloadView', types);
        },
        reloadView: function(){
            if(!this.tab.isTabDisplay()){
                return;
            }
            let birt = this.store.getState().configs.birt || {};
            if(birt.method.value === 'get'){
                this.reloadViewWithGetMethod();
            } else {
                this.reloadViewWithPostMethod();
            }
        },
        reloadViewWithGetMethod: function () {
            let birt = this.store.getState().configs.birt || {};
            let src = birt.link + this.generateQueryString();
            this.$el.attr('src', src);
        },
        generateQueryString: function(){
            let filters = this.store.getState().configs.filters;
            let params = this.store.getState().configs.params;
            let query_str = '';

            for(let p_i = 0; p_i < params.length; p_i++){
                let param = params[p_i];
                for(let f_i = 0; f_i < filters.length; f_i++){
                    let filter = filters[f_i];
                    if(param.argument.id === filter.id){
                        query_str += '&' + param.key + '=' + filter.value;
                        break;
                    }
                }
            }
            return query_str;
        },
        reloadViewWithPostMethod: function () {
            let birt = this.store.getState().configs.birt || {};
            let form = document.createElement('form');
            form.setAttribute('target', this.name);
            form.setAttribute("action", birt.link);
            let filters = this.store.getState().configs.filters;
            let params = this.store.getState().configs.params;
            for(let p_i = 0; p_i < params.length; p_i++){
                let param = params[p_i];
                for(let f_i = 0; f_i < filters.length; f_i++){
                    let filter = filters[f_i];
                    if(param.argument.id === filter.id){
                        let input = document.createElement('input');
                        input.setAttribute('type', 'hidden');
                        input.setAttribute('name', param.key);
                        input.setAttribute('value', filter.value);
                        form.appendChild(input);
                        break;
                    }
                }
            }
            form.submit().remove();
        }
    });

    return BirtReport;

});