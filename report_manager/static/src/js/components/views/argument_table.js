odoo.define('report_manager.ArgumentTable', function(require){

    var WidgetBasic = require('report_manager.WidgetBasic');
    var ArgumentItem = require('report_manager.ArgumentItem');
    var Action = require('report_manager.Action');

    var ArgumentTable = WidgetBasic.extend({
        template: 'RConfigArgumentTable',
        init: function(controller, fragment){
            this._super(controller);
            this.fragment = fragment;
            let types = [Action.ADD_ARGUMENT_ITEM,
                Action.DELETE_ARGUMENT_ITEM,
                Action.UPDATE_DISPLAY_FRAGMENT]
            this.store.subscribe(this, 'reRenderArgumentItems', types);
        },
        renderElement: function(){
            this._super.apply(this, arguments);
            this.renderArgumentItems();
        },
        reRenderArgumentItems: function(){
            if(this.fragment.isFragmentDisplay()){
                this.renderArgumentItems();
            }
        },
        renderArgumentItems: function(){
            let self = this;
            let fragmentListEl = this.$el.find('tbody')
            let configs = this.store.getState().configs
            let arguments = configs.hasOwnProperty('arguments') ? configs.arguments : [];
            fragmentListEl.html('');
            arguments.forEach(function(argument){
                let item = new ArgumentItem(self.controller, argument);
                item.appendTo(fragmentListEl);
            });
        }
    });

    return ArgumentTable;

});