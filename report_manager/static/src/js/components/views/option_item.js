odoo.define('report_manager.OptionItem', function(require){

    var WidgetBasic = require('report_manager.WidgetBasic');
    var InputOptionItemValue = require('report_manager.InputOptionItemValue');
    var InputOptionItemName = require('report_manager.InputOptionItemName');
    var ButtonDeleteOptionItem = require('report_manager.ButtonDeleteOptionItem');
    var RadioboxOptionItemDefault = require('report_manager.RadioboxOptionItemDefault');

    var OptionItem = WidgetBasic.extend({
        template: 'RConfigOptionItem',
        init: function(controller, argument, option){
            this.argument = argument;
            this.option = option;
            this._super(controller);
        },
        renderElement: function(){
            this._super.apply(this, arguments);

            let radioEl = this.$el.find('.option-item__default');
            let radioboxOptionItemDefault = new RadioboxOptionItemDefault(this.controller, this.argument, this.option);
            radioboxOptionItemDefault.appendTo(radioEl);

            let valueEl = this.$el.find('.option-item__value');
            let inputOptionItemValue = new InputOptionItemValue(this.controller, this.argument, this.option);
            inputOptionItemValue.appendTo(valueEl);

            let nameEl = this.$el.find('.option-item__name');
            let inputOptionItemName = new InputOptionItemName(this.controller, this.argument, this.option);
            inputOptionItemName.appendTo(nameEl);

            let removeEl = this.$el.find('.option-item__remove');
            let buttonDeleteOptionItem = new ButtonDeleteOptionItem(this.controller, this.argument, this.option);
            buttonDeleteOptionItem.appendTo(removeEl);
        }
    })

    return OptionItem;

});