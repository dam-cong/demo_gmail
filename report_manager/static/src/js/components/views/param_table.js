odoo.define('report_manager.ParamTable', function(require){

    var WidgetBasic = require('report_manager.WidgetBasic');
    var ParamItem = require('report_manager.ParamItem');
    var Action = require('report_manager.Action');

    var ParamTable = WidgetBasic.extend({
        template: 'RConfigParamTable',
        init: function(controller, fragment){
            this._super(controller);
            this.fragment = fragment;
            let types = [Action.ADD_PARAM_ITEM, Action.DELETE_PARAM_ITEM, Action.UPDATE_DISPLAY_FRAGMENT];
            this.store.subscribe(this, 'reRenderParamItems', types);
        },
        renderElement: function(){
            this._super.apply(this, arguments);
            this.renderParamItems();
        },
        reRenderParamItems: function(){
            if(this.fragment.isFragmentDisplay()){
                this.renderParamItems();
            }
        },
        renderParamItems: function(){
            let self = this;
            let fragmentListEl = this.$el.find('tbody')
            let configs = this.store.getState().configs
            let params = configs.hasOwnProperty('params') ? configs.params : [];
            fragmentListEl.html('');
            params.forEach(function(param){
                let item = new ParamItem(self.controller, param);
                item.appendTo(fragmentListEl);
            });
        }
    });

    return ParamTable;

});