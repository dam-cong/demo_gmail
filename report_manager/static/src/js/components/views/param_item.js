odoo.define('report_manager.ParamItem', function(require){

    var WidgetBasic = require('report_manager.WidgetBasic');
    var InputParamItemName = require('report_manager.InputParamItemName');
    var InputParamItemKey = require('report_manager.InputParamItemKey');
    var SelectorParamItemArgument = require('report_manager.SelectorParamItemArgument');
    var CheckboxParamItemCheck = require('report_manager.CheckboxParamItemCheck');

    var ParamItem = WidgetBasic.extend({
        template: 'RConfigParamItem',
        init: function(controller, param){
            this._super(controller);
            this.param = param;
        },
        renderElement: function(){
            this._super.apply(this, arguments);
            let checkEl = this.$el.find('.param-item__check');
            let checkboxParamItemCheck = new CheckboxParamItemCheck(this.controller, this.param);
            checkboxParamItemCheck.appendTo(checkEl);

            let keyEl = this.$el.find('.param-item__key');
            let inputParamItemKey = new InputParamItemKey(this.controller, this.param);
            inputParamItemKey.appendTo(keyEl);

            let nameEl = this.$el.find('.param-item__name');
            let inputParamItemName = new InputParamItemName(this.controller, this.param);
            inputParamItemName.appendTo(nameEl);

            let argumentEl = this.$el.find('.param-item__argument');
            let selectorParamItemArgument = new SelectorParamItemArgument(this.controller, this.param);
            selectorParamItemArgument.appendTo(argumentEl);
        }
    })


    return ParamItem;

});