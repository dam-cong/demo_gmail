odoo.define('report_manager.ArgumentItem', function(require){

    var WidgetBasic = require('report_manager.WidgetBasic');
    var InputArgumentItemName = require('report_manager.InputArgumentItemName');
    var CheckboxArgumentItemCheck = require('report_manager.CheckboxArgumentItemCheck');
    var CheckboxArgumentItemRequire = require('report_manager.CheckboxArgumentItemRequire');

    var ArgumentItem = WidgetBasic.extend({
        template: 'RConfigArgumentItem',
        init: function(controller, argument){
            this.argument = argument;
            this._super(controller);
        },
        renderElement: function(){
            this._super.apply(this, arguments);

            let checkEl = this.$el.find('.argument-item__check');
            let checkboxArgumentItemCheck = new CheckboxArgumentItemCheck(this.controller, this.argument);
            checkboxArgumentItemCheck.appendTo(checkEl);

            let requireEl = this.$el.find('.argument-item__require');
            let checkboxArgumentItemRequire = new CheckboxArgumentItemRequire(this.controller, this.argument);
            checkboxArgumentItemRequire.appendTo(requireEl);

            let nameEl = this.$el.find('.argument-item__name');
            let inputArgumentItemName = new InputArgumentItemName(this.controller, this.argument);
            inputArgumentItemName.appendTo(nameEl);
        }
    })

    return ArgumentItem;

});