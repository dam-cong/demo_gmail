odoo.define('report_manager.MenuTable', function(require){

    var WidgetBasic = require('report_manager.WidgetBasic');
    var MenuItem = require('report_manager.MenuItem');
    var Action = require('report_manager.Action');

    var MenuTable = WidgetBasic.extend({
        template: 'RConfigMenuTable',
        init: function(controller, fragment){
            this._super(controller);
            this.fragment = fragment;
            let types = [Action.ADD_MENU_ITEM,
                Action.DELETE_MENU_ITEM,
                Action.UPDATE_DISPLAY_FRAGMENT,
                Action.UPDATE_STORE];
            this.store.subscribe(this, 'reRenderMenuItems', types);
        },
        renderElement: function(){
            this._super.apply(this, arguments);
            this.renderMenuItems();
        },
        reRenderMenuItems: function(){
            if(this.fragment.isFragmentDisplay()){
                this.renderMenuItems();
            }
        },
        renderMenuItems: function(){
            let self = this;
            let fragmentListEl = this.$el.find('tbody')
            let configs = this.store.getState().configs
            let params = configs.hasOwnProperty('menus') ? configs.menus : [];
            fragmentListEl.html('');
            params.forEach(function(param){
                let item = new MenuItem(self.controller, param);
                item.appendTo(fragmentListEl);
            });
        }
    });

    return MenuTable;

});