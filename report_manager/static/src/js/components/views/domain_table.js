odoo.define('report_manager.DomainTable', function(require){

    var WidgetBasic = require('report_manager.WidgetBasic');
    var DomainItem = require('report_manager.DomainItem');
    var Action = require('report_manager.Action');

    var DomainTable = WidgetBasic.extend({
        template: 'RConfigDomainTable',
        init: function(controller, argument, domains){
            this._super(controller);
            this.argument = argument;
            this.domains = domains;
            let types = [Action.ADD_DOMAIN_ITEM,
                Action.DELETE_DOMAIN_ITEM];
            this.store.subscribe(this, 'reRenderDomainTableItems', types);
        },
        renderElement: function(){
            this._super.apply(this, arguments);
            this.renderDomainTableItems();
        },
        renderDomainTableItems: function(){
            let self = this;
            let bodyEl = this.$el.find('tbody');
            bodyEl.html('');
            this.domains.forEach(function(domain){
                let item = new DomainItem(self.controller, self.argument, domain);
                item.appendTo(bodyEl);
            });
        },
        reRenderDomainTableItems: function(){
            let configs = this.store.getState().configs;
            let argument = configs.selected_argument;
            let arguments = configs.arguments.filter(function(arg){
                return arg.id === argument.id;
            })
            this.argument = arguments[0];
            this.domains = this.argument.domains;
            this.renderDomainTableItems();
        }
    });

    return DomainTable;

});
