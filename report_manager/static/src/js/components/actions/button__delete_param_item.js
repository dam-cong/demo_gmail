odoo.define('report_manager.ButtonDeleteParamItem', function(require){

    var ButtonBasic = require('report_manager.ButtonBasic');
    var Action = require('report_manager.Action');

    var ButtonDeleteParamItem = ButtonBasic.extend({
        icon: 'fa fa-trash',
        label: false,
        onActionClick: function(event){
            this.store.dispatch({
                type: Action.DELETE_PARAM_ITEM
            });
        }
    });

    return ButtonDeleteParamItem;
});