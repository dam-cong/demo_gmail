odoo.define('report_manager.SelectorDomainItemArgument', function(require){

    var SelectorBasic = require('report_manager.SelectorBasic');
    var Action = require('report_manager.Action');

    var SelectorDomainItemArgument = SelectorBasic.extend({
        label: false,
        input_name: 'domain_argument',
        require: true,
        init: function(controller, argument, domain){
            this.argument = argument;
            this.selected_item = domain.argument || {};
            this.domain = domain;
            this._super(controller);
            this.input_name += '_'+domain.id;
            this.source = this.getSourceData();
        },
        onClickSelectorItem: function(event){
            let self = this;
            this._super.apply(this, arguments);
            this.store.dispatch({
                type: Action.UPDATE_DOMAIN_ITEM_ARGUMENT,
                id: self.argument.id,
                domain_id: self.domain.id,
                argument: self.selected_item
            });
        },
        getSourceData: function(search_name){
            search_name = search_name || '';
            let self = this;
            let data = this.store.getState().configs.arguments || [];
            data = data.filter(function(item){
                return self.removeUnicode(item.label).indexOf(search_name) >= 0;
            });
            return data;
        }
    });

    return SelectorDomainItemArgument;

});