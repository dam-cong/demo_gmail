odoo.define('report_manager.SelectorBirtReportMethod', function(require){

    var core = require('web.core');
    var SelectorBasic = require('report_manager.SelectorBasic');
    var Action = require('report_manager.Action');

    var _lt = core._lt;

    var SelectorBirtReportMethod = SelectorBasic.extend({
        label: _lt('Birt method'),
        input_name: 'birt_report_method',
        class: 'flex-column flex-wrap',
        require: true,
        init: function(controller, method){
            this.selected_item = method || {};
            this._super(controller);
            this.source = this.getSourceData();
        },
        onClickSelectorItem: function(event){
            let self = this;
            this._super.apply(this, arguments);
            this.store.dispatch({
                type: Action.UPDATE_BIRT_REPORT_METHOD,
                method: self.selected_item
            });
        },
        getSourceData: function(){
            return [{id: 'get', name: 'Get', value: 'get'},
                    {id: 'post', name: 'Post', value: 'post'}]
        }
    });

    return SelectorBirtReportMethod;

});