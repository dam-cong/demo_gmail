odoo.define('report_manager.ButtonToggleReportView', function(require){

    var ButtonBasic = require('report_manager.ButtonBasic');
    var Action = require('report_manager.Action');

    var ButtonToggleReportView = ButtonBasic.extend({
        icon: 'fa fa-arrow-left',
        label: false,
        class: 'btn-toggle',
        onActionClick: function(event){
            this.$el.find('i').toggleClass('fa-arrow-left fa-arrow-right');
            this.store.dispatch({
                type: Action.TOGGLE_REPORT_VIEW_CONTENT
            });
        }
    });

    return ButtonToggleReportView;
});