odoo.define('report_manager.InputFilterDatetime', function(require){

    var InputFilterDate = require('report_manager.InputFilterDate');

    var InputFilterDatetime = InputFilterDate.extend({
        input_name: 'filter_datetime',
        type_of_date: 'datetime'
    });

    return InputFilterDatetime;

});