odoo.define('report_manager.SelectorMenuItemParent', function(require){

    var SelectorBasic = require('report_manager.SelectorBasic');
    var Action = require('report_manager.Action');
    var rpc = require('web.rpc');

    var SelectorMenuItemParent = SelectorBasic.extend({
        label: false,
        input_name: 'menu_parent',
        init: function(controller, menu){
            this.selected_item = menu.parent || {};
            this.menu = menu;
            this._super(controller);
            this.source = this.getSourceParent;
            this.input_name += '_'+menu.id;
        },
        renderElement: function(){
            this._super(arguments);
        },
        onClickSelectorItem: function(event){
            let self = this;
            this._super.apply(this, arguments);
            this.store.dispatch({
                type: Action.UPDATE_MENU_ITEM_PARENT,
                id: self.menu.id,
                parent: self.selected_item
            });
        },
        getSourceParent: function(name){
            let self = this;
            let def = $.Deferred();
            if(name === '' || !name){
                return def.resolve([]);
            }
            let context = this.controller.context;
            rpc.query({
                model: 'ir.ui.menu',
                method: 'name_search',
                kwargs: {
                    context: context,
                    name: name,
                    args: ['|', ['parent_id', 'ilike', name]],
                    operator: 'ilike',
                    limit: self.limit
                },
            }, {
                shadow: true,
            }).then(function(response){
                if(Array.isArray(response)){
                    response = response.map(function(item){
                        return {
                            id: item[0],
                            name: item[1],
                            value: item[0],
                            check: false
                        };
                    });
                    return def.resolve(response);
                }
                return def.reject([]);
            });
            return def.promise();
        }
    });

    return SelectorMenuItemParent;

});