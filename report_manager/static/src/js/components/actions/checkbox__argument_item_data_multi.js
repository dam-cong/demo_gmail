odoo.define('report_manager.CheckboxArgumentItemDataMulti', function(require){

    var CheckboxBasic = require('report_manager.CheckboxBasic');
    var Action = require('report_manager.Action');

    var CheckboxArgumentItemDataMulti = CheckboxBasic.extend({
        init: function(controller, argument, options){
            this._super(controller, options);
            this.checked = argument.multi;
            this.argument = argument;
        },
        onToggleCheckbox: function(event){
            let self = this;
            this._super.apply(this, arguments);
            this.store.dispatch({
                type: Action.UPDATE_ARGUMENT_ITEM_DATA_MULTI,
                id: self.argument.id,
                multi: self.checked
            });
        },
    });

    return CheckboxArgumentItemDataMulti;

});