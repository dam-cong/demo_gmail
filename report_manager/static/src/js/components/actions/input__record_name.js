odoo.define('report_manager.InputRecordName', function(require){

    var core = require('web.core');
    var InputBasic = require('report_manager.InputBasic');
    var Action = require('report_manager.Action');

    var _lt = core._lt;

    var InputRecordName = InputBasic.extend({
        label: false,
        input_name: 'record_name',
        placeholder: _lt('Name'),
        class: 'record-name',
        init: function(controller){
            this._super(controller);
            let types = [Action.UPDATE_STORE];
            this.store.subscribe(this, 'updateValue', types);
        },
        updateValue: function(){
            let name = this.store.getState().name;
            this.$el.find('.rm__input').val(name || '');
            this.$el.find('.rm__input-preview').text(name || '');
        },
        renderElement: function(){
            this.value = this.store.getState().name || this.store.getState().display_name;
            this._super(arguments);
        },
        onInputChange: function(event){
            this._super.apply(this, arguments);
            var record_name = $(event.currentTarget).val();
            this.store.dispatch({type: Action.UPDATE_RECORD_NAME,
                                 name: record_name});
            this.controller.setTitle(record_name);
        },
    });

    return InputRecordName;

});