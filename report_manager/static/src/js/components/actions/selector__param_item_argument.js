odoo.define('report_manager.SelectorParamItemArgument', function(require){

    var SelectorBasic = require('report_manager.SelectorBasic');
    var Action = require('report_manager.Action');

    var SelectorParamItemArgument = SelectorBasic.extend({
        label: false,
        input_name: 'param_argument',
        init: function(controller, param){
            this.selected_item = param.argument || {};
            this.param = param;
            this._super(controller);
            this.input_name += '_'+param.id;
            this.source = this.getSourceData();
        },
        onClickSelectorItem: function(event){
            let self = this;
            this._super.apply(this, arguments);
            this.store.dispatch({
                type: Action.UPDATE_PARAM_ITEM_ARGUMENT,
                id: self.param.id,
                argument: self.selected_item
            });
        },
        getSourceData: function(search_name){
            search_name = search_name || '';
            let self = this;
            let data = this.store.getState().configs.arguments || [];
            data = data.filter(function(item){
                return self.removeUnicode(item.label).indexOf(search_name) >= 0;
            });
            return data;
        }
    });

    return SelectorParamItemArgument;

});