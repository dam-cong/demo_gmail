odoo.define('report_manager.SelectorFilterModel', function(require){

    var SelectorBasic = require('report_manager.SelectorBasic');
    var Action = require('report_manager.Action');
    var rpc = require('web.rpc');

    var SelectorFilterModel = SelectorBasic.extend({
        label: false,
        input_name: 'filter_model',
        vertical: true,
        class: 'mb-3 w-100',
        require: false,
        error: false,
        init: function(action, arg,  props){
            this.arg = arg;
            this.action = action;
            this.multi = arg.multi || false;
            this.store = action.store;
            this.label = arg.name;
            this.require = arg.require;
            this.input_name += 'filter_' + _.random(0, 2147483647);
            this.source = this.getSourceModel;
            this._super(action, props);
            this.selected_items = [];
            this.selected_item = {};
            this.store.subscribe(this, 'updateViewState', [Action.UPDATE_ARGUMENTS]);
        },
        updateViewState: function(){
            if(!this.arg.require){
                return;
            }
            let self = this;
            let arguments = this.store.getState().configs.arguments;
            arguments.forEach(function(argument){
                if(argument.id === self.arg.id){
                    self.arg.error = argument.error;
                    if(argument.error){
                        self.$el.addClass('is_error');
                    } else {
                        self.$el.removeClass('is_error');
                    }
                }
            });
        },
        onClickSelectorItem: function(event){
            let self = this;
            this._super.apply(this, arguments);
            if(self.multi){
                var value = [];
                self.selected_items.forEach(function (item) {
                    value.push(item.id);
                });
                if(this.arg.to_string){
                    value = value.join(',');
                }
            } else {
                var value = self.selected_item ? self.selected_item.id : null;
            }
            this.store.dispatch({
                type: Action.UPDATE_FILTER_ITEM,
                id: self.arg.id,
                value: value
            });
        },
        onRemoveSelectedItem: function(event){
            let self = this;
            this._super.apply(this, arguments);
            if(self.multi){
                var value = [];
                self.selected_items.forEach(function (item) {
                    value.push(item.id);
                });
                if(this.arg.to_string){
                    value = value.join(',');
                }
            } else {
                var value = self.selected_item ? self.selected_item.id : null;
            }
            this.store.dispatch({
                type: Action.UPDATE_FILTER_ITEM,
                id: self.arg.id,
                value: value
            });
        },
        getSourceModel: function(name){
            let self = this;
            let def = $.Deferred();
            let context = this.action.context;
            let domain = this.getDomain();
            rpc.query({
                model: self.arg.model.model,
                method: 'name_search',
                kwargs: {
                    context: context,
                    name: name,
                    args: domain,
                    operator: 'ilike',
                    limit: self.limit
                },
            }, {
                shadow: true,
            }).then(function(models){
                if(Array.isArray(models)){
                    models = models.map(function(item){
                        return {
                            id: item[0],
                            name: item[1],
                            value: item[0],
                            check: false
                        };
                    });
                    return def.resolve(models);
                }
                return def.reject([]);
            });
            return def.promise();
        },
        getDomain: function(){
            if(!this.arg.hasOwnProperty('domains') || !Array.isArray(this.arg.domains)){
                return [];
            }
            let result = [];
            let domains = this.arg.domains;
            let filters = this.store.getState().configs.filters;
            domains.forEach(function(domain){
                if(!domain.argument || _.isEmpty(domain.argument)){
                    result.push([domain.field, domain.operator, domain.value]);
                } else {
                    filters.forEach(function(filter){
                        if(filter.id === domain.argument.id){
                            result.push([domain.field, domain.operator, filter.value]);
                        }
                    });
                }
            });
            return result;
        }
    });

    return SelectorFilterModel;

});