odoo.define('report_manager.ButtonDeleteOptionItem', function(require){

    var ButtonBasic = require('report_manager.ButtonBasic');
    var Action = require('report_manager.Action');

    var ButtonDeleteOptionItem = ButtonBasic.extend({
        icon: 'fa fa-trash',
        label: false,
        init: function(controller, argument, option){
            this._super.apply(this, arguments);
            this.argument = argument;
            this.option = option;
        },
        onActionClick: function(event){
            let self = this;
            this.store.dispatch({
                type: Action.DELETE_OPTION_ITEM,
                id: self.argument.id,
                option_id: self.option.id
            });
        }
    });

    return ButtonDeleteOptionItem;
});