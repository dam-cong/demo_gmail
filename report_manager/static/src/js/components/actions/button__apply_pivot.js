odoo.define('report_manager.ButtonApplyPivot', function(require){

    var core = require('web.core');
    var ButtonBasic = require('report_manager.ButtonBasic');
    var Action = require('report_manager.Action');
    var ReportNotification = require('report_manager.ReportNotification');

    var _lt = core._lt;

    var ButtonApplyPivot = ButtonBasic.extend({
        icon: 'fa fa-check',
        label: _lt('Apply pivot'),
        init: function(controller){
            this._super(controller);
            this.pivot_config = '';
        },
        onActionClick: function(event){
            event.stopPropagation();
            let pivot_config = this.getPivotConfig();
            this.store.dispatch({
                type: Action.APPLY_PIVOT_CONFIG,
                pivot_config: pivot_config
            });
            this.call('notification', 'notify', {type: 'notify',
                                                 Notification: ReportNotification,
                                                 title: _lt('Notify'),
                                                 message: _lt('Apply this pivot config')})
        },
        getPivotConfig: function(){
            let config = $('#pivot-design').data("pivotUIOptions");
            let config_copy = JSON.parse(JSON.stringify(config));
            delete config_copy["aggregators"];
            delete config_copy["renderers"];
            return config_copy;
        }
    });

    return ButtonApplyPivot;
});