odoo.define('report_manager.SelectorFilterOption', function(require){

    var SelectorBasic = require('report_manager.SelectorBasic');
    var Action = require('report_manager.Action');

    var SelectorFilterOption = SelectorBasic.extend({
        label: false,
        input_name: 'filter_option',
        vertical: true,
        class: 'mb-3 w-100',
        require: false,
        error: false,
        init: function(action, arg, props){
            this.arg = arg;
            this.action = action;
            this.store = action.store;
            this.label = arg.name;
            this.require = arg.require;
            this._super(action, props);
            this.source = this.arg.options || [];
            this.input_name += 'filter_'+_.random(0, 2147483647);
            this.store.subscribe(this, 'updateViewState', [Action.UPDATE_ARGUMENTS]);
        },
        updateViewState: function(){
            if(!this.arg.require){
                return;
            }
            let self = this;
            let arguments = this.store.getState().configs.arguments;
            arguments.forEach(function(argument){
                if(argument.id === self.arg.id){
                    self.arg.error = argument.error;
                    if(argument.error){
                        self.$el.addClass('is_error');
                    } else {
                        self.$el.removeClass('is_error');
                    }
                }
            });
        },
        onClickSelectorItem: function(event){
            let self = this;
            this._super.apply(this, arguments);
            let value = self.selected_item ? self.selected_item.value : null;
            this.store.dispatch({
                type: Action.UPDATE_FILTER_ITEM,
                id: self.arg.id,
                value: value
            });
        }
    });

    return SelectorFilterOption;

});