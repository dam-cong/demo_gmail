odoo.define('report_manager.ButtonApplyFilter', function(require){

    var core = require('web.core');
    var ButtonBasic = require('report_manager.ButtonBasic');
    var Action = require('report_manager.Action');

    var _lt = core._lt;

    var ButtonApplyFilter = ButtonBasic.extend({
        icon: 'fa fa-check',
        label: _lt('Apply filter'),
        class: 'w-100 mb-3',
        init: function(controller){
            this._super(controller);
            this.sql = '';
        },
        onActionClick: function(event){
            event.stopPropagation();
            let filters = this.getFilters();
            if(!filters){
                return;
            }
            this.store.dispatch({
                type: Action.APPLY_FILTER,
                filters: filters
            })
        },
        getFilters: function(){
            var filters = this.store.getState().configs.filters || [];
            var arguments = this.store.getState().configs.arguments || [];
            var return_filter = true;
            let self = this;
            arguments.forEach(function(argument){
                let exist = false;
                argument.error = false;
                filters.forEach(function(filter){
                    if(filter.id === argument.id){
                        if(argument.require && filter.value === self.getArgumentDefaultValue(argument)){
                            return_filter = false;
                            argument.error = true;
                        }
                        exist = true;
                    }
                });
                if(!exist){
                    argument.error = argument.require;
                    if(argument.require){
                        return_filter = false;
                    }
                    let default_value = self.getArgumentDefaultValue(argument);
                    filters.push({
                        id: argument.id,
                        value: default_value
                    });
                }
            });
            this.store.dispatch({
                type: Action.UPDATE_ARGUMENTS,
                arguments: arguments
            });
            return return_filter ? filters : false;
        },
        getArgumentDefaultValue: function(argument){
            switch(argument.data_type.value){
                case 'string':
                case 'date':
                case 'datetime':
                case 'options':
                case 'number':
                    return '';
                case 'model':
                    if(argument.multi){
                        if(argument.to_string){
                            return ''
                        }
                        return [];
                    }
                    return '';
                default:
                    return '';
            }
        }
    });

    return ButtonApplyFilter;
});