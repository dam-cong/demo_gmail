odoo.define('report_manager.SelectorMenuItemGroups', function(require){

    var SelectorBasic = require('report_manager.SelectorBasic');
    var Action = require('report_manager.Action');
    var rpc = require('web.rpc');

    var SelectorMenuItemGroups = SelectorBasic.extend({
        label: false,
        input_name: 'menu_groups',
        multi: true,
        init: function(controller, menu){
            this.selected_items = menu.groups || [];
            this.menu = menu;
            this._super(controller);
            this.source = this.getSourceGroups;
            this.input_name += '_'+menu.id;
        },
        renderElement: function(){
            this._super(arguments);
        },
        onClickSelectorItem: function(event){
            let self = this;
            this._super.apply(this, arguments);
            this.store.dispatch({
                type: Action.UPDATE_MENU_ITEM_GROUPS,
                id: self.menu.id,
                groups: self.selected_items
            });
        },
        getSourceGroups: function(name){
            let self = this;
            let def = $.Deferred();
            let context = this.controller.context;
            rpc.query({
                model: 'res.groups',
                method: 'name_search',
                kwargs: {
                    context: context,
                    name: name,
                    operator: 'ilike',
                    limit: self.limit
                },
            }, {
                shadow: true,
            }).then(function(response){
                if(Array.isArray(response)){
                    response = response.map(function(item){
                        return {
                            id: item[0],
                            name: item[1],
                            value: item[0],
                            check: false
                        };
                    });
                    return def.resolve(response);
                }
                return def.reject([]);
            });
            return def.promise();
        }
    });

    return SelectorMenuItemGroups;

});