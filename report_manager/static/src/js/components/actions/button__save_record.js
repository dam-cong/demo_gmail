odoo.define('report_manager.ButtonSaveRecord', function(require){

    var core = require('web.core');
    var ButtonBasic = require('report_manager.ButtonBasic');
    var ReportNotification = require('report_manager.ReportNotification');

    var _lt = core._lt;

    var ButtonSaveRecord = ButtonBasic.extend({
        icon: 'fa fa-floppy-o',
        label: _lt('Save'),
        init: function(parent){
            this._super(parent);
        },
        onActionClick: function(event){
            let self = this;
            this.controller.prepareDataToSave();
            this.controller.saveRecord().then(function(){
                self.call('notification', 'notify', {type: 'success',
                                                     Notification: ReportNotification,
                                                     title: _lt('Notify'),
                                                     message: _lt('Save data success')});
            });
        }
    });

    return ButtonSaveRecord;

});