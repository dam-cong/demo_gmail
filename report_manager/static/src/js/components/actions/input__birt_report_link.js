odoo.define('report_manager.InputBirtReportLink', function(require){

    var core = require('web.core');
    var InputBasic = require('report_manager.InputBasic');
    var Action = require('report_manager.Action');

    var _lt = core._lt;

    var InputBirtReportLink = InputBasic.extend({
        label: _lt('Birt link'),
        input_name: 'birt_link',
        placeholder: _lt('Birt link'),
        only_edit_mode: true,
        mode: 'edit',
        class: 'w-100',
        init: function(controller, link){
            this.value = link;
            this._super(controller);
        },
        onInputChange: function(event){
            this._super.apply(this, arguments);
            let value = $(event.currentTarget).val();
            this.store.dispatch({
                type: Action.UPDATE_BIRT_REPORT_LINK,
                link: value
            });
        }
    });

    return InputBirtReportLink;

});