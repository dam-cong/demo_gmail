odoo.define('report_manager.InputOptionItemName', function(require){

    var core = require('web.core');
    var InputBasic = require('report_manager.InputBasic');
    var Action = require('report_manager.Action');

    var _lt = core._lt;

    var InputOptionItemName = InputBasic.extend({
        label: false,
        input_name: 'option_name',
        placeholder: _lt('Option name'),
        init: function(controller, argument, option){
            this._super(controller);
            this.input_name = option.id;
            this.argument = argument;
            this.option = option;
            this.value = option.name;
        },
        onInputChange: function(event){
            this._super.apply(this, arguments);
            let self = this;
            let value = $(event.currentTarget).val();
            this.store.dispatch({
                type: Action.UPDATE_OPTION_ITEM_NAME,
                id: self.argument.id,
                option_id: self.option.id,
                name: value
            });
        }
    });

    return InputOptionItemName;

});