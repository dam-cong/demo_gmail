odoo.define('report_manager.RadioboxOptionItemDefault', function(require){

    var RadioboxBasic = require('report_manager.RadioboxBasic');
    var Action = require('report_manager.Action');

    var RadioboxOptionItemDefault = RadioboxBasic.extend({
        init: function(controller, argument, option){
            this.name = 'option_'+argument.id;
            this._super(controller);
            this.checked = option.default;
            this.argument = argument;
            this.option = option;
        },
        onClickRadiobox: function(event){
            let self = this;
            this._super.apply(this, arguments);
            this.store.dispatch({
                type: Action.UPDATE_OPTION_ITEM_DEFAULT,
                id: self.argument.id,
                option_id: self.option.id
            });
        },
    });

    return RadioboxOptionItemDefault;

});