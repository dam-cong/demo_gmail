odoo.define('report_manager.RadioboxBasic', function(require) {

    var WidgetBasic = require('report_manager.WidgetBasic');

    var RadioboxBasic = WidgetBasic.extend({
        template: 'RMRadiobox',
        events: {
            'click .rm__radiobox i': 'onClickRadiobox'
        },
        checked: false,
        label: false,
        class: '',
        name: '',
        help: '',
        init: function(controller, props){
            props = props || {};
            this._super.apply(this, arguments);
            this.mapPropsToState(props)
        },
        onClickRadiobox: function(event){
            event.stopPropagation();
            let currentRadioBox = $(event.currentTarget).closest('.rm__radiobox');
            $('span[name="'+this.name+'"]').not(currentRadioBox).removeClass('rm__radiobox--checked');
            $('span[name="'+this.name+'"]').not(currentRadioBox).find('i').removeClass('fa-dot-circle-o').addClass('fa-circle-o');
            this.checked = true;
            if(this.checked){
                this.$el.find('.rm__radiobox').addClass('rm__radiobox--checked');
                this.$el.find('i').addClass('fa-dot-circle-o').removeClass('fa-circle-o');
            } else {
                this.$el.find('.rm__radiobox').removeClass('rm__radiobox--checked');
                this.$el.find('i').removeClass('fa-dot-circle-o').addClass('fa-circle-o');
            }
        }
    });

    return RadioboxBasic;

});