odoo.define('report_manager.ButtonBasic', function(require){

    var WidgetBasic = require('report_manager.WidgetBasic');

    var ButtonBasic = WidgetBasic.extend({
        template: 'RMButton',
        events: {
            'click': 'onActionClick'
        },
        icon: 'fa-info',
        label: '',
        active: false,
        class: '',
        init: function(controller){
            this._super.apply(this, arguments);
        },
        onActionClick: function(event){
            event.stopPropagation();
        },
    });

    return ButtonBasic;

});