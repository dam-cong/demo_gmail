odoo.define('report_manager.SelectorBasic', function(require){

    var core = require('web.core');
    var WidgetBasic = require('report_manager.WidgetBasic');

    var _lt = core._lt;

    var SelectorBasic = WidgetBasic.extend({
        template: 'RMSelector',
        events: {
            'click .rm__selected-label': 'onToggleSelectorList',
            'click .rm__selected-labels': 'onToggleSelectorList',
            'click .rm__selected-labels .tag-remove': 'onRemoveSelectedItem',
            'click .rm__selector-item': 'onClickSelectorItem',
            'keyup .rm__selector-search-input': 'onKeyupSearchInput',
        },
        label: false,
        input_name: false,
        value: '',
        help: '',
        source: null,
        current_source: [],
        multi: false,
        vertical: false,
        class: '',
        placeholder: _lt('Select...'),
        selected_item: {},
        selected_items: [],
        is_show_selector_data: false,
        search_input_name: 'search_selector_item_'+_.random(0, 2147483647),
        search_input_value: '',
        search_input_placeholder: _lt('Search...'),
        typingTimer: null,
        limit: 10,
        DONE_TYPING_INTERVAL: 500,
        require: false,
        init: function(controller, props){
            props = props || {};
            this.mapPropsToState(props);
            this._super.apply(this, arguments);
            if(this.selected_item === undefined){
                this.selected_item = {}
            }
        },
        renderElement: function(){
            this._super.apply(this, arguments);
            this.renderSelectorItems();
        },
        start: function(){
            this._super.apply(this, arguments);
        },
        renderSelectorItems: function(search){
            if(typeof this.source === 'function'){
                this._renderSelectorItemsFromFunction(search);
            }
            if (Array.isArray(this.source)){
                this._renderSelectorItemsFromArray(search);
            }
        },
        _renderSelectorItemsFromFunction: async function(search){
            this.current_source = await this.source(search);
            this._renderSelectorItems();
        },
        _renderSelectorItemsFromArray: function(search){
            search = search || '';
            let self = this;
            this.current_source = this.source.filter(function(item){
                return self.removeUnicode(item.name).toLowerCase().indexOf(search) >= 0
            });
            this._renderSelectorItems();
        },
        _renderSelectorItems: function(){
            let html = '';
            if(!this.require){
                html += '<li value="" data-id="" class="rm__selector-item"></li>';
            }
            this.current_source.forEach(function(item){
                let check = item.check ? 'active' : '';
                html += '<li value="'+item.value+'" data-id="'+item.id+'" class="rm__selector-item '+check+'">'+item.name+'</li>';
            });
            this.$el.find('.rm__selector-list').html(html);
        },
        onToggleSelectorList: function(event){
            event.stopPropagation();
            if(this.is_show_selector_data){
                this.hideSelectorData();
            } else {
                this.showSelectorData();
            }
        },
        onClickSelectorItem: function(event){
            event.stopPropagation();
            this.setSelectedItem(event);
            this.hideSelectorData();
        },
        onKeyupSearchInput: function(event){
            event.stopPropagation();
            var self = this;
            this.search_input_value = $(event.currentTarget).val();
            clearTimeout(this.typingTimer);
            this.typingTimer = setTimeout(function(){
                self.renderSelectorItems(self.search_input_value);
            }, self.DONE_TYPING_INTERVAL);
        },
        onRemoveSelectedItem: function(event){
            event.stopPropagation();
            let itemId = $(event.currentTarget).attr('data-id');
            this.selected_items = this.selected_items.filter(function(item){
                return item.id != itemId;
            });
            this.setSelectedItemsLabel();
            this.hideSelectorData();
        },
        showSelectorData: function(){
            this.$el.find('.rm__selector-data').show(300);
            this.is_show_selector_data = true;
        },
        hideSelectorData: function(){
            this.$el.find('.rm__selector-data').hide(300);
            this.is_show_selector_data = false;
        },
        setSelectedItem: function(event){
            let self = this;
            let selected_item = null;
            let selectedItemId = $(event.currentTarget).attr('data-id');
            this.current_source.forEach(function(item){
                if(item.id == selectedItemId){
                    selected_item = item;
                }
            });
            self.selected_item = selected_item;

            if(!this.multi){
                this.setSelectedItemLabel();
                return;
            }

            let exists = this.selected_items.filter(function(item){
                return item.id == selectedItemId;
            });
            if(exists.length <= 0 && selectedItemId){
                this.selected_items.push(this.selected_item);
            }
            this.setSelectedItemsLabel();
        },
        setSelectedItemLabel: function(){
            let html = this.selected_item ? this.selected_item.name : '';
            this.$el.find('.rm__selected-label').html(html);
        },
        setSelectedItemsLabel: function(){
            let html = '';
            this.selected_items.forEach(function(item){
                html += '<p class="rm__selected-label-tag">'
                            + '<span class="tag-remove" data-id="'+item.id+'"><i class="fa fa-times"></i></span>'
                            + '<span class="tag-label">'+item.name+'</span>'
                        + '</p>';
            });
            this.$el.find('.rm__selected-labels').html(html);
        }
    });

    return SelectorBasic;

});