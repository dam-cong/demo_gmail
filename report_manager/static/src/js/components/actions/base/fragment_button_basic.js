odoo.define('report_manager.FragmentButtonBasic', function(require){

    var ButtonBasic = require('report_manager.ButtonBasic');

    var FragmentButtonBasic = ButtonBasic.extend({
        init: function(controller, fragment){
            this.fragment = fragment;
            this._super.apply(this, arguments);
        }
    });

    return FragmentButtonBasic;

});