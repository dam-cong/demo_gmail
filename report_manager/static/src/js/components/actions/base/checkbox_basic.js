odoo.define('report_manager.CheckboxBasic', function(require) {

    var WidgetBasic = require('report_manager.WidgetBasic');

    var CheckboxBasic = WidgetBasic.extend({
        template: 'RMCheckbox',
        events: {
            'click .rm__checkbox i': 'onToggleCheckbox'
        },
        checked: false,
        label: false,
        class: '',
        help: '',
        init: function(controller, props){
            props = props || {};
            this._super.apply(this, arguments);
            this.mapPropsToState(props)
        },
        onToggleCheckbox: function(event){
            event.stopPropagation();
            this.checked = !this.checked;
            if(this.checked){
                this.$el.find('.rm__checkbox').addClass('rm__checkbox--checked');
                this.$el.find('i').addClass('fa-check-square-o').removeClass('fa-square-o');
            } else {
                this.$el.find('.rm__checkbox').removeClass('rm__checkbox--checked');
                this.$el.find('i').removeClass('fa-check-square-o').addClass('fa-square-o');
            }
        }
    });

    return CheckboxBasic;

});