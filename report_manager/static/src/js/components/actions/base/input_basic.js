odoo.define('report_manager.InputBasic', function(require){

    var WidgetBasic = require('report_manager.WidgetBasic');

    var InputBasic = WidgetBasic.extend({
        template: 'RMInput',
        events: {
            'focus .rm__input': 'onInputFocus',
            'change .rm__input': 'onInputChange',
            'blur .rm__input': 'onInputBlur',
            'click .rm__input-preview': 'onInputValueClick'
        },
        icon: 'fa-info',
        label: false,
        input_name: false,
        placeholder: '',
        class: '',
        mode: 'readonly',
        value: '',
        clickCount: 0,
        type: 'text',
        only_edit_mode: false,
        require: false,
        error: false,
        help: '',
        init: function(controller, props){
            props = props || {};
            this._super.apply(this, arguments);
            this.mapPropsToState(props)
            if(!this.input_name){
                this.input_name = _.random(0, 2147483647);
            }
        },
        onInputFocus: function(event){
            event.stopPropagation();
        },
        onInputChange: function(event){
            event.stopPropagation();
            let value = $(event.currentTarget).val()
            this.$el.find('.rm__input-preview').text(value);
        },
        onInputBlur: function(event){
            event.stopPropagation();
            this.readonlyMode();
        },
        onInputValueClick: function(event){
            event.stopPropagation();
            let self = this;
            this.clickCount++;
            if (this.clickCount === 1) {
                setTimeout(function(){
                    if(self.clickCount === 1) {
                        self._onClickInputValue(event);
                    } else {
                        self._onDblclickInputValue(event);
                    }
                    self.clickCount = 0;
                }, 200);
            }
        },
        _onClickInputValue: function(event){

        },
        _onDblclickInputValue: function(event){
            this.editMode();
            this.setFocusInput();
        },
        editMode: function(){
            this.mode = 'edit';
            this.$el.find('.rm__input ').addClass('display');
            this.$el.find('.rm__input-preview').removeClass('display');
        },
        readonlyMode: function(){
            if(this.only_edit_mode){
                return
            }
            this.mode = 'readonly';
            this.$el.find('.rm__input ').removeClass('display');
            this.$el.find('.rm__input-preview').addClass('display');
        },
        setFocusInput: function(){
            let input = this.$el.find('.rm__input');
            let oldVal = input.val();
            input.focus();
            input.val(oldVal);
        }
    });

    return InputBasic;

});