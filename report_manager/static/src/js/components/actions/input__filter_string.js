odoo.define('report_manager.InputFilterString', function(require){

    var InputBasic = require('report_manager.InputBasic');
    var Action = require('report_manager.Action');

    var InputFilterString = InputBasic.extend({
        input_name: 'filter_string',
        placeholder: 'Giá trị',
        class: 'mb-3 w-100',
        only_edit_mode: true,
        mode: 'edit',
        require: false,
        error: false,
        init: function(controller, arg){
            this._super(controller);
            this.arg = arg;
            this.label = arg.name;
            this.input_name += '_'+arg.id;
            this.store.subscribe(this, 'updateViewState', [Action.UPDATE_ARGUMENTS]);
        },
        updateViewState: function(){
            if(!this.arg.require){
                return;
            }
            let self = this;
            let arguments = this.store.getState().configs.arguments;
            arguments.forEach(function(argument){
                if(argument.id === self.arg.id){
                    self.arg.error = argument.error;
                    if(argument.error){
                        self.$el.addClass('is_error');
                    } else {
                        self.$el.removeClass('is_error');
                    }
                }
            });
        },
        renderElement: function(){
            this._super(arguments);
        },
        onInputChange: function(event){
            this._super.apply(this, arguments);
            let self = this;
            let value = $(event.currentTarget).val();
            if(this.arg.search_like || false){
                value = '%'+value+'%';
            }
            this.store.dispatch({
                type: Action.UPDATE_FILTER_ITEM,
                id: self.arg.id,
                value: value
            });
        },
    });

    return InputFilterString;

});