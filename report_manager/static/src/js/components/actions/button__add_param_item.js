odoo.define('report_manager.ButtonAddParamItem', function(require){

    var ButtonBasic = require('report_manager.ButtonBasic');
    var Action = require('report_manager.Action');

    var ButtonAddParamItem = ButtonBasic.extend({
        icon: 'fa fa-plus',
        label: false,
        onActionClick: function(event){
            this.store.dispatch({
                type: Action.ADD_PARAM_ITEM
            });
        }
    });

    return ButtonAddParamItem;
});