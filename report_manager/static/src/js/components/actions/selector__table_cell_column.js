odoo.define('report_manager.SelectorTableCellColumn', function(require){

    var SelectorBasic = require('report_manager.SelectorBasic');
    var Action = require('report_manager.Action');
    var rpc = require('web.rpc');

    var SelectorTableCellColumn = SelectorBasic.extend({
        label: false,
        input_name: 'argument_model',
        init: function(controller, cell){
            this.selected_item = cell.sql_column || {};
            this.cell = cell;
            this._super(controller);
            this.source = this.getSourceModel;
            this.input_name += '_'+cell.id;
        },
        renderElement: function(){
            this._super(arguments);
        },
        onClickSelectorItem: function(event){
            let self = this;
            this._super.apply(this, arguments);
            this.store.dispatch({
                type: Action.UPDATE_TABLE_COLUMN_SQL_COLUMN,
                col_id: self.cell.col_id,
                sql_column: self.selected_item
            });
        },
        getSourceModel: function(){
            let configs = this.store.getState().configs
            let source = configs.sql_columns.map(function(column){
                column['value'] = column.key;
                return column;
            });
            return source;
        }
    });

    return SelectorTableCellColumn;

});