odoo.define('report_manager.CheckboxArgumentItemRequire', function(require){

    var CheckboxBasic = require('report_manager.CheckboxBasic');
    var Action = require('report_manager.Action');

    var CheckboxArgumentItemRequire = CheckboxBasic.extend({
        init: function(controller, argument){
            this._super(controller);
            this.checked = argument.require;
            this.argument = argument;
        },
        onToggleCheckbox: function(event){
            let self = this;
            this._super.apply(this, arguments);
            this.store.dispatch({
                type: Action.UPDATE_ARGUMENT_ITEM_REQUIRE,
                id: self.argument.id,
                require: self.checked
            });
        },
    });

    return CheckboxArgumentItemRequire;

});