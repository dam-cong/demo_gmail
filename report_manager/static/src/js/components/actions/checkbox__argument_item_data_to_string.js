odoo.define('report_manager.CheckboxArgumentItemDataToString', function(require){

    var CheckboxBasic = require('report_manager.CheckboxBasic');
    var Action = require('report_manager.Action');

    var CheckboxArgumentItemDataToString = CheckboxBasic.extend({
        init: function(controller, argument, options){
            this._super(controller, options);
            this.checked = argument.to_string;
            this.argument = argument;
        },
        onToggleCheckbox: function(event){
            let self = this;
            this._super.apply(this, arguments);
            this.store.dispatch({
                type: Action.UPDATE_ARGUMENT_ITEM_DATA_TO_STRING,
                id: self.argument.id,
                to_string: self.checked
            });
        },
    });

    return CheckboxArgumentItemDataToString;

});