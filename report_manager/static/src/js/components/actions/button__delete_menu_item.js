odoo.define('report_manager.ButtonDeleteMenuItem', function(require){

    var ButtonBasic = require('report_manager.ButtonBasic');
    var Action = require('report_manager.Action');

    var ButtonDeleteMenuItem = ButtonBasic.extend({
        icon: 'fa fa-trash',
        label: false,
        onActionClick: function(event){
            this.store.dispatch({
                type: Action.DELETE_MENU_ITEM
            });
        }
    });

    return ButtonDeleteMenuItem;
});