odoo.define('report_manager.SelectorArgumentItemDataType', function(require){

    var SelectorBasic = require('report_manager.SelectorBasic');
    var Action = require('report_manager.Action');

    var SelectorArgumentItemDataType = SelectorBasic.extend({
        label: false,
        input_name: 'argument_data_type',
        init: function(controller, argument, options){
            this.selected_item = argument.data_type || {};
            this.argument = argument;
            this._super(controller, options);
            this.source = this.generateSourceData();
            this.input_name += '_'+argument.id;
        },
        renderElement: function(){
            this._super(arguments);
        },
        onClickSelectorItem: function(event){
            let self = this;
            this._super.apply(this, arguments);
            this.store.dispatch({
                type: Action.UPDATE_ARGUMENT_ITEM_DATA_TYPE,
                id: self.argument.id,
                data_type: self.selected_item
            });
        },
        generateSourceData: function(){
            return [{id: 'string', name: 'String', value: 'string'},
                    {id: 'number', name: 'Number', value: 'number'},
                    {id: 'date', name: 'Date', value: 'date'},
                    {id: 'datetime', name: 'Datetime', value: 'datetime'},
                    {id: 'options', name: 'Options', value: 'options'},
                    {id: 'model', name: 'Model', value: 'model'}]
        }
    });

    return SelectorArgumentItemDataType;

});