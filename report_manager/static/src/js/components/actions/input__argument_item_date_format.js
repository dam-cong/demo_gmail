odoo.define('report_manager.InputArgumentItemDateFormat', function(require){

    var core = require('web.core');
    var InputBasic = require('report_manager.InputBasic');
    var Action = require('report_manager.Action');

    var _lt = core._lt;

    var InputArgumentItemDateFormat = InputBasic.extend({
        label: false,
        input_name: 'argument_date_format',
        placeholder: _lt('Argument date format'),
        help: _lt('Visit https://momentjs.com/docs/#/displaying/format/ to get more information.'),
        init: function(controller, argument, options){
            this._super(controller, options);
            this.input_name = argument.id;
            this.value = argument.format;
            this.argument = argument;
        },
        onInputChange: function(event){
            this._super.apply(this, arguments);
            let self = this;
            let value = $(event.currentTarget).val();
            this.store.dispatch({
                type: Action.UPDATE_ARGUMENT_ITEM_DATE_FORMAT,
                id: self.argument.id,
                format: value
            });
        },
    });

    return InputArgumentItemDateFormat;

});