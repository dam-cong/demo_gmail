odoo.define('report_manager.CheckboxParamItemCheck', function(require){

    var CheckboxBasic = require('report_manager.CheckboxBasic');
    var Action = require('report_manager.Action');

    var CheckboxParamItemCheck = CheckboxBasic.extend({
        init: function(controller, param){
            this._super(controller);
            this.checked = param.check;
            this.param = param;
        },
        onToggleCheckbox: function(event){
            let self = this;
            this._super.apply(this, arguments);
            this.store.dispatch({
                type: Action.UPDATE_PARAM_ITEM_CHECK,
                id: self.param.id,
                check: self.checked
            });
        },
    });

    return CheckboxParamItemCheck;

});