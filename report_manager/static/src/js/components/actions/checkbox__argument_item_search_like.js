odoo.define('report_manager.CheckboxArgumentItemSearchLike', function(require){

    var CheckboxBasic = require('report_manager.CheckboxBasic');
    var Action = require('report_manager.Action');

    var CheckboxArgumentItemSearchLike = CheckboxBasic.extend({
        init: function(controller, argument, options){
            this._super(controller, options);
            this.checked = argument.search_like;
            this.argument = argument;
        },
        onToggleCheckbox: function(event){
            let self = this;
            this._super.apply(this, arguments);
            this.store.dispatch({
                type: Action.UPDATE_ARGUMENT_ITEM_SEARCH_LIKE,
                id: self.argument.id,
                search_like: self.checked
            });
        },
    });

    return CheckboxArgumentItemSearchLike;

});