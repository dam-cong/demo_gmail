odoo.define('report_manager.InputArgumentItemName', function(require){

    var core = require('web.core');
    var InputBasic = require('report_manager.InputBasic');
    var Action = require('report_manager.Action');

    var _lt = core._lt;

    var InputArgumentItemName = InputBasic.extend({
        label: false,
        input_name: 'argument_name',
        placeholder: _lt('Argument name'),
        init: function(controller, argument, options){
            this._super(controller, options);
            this.input_name = argument.id;
            this.value = argument.name;
            this.argument = argument;
        },
        onInputChange: function(event){
            this._super.apply(this, arguments);
            let self = this;
            let value = $(event.currentTarget).val();
            this.store.dispatch({
                type: Action.UPDATE_ARGUMENT_ITEM_NAME,
                id: self.argument.id,
                name: value
            });
        },
        _onClickInputValue: function(event){
            let self = this;
            this.store.dispatch({
                type: Action.UPDATE_SELECTED_ARGUMENT,
                argument: self.argument
            });
        }
    });

    return InputArgumentItemName;

});