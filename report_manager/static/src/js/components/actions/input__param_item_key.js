odoo.define('report_manager.InputParamItemKey', function(require){

    var core = require('web.core');
    var InputBasic = require('report_manager.InputBasic');
    var Action = require('report_manager.Action');

    var _lt = core._lt;

    var InputParamItemKey = InputBasic.extend({
        label: false,
        input_name: 'param_key',
        placeholder: _lt('Param key'),
        init: function(controller, param){
            this._super(controller);
            this.input_name = param.id;
            this.value = param.key;
            this.param = param;
        },
        onInputChange: function(event){
            this._super.apply(this, arguments);
            let self = this;
            let value = $(event.currentTarget).val();
            this.store.dispatch({
                type: Action.UPDATE_PARAM_ITEM_KEY,
                id: self.param.id,
                key: value
            })
        },
    });

    return InputParamItemKey;

});