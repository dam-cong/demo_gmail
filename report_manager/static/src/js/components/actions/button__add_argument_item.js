odoo.define('report_manager.ButtonAddArgumentItem', function(require){

    var ButtonBasic = require('report_manager.ButtonBasic');
    var Action = require('report_manager.Action');

    var ButtonAddArgumentItem = ButtonBasic.extend({
        icon: 'fa fa-plus',
        label: false,
        onActionClick: function(event){
            this.store.dispatch({
                type: Action.ADD_ARGUMENT_ITEM
            });
        }
    });

    return ButtonAddArgumentItem;
});