odoo.define('report_manager.SelectorMenuItemReportTypes', function(require){

    var SelectorBasic = require('report_manager.SelectorBasic');
    var Action = require('report_manager.Action');

    var SelectorMenuItemReportTypes = SelectorBasic.extend({
        label: false,
        input_name: 'menu_report_types',
        multi: true,
        init: function(controller, menu){
            this.selected_items = menu.report_types || [];
            this.menu = menu;
            this._super(controller);
            this.source = this.getSourceData();
            this.input_name += '_'+menu.id;
        },
        renderElement: function(){
            this._super(arguments);
        },
        onClickSelectorItem: function(event){
            let self = this;
            this._super.apply(this, arguments);
            this.store.dispatch({
                type: Action.UPDATE_MENU_ITEM_REPORT_TYPES,
                id: self.menu.id,
                report_types: self.selected_items
            });
        },
        getSourceData: function(){
            return [{id: 'table', name: 'Table', value: 'table'},
                    {id: 'pivot', name: 'Pivot', value: 'pivot'},
                    {id: 'birt', name: 'Birt', value: 'birt'},
                    {id: 'perspective', name: 'Perspective', value: 'perspective'}];
        }
    });

    return SelectorMenuItemReportTypes;

});