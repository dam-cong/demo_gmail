odoo.define('report_manager.InputTableCellHeadName', function(require){

    var core = require('web.core');
    var InputBasic = require('report_manager.InputBasic');
    var Action = require('report_manager.Action');

    var _lt = core._lt;

    var InputTableCellHeadName = InputBasic.extend({
        label: false,
        input_name: 'param_name',
        placeholder: _lt('Cell name'),
        init: function(controller, cell){
            this._super(controller);
            this.input_name = cell.id;
            this.value = cell.name;
            this.cell = cell;
            this.store.subscribe(this, 'updateCellView', [Action.UPDATE_TABLE_COLUMN_SQL_COLUMN])
        },
        updateCellView: function(){
            let self = this;
            let table = this.store.getState().configs.table.table || [];
            table.forEach(function (row) {
                row.forEach(function (cell) {
                    if(_.isEqual(self.cell, cell)){
                        self.cell = cell;
                        self._updateCellView();
                    }
                });
            });
        },
        _updateCellView: function(){
            this.$el.find('.rm__input-preview').text(this.cell.name);
        },
        onInputChange: function(event){
            this._super.apply(this, arguments);
            let self = this;
            let value = $(event.currentTarget).val();
            this.store.dispatch({
                type: Action.UPDATE_TABLE_CELL_HEAD_NAME,
                id: self.cell.id,
                name: value
            })
        },
    });

    return InputTableCellHeadName;

});