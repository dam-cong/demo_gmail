odoo.define('report_manager.ButtonDeleteArgumentItem', function(require){

    var ButtonBasic = require('report_manager.ButtonBasic');
    var Action = require('report_manager.Action');

    var ButtonDeleteArgumentItem = ButtonBasic.extend({
        icon: 'fa fa-trash',
        label: false,
        onActionClick: function(event){
            this.store.dispatch({
                type: Action.DELETE_ARGUMENT_ITEM
            });
        }
    });

    return ButtonDeleteArgumentItem;
});