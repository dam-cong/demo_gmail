odoo.define('report_manager.ButtonAddOptionItem', function(require){

    var ButtonBasic = require('report_manager.ButtonBasic');
    var Action = require('report_manager.Action');

    var ButtonAddOptionItem = ButtonBasic.extend({
        icon: 'fa fa-plus',
        label: false,
        init: function(controller, argument){
            this._super.apply(this, arguments);
            this.argument = argument;
        },
        onActionClick: function(event){
            let self = this;
            this.store.dispatch({
                type: Action.ADD_OPTION_ITEM,
                id: self.argument.id
            });
        }
    });

    return ButtonAddOptionItem;
});