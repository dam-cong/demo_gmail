odoo.define('report_manager.InputMenuItemName', function(require){

    var InputBasic = require('report_manager.InputBasic');
    var Action = require('report_manager.Action');

    var InputMenuItemName = InputBasic.extend({
        label: false,
        input_name: 'menu_name',
        placeholder: 'Tên menu',
        init: function(controller, menu){
            this._super(controller);
            this.input_name = menu.id;
            this.value = menu.name;
            this.menu = menu;
        },
        onInputChange: function(event){
            this._super.apply(this, arguments);
            let self = this;
            let value = $(event.currentTarget).val();
            this.store.dispatch({
                type: Action.UPDATE_MENU_ITEM_NAME,
                id: self.menu.id,
                name: value
            })
        },
    });

    return InputMenuItemName;

});