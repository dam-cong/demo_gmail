odoo.define('report_manager.CheckboxMenuItemCheck', function(require){

    var CheckboxBasic = require('report_manager.CheckboxBasic');
    var Action = require('report_manager.Action');

    var CheckboxMenuItemCheck = CheckboxBasic.extend({
        init: function(controller, menu){
            this._super(controller);
            this.checked = menu.check;
            this.menu = menu;
        },
        onToggleCheckbox: function(event){
            let self = this;
            this._super.apply(this, arguments);
            this.store.dispatch({
                type: Action.UPDATE_MENU_ITEM_CHECK,
                id: self.menu.id,
                check: self.checked
            });
        },
    });

    return CheckboxMenuItemCheck;

});