odoo.define('report_manager.ButtonSwitchTab', function(require){

    var core = require('web.core');
    var ButtonBasic = require('report_manager.ButtonBasic');
    var Action = require('report_manager.Action');

    var _lt = core._lt;

    var ButtonSwitchTab = ButtonBasic.extend({
        icon: 'fa fa-circle',
        label: _lt('Switch tab'),
        display_tab: '',
        class: 'btn-config--tab',
        init: function(parent, icon, label, display_tab){
            this.icon = icon || this.icon;
            this.label = label || this.label;
            this.display_tab = display_tab;
            this._super(parent);
            let types = [Action.UPDATE_DISPLAY_TAB, Action.UPDATE_STORE];
            this.store.subscribe(this, 'toggleButtonActive', types);
        },
        renderElement(){
            this._super.apply(this, arguments);
        },
        onActionClick: function(event){
            let self = this;
            this.store.dispatch({
                type: Action.UPDATE_DISPLAY_TAB,
                display_tab: self.display_tab
            });
        },
        toggleButtonActive: function(){
            let data = this.store.getState().configs;
            this.active = data && data.hasOwnProperty('display_tab') && data.display_tab === this.display_tab;
            if(!this.$el){
                return;
            }
            if(this.active){
                this.$el.addClass('active');
            } else {
                this.$el.removeClass('active');
            }
        }
    });

    return ButtonSwitchTab;
});