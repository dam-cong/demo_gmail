odoo.define('report_manager.ButtonSaveSql', function(require){

    var core = require('web.core');
    var ButtonBasic = require('report_manager.ButtonBasic');
    var Action = require('report_manager.Action');
    var ReportNotification = require('report_manager.ReportNotification');

    var _lt = core._lt;

    var ButtonSaveSql = ButtonBasic.extend({
        icon: 'fa fa-check',
        label: _lt('Apply'),
        init: function(controller, fragment){
            this._super(controller);
            this.fragment = fragment;
            this.sql = '';
        },
        onActionClick: function(event){
            this.saveSql();
            this.createParamData();
            this.createColumnsData();
        },
        saveSql: function(){
            let self = this;
            this.sql = self.fragment.boxSqlEditor.editor.editor.getValue();
            let sql = btoa(encodeURIComponent(this.sql));
            this.store.dispatch({
                type: Action.UPDATE_SQL,
                sql: sql
            });
            this.call('notification', 'notify', {type: 'notify',
                                                 Notification: ReportNotification,
                                                 title: _lt('Notify'),
                                                 message: _lt('Apply this query to get data')})
        },
        createParamData: function(){
            let totalParams = (this.sql.match(new RegExp(/\?/, 'g')) || []).length;
            let params = this.store.getState().configs.params;
            if(totalParams.length <= params.length){
                return;
            }
            for(let i = params.length; i < totalParams; i++){
                params.push({id: 'param_'+_.random(0, 2147483647),
                             name: 'param_'+_.random(0, 2147483647),
                             check: false,
                             argument: null,
                             default_value: null});
            }
            this.store.dispatch({
                type: Action.UPDATE_PARAMS,
                params: params
            })
        },
        createColumnsData: function(){
            let totalColumns = this.generateSqlColumns(this.sql);
            let columns = [];
            let sql_columns = this.store.getState().configs.sql_columns || [];
            for(let i=0; i < totalColumns.length; i++){
                let sql_cols = sql_columns.filter(function(col){
                    return col.key === totalColumns[i];
                });
                if(sql_cols.length === 1){
                    columns.push(sql_cols[0]);
                    continue;
                }
                columns.push({id: 'col_'+_.random(0, 2147483647),
                              name: totalColumns[i],
                              key: totalColumns[i]});
            }
            this.store.dispatch({
                type: Action.UPDATE_SQL_COLUMNS,
                sql_columns: columns
            });
        },
        generateSqlColumns: function(sql){
            sql = sql || '';
            let columns = [];
            try{
                //Xóa hết đoạn nào bắt đầu bằng '(' và kết thúc bằng ')'
                sql = sql.replace(/\(.*\)/g, '');
                // Lấy dữ liệu từ đoạn select đến from
                let rawColumnsStr = (/\bselect\b\s+([\S\s]+?)from\s/i.exec(sql) || [,""])[1];
                let rawColumns = rawColumnsStr.replace(/\s[A-z]*\([\S\s]+?\)/g, '').split(/\s*,\s*/g);
                columns = rawColumns.map(function(column){
                    let columnArr = column.trim().split(" ");
                    let rawColumn = columnArr[columnArr.length - 1];
                    let rawColumnArr = rawColumn.split('.');
                    return rawColumnArr[rawColumnArr.length - 1];
                });
                return columns;
            } catch (e){
                return columns;
            }
        }
    });

    return ButtonSaveSql;
});