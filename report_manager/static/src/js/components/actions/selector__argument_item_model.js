odoo.define('report_manager.SelectorArgumentItemModel', function(require){

    var SelectorBasic = require('report_manager.SelectorBasic');
    var Action = require('report_manager.Action');
    var rpc = require('web.rpc');

    var SelectorArgumentItemModel = SelectorBasic.extend({
        label: false,
        input_name: 'argument_model',
        init: function(controller, argument, options){
            this.selected_item = argument.model || {};
            this.argument = argument;
            this._super(controller, options);
            this.source = this.getSourceModel;
            this.input_name += '_'+argument.id;
        },
        renderElement: function(){
            this._super(arguments);
        },
        onClickSelectorItem: function(event){
            let self = this;
            this._super.apply(this, arguments);
            this.store.dispatch({
                type: Action.UPDATE_ARGUMENT_ITEM_MODEL,
                id: self.argument.id,
                model: self.selected_item
            });
        },
        getSourceModel: function(name){
            let self = this;
            let def = $.Deferred();
            let context = this.controller.context;
            rpc.query({
                model: 'ir.model',
                method: 'name_search',
                kwargs: {
                    context: context,
                    name: name,
                    operator: 'ilike',
                    limit: self.limit
                },
            }, {
                shadow: true,
            }).then(function(models){
                if(Array.isArray(models)){
                    models = models.map(function(item){
                        return {
                            id: item[0],
                            name: item[1],
                            value: item[0],
                            check: false
                        };
                    });
                    return def.resolve(models);
                }
                return def.reject([]);
            });
            return def.promise();
        }
    });

    return SelectorArgumentItemModel;

});