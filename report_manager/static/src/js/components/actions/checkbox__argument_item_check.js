odoo.define('report_manager.CheckboxArgumentItemCheck', function(require){

    var CheckboxBasic = require('report_manager.CheckboxBasic');
    var Action = require('report_manager.Action');

    var CheckboxArgumentItemCheck = CheckboxBasic.extend({
        init: function(controller, argument){
            this._super(controller);
            this.checked = argument.check;
            this.argument = argument;
        },
        onToggleCheckbox: function(event){
            let self = this;
            this._super.apply(this, arguments);
            this.store.dispatch({
                type: Action.UPDATE_ARGUMENT_ITEM_CHECK,
                id: self.argument.id,
                check: self.checked
            });
        },
    });

    return CheckboxArgumentItemCheck;

});