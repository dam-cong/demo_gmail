odoo.define('report_manager.InputSqlColumnItemName', function(require){

    var core = require('web.core');
    var InputBasic = require('report_manager.InputBasic');
    var Action = require('report_manager.Action');

    var _lt = core._lt;

    var InputSqlColumnItemName = InputBasic.extend({
        label: false,
        input_name: 'sql_column_name',
        placeholder: _lt('Column name'),
        init: function(controller, column){
            this._super(controller);
            this.input_name = column.key;
            this.column = column;
            this.value = column.name;
        },
        onInputChange: function(event){
            this._super.apply(this, arguments);
            let self = this;
            let value = $(event.currentTarget).val();
            this.store.dispatch({
                type: Action.UPDATE_SQL_COLUMN_ITEM_NAME,
                key: self.column.key,
                name: value
            });
        }
    });

    return InputSqlColumnItemName;

});