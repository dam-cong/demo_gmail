odoo.define('report_manager.ButtonAddMenuItem', function(require){

    var ButtonBasic = require('report_manager.ButtonBasic');
    var Action = require('report_manager.Action');

    var ButtonAddMenuItem = ButtonBasic.extend({
        icon: 'fa fa-plus',
        label: false,
        onActionClick: function(event){
            this.store.dispatch({
                type: Action.ADD_MENU_ITEM
            });
        }
    });

    return ButtonAddMenuItem;
});