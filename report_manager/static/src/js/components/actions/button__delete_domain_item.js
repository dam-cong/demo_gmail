odoo.define('report_manager.ButtonDeleteDomainItem', function(require){

    var ButtonBasic = require('report_manager.ButtonBasic');
    var Action = require('report_manager.Action');

    var ButtonDeleteDomainItem = ButtonBasic.extend({
        icon: 'fa fa-trash',
        label: false,
        init: function(controller, argument, domain){
            this._super.apply(this, arguments);
            this.argument = argument;
            this.domain = domain;
        },
        onActionClick: function(event){
            let self = this;
            this.store.dispatch({
                type: Action.DELETE_DOMAIN_ITEM,
                id: self.argument.id,
                domain_id: self.domain.id
            });
        }
    });

    return ButtonDeleteDomainItem;
});