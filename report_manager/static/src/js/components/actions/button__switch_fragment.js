odoo.define('report_manager.ButtonSwitchFragment', function(require){

    var core = require('web.core');
    var ButtonBasic = require('report_manager.ButtonBasic');
    var Action = require('report_manager.Action');

    var _lt = core._lt;

    var ButtonSwitchFragment = ButtonBasic.extend({
        icon: 'fa fa-bars',
        label: _lt('Switch fragment'),
        display_fragment: '',
        class: 'sidebar-item',
        init: function(parent, icon, label, display_fragment){
            this.icon = icon || this.icon;
            this.label = label || this.label;
            this.display_fragment = display_fragment;
            this._super(parent);
            let types = [Action.UPDATE_DISPLAY_FRAGMENT, Action.UPDATE_STORE];
            this.store.subscribe(this, 'toggleButtonActive', types);
        },
        renderElement(){
            this._super.apply(this, arguments);
        },
        onActionClick: function(event){
            let self = this;
            this.store.dispatch({
                type: Action.UPDATE_DISPLAY_FRAGMENT,
                display_fragment: self.display_fragment
            });
        },
        toggleButtonActive: function(){
            let data = this.store.getState().configs;
            this.active = data && data.hasOwnProperty('display_fragment') && data.display_fragment === this.display_fragment;
            if(!this.$el){
                return;
            }
            if(this.active){
                this.$el.addClass('active');
            } else {
                this.$el.removeClass('active');
            }
        }
    });

    return ButtonSwitchFragment;
});