odoo.define('report_manager.InputParamItemName', function(require){

    var core = require('web.core');
    var InputBasic = require('report_manager.InputBasic');
    var Action = require('report_manager.Action');

    var _lt = core._lt;

    var InputParamItemName = InputBasic.extend({
        label: false,
        input_name: 'param_name',
        placeholder: _lt('Param name'),
        init: function(controller, param){
            this._super(controller);
            this.input_name = param.id;
            this.value = param.name;
            this.param = param;
        },
        onInputChange: function(event){
            this._super.apply(this, arguments);
            let self = this;
            let value = $(event.currentTarget).val();
            this.store.dispatch({
                type: Action.UPDATE_PARAM_ITEM_NAME,
                id: self.param.id,
                name: value
            })
        },
    });

    return InputParamItemName;

});