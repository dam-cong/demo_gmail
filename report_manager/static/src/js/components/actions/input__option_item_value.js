odoo.define('report_manager.InputOptionItemValue', function(require){

    var core = require('web.core');
    var InputBasic = require('report_manager.InputBasic');
    var Action = require('report_manager.Action');

    var _lt = core._lt;

    var InputOptionItemValue = InputBasic.extend({
        label: false,
        input_name: 'option_value',
        placeholder: _lt('Option value'),
        init: function(controller, argument, option){
            this._super(controller);
            this.input_name = option.id;
            this.argument = argument;
            this.option = option;
            this.value = option.value;
        },
        onInputChange: function(event){
            this._super.apply(this, arguments);
            let self = this;
            let value = $(event.currentTarget).val();
            this.store.dispatch({
                type: Action.UPDATE_OPTION_ITEM_VALUE,
                id: self.argument.id,
                option_id: self.option.id,
                value: value
            });
        }
    });

    return InputOptionItemValue;

});