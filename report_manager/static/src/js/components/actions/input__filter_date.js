odoo.define('report_manager.InputFilterDate', function(require){

    var core = require('web.core');
    var InputBasic = require('report_manager.InputBasic');
    var Action = require('report_manager.Action');
    var time = require('web.time');

    var _lt = core._lt;

    var InputFilterDate = InputBasic.extend({
        template: 'RMDatePicker',
        events: _.extend({}, InputBasic.prototype.events, {
            'change.datetimepicker ': 'onInputChange',
        }),
        input_name: 'filter_date',
        placeholder: _lt('Select date'),
        class: 'mb-3 w-100',
        type: 'text',
        only_edit_mode: true,
        mode: 'edit',
        type_of_date: 'date',
        require: false,
        error: false,
        init: function(controller, arg){
            this._super(controller);
            this.arg = arg;
            this.label = arg.name;
            this.require = arg.require;
            this.input_name += '_'+arg.id;
            this.store.subscribe(this, 'updateViewState', [Action.UPDATE_ARGUMENTS]);
        },
        updateViewState: function(){
            if(!this.arg.require){
                return;
            }
            let self = this;
            let arguments = this.store.getState().configs.arguments;
            arguments.forEach(function(argument){
                if(argument.id === self.arg.id){
                    self.arg.error = argument.error;
                    if(argument.error){
                        self.$el.addClass('is_error');
                    } else {
                        self.$el.removeClass('is_error');
                    }
                }
            });
        },
        renderElement: function(){
            this._super.apply(this, arguments);
            this.makeDatePicker();
        },
        makeDatePicker: function(){
            let options = {
                locale: moment.locale(),
                format : this.type_of_date === 'datetime' ? time.getLangDatetimeFormat() : time.getLangDateFormat(),
                minDate: moment({ y: 1900 }),
                maxDate: moment({ y: 9999, M: 11, d: 31 }),
                useCurrent: false,
                icons: {
                    time: 'fa fa-clock-o',
                    date: 'fa fa-calendar',
                    up: 'fa fa-chevron-up',
                    down: 'fa fa-chevron-down',
                    previous: 'fa fa-chevron-left',
                    next: 'fa fa-chevron-right',
                    today: 'fa fa-calendar-check-o',
                    clear: 'fa fa-delete',
                    close: 'fa fa-times'
                },
                calendarWeeks: true,
                buttons: {
                    showToday: false,
                    showClear: false,
                    showClose: false,
                },
                keyBinds: null,
                allowInputToggle: true,
            }
            this.$el.find('.o_datepicker').datetimepicker(options);
        },
        onInputChange: function(event){
            event.stopPropagation();
            let self = this;
            this._super.apply(this, arguments);
            let format = this.arg.format || 'YYYY-MM-DD';
            let value = '';
            if(this.type_of_date === 'datetime'){
                format = this.arg.format || 'YYYY-MM-DD HH:mm:ss';
            }
            if(event.hasOwnProperty('date')){
                value = event.date.format(format);
            } else {
                let val = $(event.currentTarget).val();
                if(this.type_of_date === 'datetime'){
                    let dt = moment(val, 'MM/DD/YYYY HH:mm:ss');
                    value = dt.isValid() ? dt.format(format) : '';
                } else {
                    let d = moment(val, 'MM/DD/YYYY');
                    value = d.isValid() ? d.format(format) : '';
                }
            }
            this.store.dispatch({
                type: Action.UPDATE_FILTER_ITEM,
                id: self.arg.id,
                value: value
            });
        },
    });

    return InputFilterDate;

});