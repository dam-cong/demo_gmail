odoo.define('report_manager.InputDomainItemValue', function(require){

    var core = require('web.core');
    var InputBasic = require('report_manager.InputBasic');
    var Action = require('report_manager.Action');

    var _lt = core._lt;

    var InputDomainItemValue = InputBasic.extend({
        label: false,
        input_name: 'domain_value',
        placeholder: _lt('Domain value'),
        init: function(controller, argument, domain){
            this._super(controller);
            this.input_name = domain.id;
            this.argument = argument;
            this.domain = domain;
            this.value = domain.value;
        },
        onInputChange: function(event){
            this._super.apply(this, arguments);
            let self = this;
            let value = $(event.currentTarget).val();
            this.store.dispatch({
                type: Action.UPDATE_DOMAIN_ITEM_VALUE,
                id: self.argument.id,
                domain_id: self.domain.id,
                value: value
            });
        }
    });

    return InputDomainItemValue;

});