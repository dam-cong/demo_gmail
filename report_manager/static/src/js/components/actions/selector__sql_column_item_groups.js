odoo.define('report_manager.SelectorSqlColumnItemGroups', function(require){

    var SelectorBasic = require('report_manager.SelectorBasic');
    var Action = require('report_manager.Action');
    var rpc = require('web.rpc');

    var SelectorSqlColumnItemGroups = SelectorBasic.extend({
        label: false,
        input_name: 'sql_column_groups',
        multi: true,
        init: function(controller, sql_column){
            this.selected_items = sql_column.groups || [];
            this.sql_column = sql_column;
            this._super(controller);
            this.source = this.getSourceGroups;
            this.input_name += '_'+sql_column.key;
        },
        renderElement: function(){
            this._super(arguments);
        },
        onClickSelectorItem: function(event){
            let self = this;
            this._super.apply(this, arguments);
            this.store.dispatch({
                type: Action.UPDATE_SQL_COLUMN_ITEM_GROUPS,
                key: self.sql_column.key,
                groups: self.selected_items
            });
        },
        getSourceGroups: function(name){
            let self = this;
            let def = $.Deferred();
            let context = this.controller.context;
            rpc.query({
                model: 'res.groups',
                method: 'name_search',
                kwargs: {
                    context: context,
                    name: name,
                    operator: 'ilike',
                    limit: self.limit
                },
            }, {
                shadow: true,
            }).then(function(response){
                if(Array.isArray(response)){
                    response = response.map(function(item){
                        return {
                            id: item[0],
                            name: item[1],
                            value: item[0],
                            check: false
                        };
                    });
                    return def.resolve(response);
                }
                return def.reject([]);
            });
            return def.promise();
        }
    });

    return SelectorSqlColumnItemGroups;

});