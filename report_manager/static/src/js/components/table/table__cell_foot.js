odoo.define('report_manager.TableCellFoot', function(require){

    var TableCell = require('report_manager.TableCell');
    var Action = require('report_manager.Action');

    var TableCellFoot = TableCell.extend({
        tag: 'th',
        init: function(controller, cell){
            this._super.apply(this, arguments);
            this.store.subscribe(this, 'updateCellView', [Action.UPDATE_TABLE_COLUMN_SQL_COLUMN])
        },
        updateCellView: function(){
            let self = this;
            let table = this.store.getState().configs.table.table || [];
            table.forEach(function (row) {
                row.forEach(function (cell) {
                    if(_.isEqual(self.cell, cell)){
                        self.cell = cell;
                    }
                });
            });
        },
        _updateCellView: function(){
            this.makeCellStyle();
        },
    });

    return TableCellFoot;

});