odoo.define('report_manager.TableRow', function(require){

    var WidgetBasic = require('report_manager.WidgetBasic');
    var TableCellHead = require('report_manager.TableCellHead');
    var TableCellBody = require('report_manager.TableCellBody');
    var TableCellFoot = require('report_manager.TableCellFoot');
    var Action = require('report_manager.Action');

    var TableRow = WidgetBasic.extend({
        template: 'RMTableRow',
        cells: [],
        init: function(controller, row, type){
            this.controller = controller;
            this.store = controller.store;
            this.type = type;
            this.row = row;
            this._super.apply(this, arguments);
        },
        renderElement: function(){
            this._super.apply(this, arguments);
            this.renderCell();
        },
        renderCell: function () {
            if(this.type === 'head'){
                this._renderCellHead();
            } else if(this.type === 'body') {
                this._renderCellBody()
            } else if(this.type === 'foot') {
                this._renderCellFoot()
            }
        },
        _renderCellHead: function(){
            let self = this;
            let el = this.$el;
            this.row.forEach(function(cell){
                let tableCellHead = new TableCellHead(self.controller, cell);
                tableCellHead.appendTo(el);
            });
        },
        _renderCellBody: function(){
            let self = this;
            let el = this.$el;
            this.row.forEach(function(cell){
                let tableCellBody = new TableCellBody(self.controller, cell);
                tableCellBody.appendTo(el);
            });
        },
        _renderCellFoot: function(){
            let self = this;
            let el = this.$el;
            this.row.forEach(function(cell){
                let tableCellFoot = new TableCellFoot(self.controller, cell);
                tableCellFoot.appendTo(el);
            });
        },
    });

    return TableRow;

});