odoo.define('report_manager.TableCell', function(require){

    var WidgetBasic = require('report_manager.WidgetBasic');
    var Action = require('report_manager.Action');

    var TableCell = WidgetBasic.extend({
        template: 'RMTableCell',
        events: function(){

        },
        tag: 'td',
        width: '3cm',
        backgroundColor: '#ffffff',
        color: '#333333',
        fontSize: '14px',
        fontWeight: 'normal',
        init: function(controller, cell){
            this.controller = controller;
            this.store = controller.store;
            this.mapCellToProp(cell);
            this._super.apply(this, arguments);
        },
        renderElement: function(){
            this._super.apply(this, arguments);
            this.makeCellStyle();
        },
        makeCellStyle: function(){
            if(_.isEqual(this.cell.row_id, 'fix_row') && _.isEqual(this.cell.col_id, 'fix_col')){
                this.$el.addClass('cell--hidden');
                return;
            } else if(_.isEqual(this.cell.row_id, 'fix_row') && !_.isEqual(this.cell.col_id, 'fix_col')){
                this.$el.addClass('cell--col');
                return;
            } else if(!_.isEqual(this.cell.row_id, 'fix_row') && _.isEqual(this.cell.col_id, 'fix_col')){
                this.$el.addClass('cell--row');
                return;
            }
            let css = {width: this.width,
                       'background-color': this.backgroundColor,
                       'color': this.backgroundColor,
                       'font-size': this.fontSize,
                       'font-weight': this.fontWeight};
            if(this.tag === 'th'){
                this.$el.css(css);
            }
        },
        mapCellToProp: function(cell){
            this.cell = cell;
            let self = this;
            let cellKeys = Object.keys(cell);
            cellKeys.forEach(function(key){
                self[key] = cell[key];
            });
        },
        mapPropToCell: function(){
            let self = this;
            let cellKeys = Object.keys(this.cell);
            cellKeys.forEach(function(key){
                self.cell[key] = self[key];
            });
        }
    });

    return TableCell;

});