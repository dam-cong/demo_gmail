odoo.define('report_manager.TableCellBody', function(require){

    var TableCell = require('report_manager.TableCell');
    var SelectorTableCellColumn = require('report_manager.SelectorTableCellColumn');
    var Action = require('report_manager.Action');

    var TableCellBody = TableCell.extend({
        init: function(controller, cell){
            this._super.apply(this, arguments);
            this.store.subscribe(this, 'updateCellView', [Action.UPDATE_TABLE_COLUMN_SQL_COLUMN])
        },
        updateCellView: function(){
            let self = this;
            let table = this.store.getState().configs.table.table || [];
            table.forEach(function (row) {
                row.forEach(function (cell) {
                    if(_.isEqual(self.cell, cell)){
                        self.cell = cell;
                    }
                });
            });
        },
        _updateCellView: function(){
            this.makeCellStyle();
        },
        renderElement: function(){
            this._super.apply(this, arguments);
            if(!_.isEqual(this.cell.row_id, 'fix_row') && !_.isEqual(this.cell.col_id, 'fix_col')){
                let selectorTableCellColumn = new SelectorTableCellColumn(this.controller, this.cell);
                selectorTableCellColumn.appendTo(this.$el);
            }
        }
    });

    return TableCellBody;

});