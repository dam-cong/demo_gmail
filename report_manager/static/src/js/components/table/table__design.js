odoo.define('report_manager.TableDesign', function(require){

    var WidgetBasic = require('report_manager.WidgetBasic');
    var TableRow = require('report_manager.TableRow');
    var Action = require('report_manager.Action');

    var TableDesign = WidgetBasic.extend({
        template: 'RMTableDesign',
        cell_ids: [],
        column_ids: [...Action.COLUMN_IDS],
        head_row_ids: [...Action.HEAD_ROW_IDS],
        body_row_ids: [...Action.BODY_ROW_IDS],
        foot_row_ids: [...Action.FOOT_ROW_IDS],
        table: [],
        init: function(controller){
            this.controller = controller;
            this.store = controller.store;
            this._super.apply(this, arguments);
            this.mapTableDataToState();
            if(_.isEmpty(this.table)){
                this.initTableData();
            }
            let types = [Action.UPDATE_TABLE_DATA];
            this.store.subscribe(this, 'reRenderTable', types);
        },
        renderElement: function(){
            this._super.apply(this, arguments);
            this.renderTable();
            this.initContextMenu();
        },
        reRenderTable: function(){
            this.mapTableDataToState();
            this.renderTable();
        },
        renderTable: function () {
            let self = this;
            this.clearTableHtml();
            _.forEach(this.table, function(row){
                if(self.head_row_ids.indexOf(row[0].row_id) >= 0){
                    self.renderTableHead(row);
                } else if(self.body_row_ids.indexOf(row[0].row_id) >= 0){
                    self.renderTableBody(row);
                } else {
                    self.renderTableFoot(row);
                }
            });
        },
        clearTableHtml: function(){
            this.$el.find('thead').html('');
            this.$el.find('tbody').html('');
            this.$el.find('tfoot').html('');
        },
        renderTableHead: function(row){
            let headEl = this.$el.find('thead');
            let headRow = new TableRow(this.controller, row, 'head');
            headRow.appendTo(headEl);
        },
        renderTableBody: function(row){
            let bodyEl = this.$el.find('tbody');
            let bodyRow = new TableRow(this.controller, row, 'body');
            bodyRow.appendTo(bodyEl);
        },
        renderTableFoot: function(row){
            let footEl = this.$el.find('tfoot');
            let footRow = new TableRow(this.controller, row, 'foot');
            footRow.appendTo(footEl);
        },
        initContextMenu: function(){
            let self = this;
            $.contextMenu({
                selector: '.cell--col',
                items: {
                    'add_col_before': {name: 'Add column before', icon: 'fa-plus'},
                    'del_col': {name: 'Delete column', icon: 'fa-trash'},
                    'add_col_after': {name: 'Add column after', icon: 'fa-plus'},
                },
                callback: function(key, options) {
                    let col_id = options.$trigger.data('col-id');
                    switch (key) {
                        case 'add_col_before':
                            self.addColBefore(col_id);
                            break;
                        case 'del_col':
                            self.deleteCol(col_id);
                            break;
                        case 'add_col_after':
                            self.addColAfter(col_id);
                            break;
                    }
                    self.store.dispatch({
                        type: Action.UPDATE_TABLE_DATA,
                        table: self.mapStateToTableData()
                    });
                }
            });
            $.contextMenu({
                selector: '.cell--row',
                items: {
                    'add_row_before': {name: 'Add row before', icon: 'fa-plus'},
                    'del_row': {name: 'Delete row', icon: 'fa-trash'},
                    'add_row_after': {name: 'Add row after', icon: 'fa-plus'},
                },
                callback: function(key, options) {
                    let row_id = options.$trigger.data('row-id');
                    switch (key) {
                        case 'add_row_before':
                            self.addRowBefore(row_id);
                            break;
                        case 'del_row':
                            self.deleteRow(row_id);
                            break;
                        case 'add_row_after':
                            self.addRowAfter(row_id);
                            break;
                    }
                    self.store.dispatch({
                        type: Action.UPDATE_TABLE_DATA,
                        table: self.mapStateToTableData()
                    });
                }
            });
        },
        addColBefore: function(col_id){
            let self = this;
            let new_col_id = this.getColId();
            for(let i = 0; i < this.column_ids.length; i++){
                if(this.column_ids[i] === col_id){
                    this.column_ids.splice(i, 0, new_col_id);
                    break;
                }
            }
            _.map(this.table, function(row){
                for(let i = 0; i < row.length; i++){
                    if(row[i].col_id === col_id){
                        let cell_id = self.getCellId();
                        row.splice(i, 0, {id: cell_id,
                                          row_id: row[i].row_id,
                                          col_id: new_col_id,
                                          name: 'Cell'});
                        break;
                    }
                }
                return row;
            });
        },
        deleteCol: function(col_id){
            let self = this;
            for(let i = 0; i < this.column_ids.length; i++){
                if(this.column_ids[i] === col_id){
                    this.column_ids.splice(i, 1);
                    break;
                }
            }
            _.map(this.table, function(row){
                for(let i = 0; i < row.length; i++){
                    if(row[i].col_id === col_id){
                        row.splice(i, 1);
                        break;
                    }
                }
                return row;
            });
            if(this.column_ids.length <= 1){
                this.column_ids = [...Action.COLUMN_IDS];
                let new_col_id = this.column_ids[1];
                _.map(this.table, function(row){
                    for(let i = 0; i < row.length; i++){
                        if(row[i].col_id === 'fix_col'){
                            let cell_id = self.getCellId();
                            row.splice(i+1, 0, {id: cell_id,
                                                row_id: row[i].row_id,
                                                col_id: new_col_id,
                                                name: 'Cell'});
                            break;
                        }
                    }
                    return row;
                });
            }
        },
        addColAfter: function(col_id){
            let self = this;
            let new_col_id = this.getColId();
            for(let i = 0; i < this.column_ids.length; i++){
                if(this.column_ids[i] === col_id){
                    this.column_ids.splice(i+1, 0, new_col_id);
                    break;
                }
            }
            _.map(this.table, function(row){
                for(let i = 0; i < row.length; i++){
                    if(row[i].col_id === col_id){
                        let cell_id = self.getCellId();
                        row.splice(i+1, 0, {id: cell_id,
                                            row_id: row[i].row_id,
                                            col_id: new_col_id,
                                            name: 'Cell'});
                        break;
                    }
                }
                return row;
            });
        },
        addRowBefore: function(row_id){
            if(this.head_row_ids.indexOf(row_id) >= 0){
                this._addHeadRowBefore(row_id);
            } else if(this.body_row_ids.indexOf(row_id) >= 0){
                this._addBodyRowBefore(row_id);
            } else {
                this._addFootRowBefore(row_id);
            }
        },
        _addHeadRowBefore: function(row_id){
            let self = this;
            for(let i = 0; i < this.head_row_ids.length; i++){
                if(this.head_row_ids[i] === row_id){
                    let new_row_id = self.getRowId('head');
                    this.head_row_ids.splice(i, 0, new_row_id);
                    this.table.splice(i, 0, this.generateRow(new_row_id));
                    break;
                }
            }
        },
        _addBodyRowBefore: function(row_id){
            let self = this;
            for(let i = 0; i < this.body_row_ids.length; i++){
                if(this.body_row_ids[i] === row_id){
                    let new_row_id = self.getRowId('body');
                    this.body_row_ids.splice(i, 0, new_row_id);
                    let row_pos = this.head_row_ids.length + i;
                    this.table.splice(row_pos, 0, this.generateRow(new_row_id));
                    break;
                }
            }
        },
        _addFootRowBefore: function(row_id){
            let self = this;
            for(let i = 0; i < this.foot_row_ids.length; i++){
                if(this.foot_row_ids[i] === row_id){
                    let new_row_id = self.getRowId('foot');
                    this.foot_row_ids.splice(i, 0, new_row_id);
                    let row_pos = this.head_row_ids.length + this.body_row_ids.length + i;
                    this.table.splice(row_pos, 0, this.generateRow(new_row_id));
                    break;
                }
            }
        },
        deleteRow: function(row_id){
            if(this.head_row_ids.indexOf(row_id) >= 0){
                this._deleteHeadRow(row_id);
            } else if(this.body_row_ids.indexOf(row_id) >= 0){
                this._deleteBodyRow(row_id);
            } else {
                this._deleteFootRow(row_id);
            }
        },
        _deleteHeadRow: function(row_id){
            for(let i = 0; i < this.head_row_ids.length; i++){
                if(this.head_row_ids[i] === row_id){
                    this.head_row_ids.splice(i, 1);
                    this.table.splice(i, 1);
                    break;
                }
            }
            if(this.head_row_ids.length < 1){
                this.head_row_ids = [...Action.HEAD_ROW_IDS];
                this.generateHeadRow();
            }
        },
        _deleteBodyRow: function(row_id){
            for(let i = 0; i < this.body_row_ids.length; i++){
                if(this.body_row_ids[i] === row_id){
                    this.body_row_ids.splice(i, 1);
                    let row_pos = this.head_row_ids.length + i;
                    this.table.splice(row_pos, 1);
                    break;
                }
            }
            if(this.body_row_ids.length < 1){
                this.body_row_ids = [...Action.BODY_ROW_IDS];
                this.generateBodyRow();
            }
        },
        _deleteFootRow: function(row_id){
            for(let i = 0; i < this.foot_row_ids.length; i++){
                if(this.foot_row_ids[i] === row_id){
                    this.foot_row_ids.splice(i, 1);
                    let row_pos = this.head_row_ids.length + this.body_row_ids.length + i;
                    this.table.splice(row_pos, 1);
                    break;
                }
            }
            if(this.foot_row_ids.length < 1){
                this.foot_row_ids = [...Action.FOOT_ROW_IDS];
                this.generateFootRow();
            }
        },
        addRowAfter: function(row_id){
            if(this.head_row_ids.indexOf(row_id) >= 0){
                this._addHeadRowAfter(row_id);
            } else if(this.body_row_ids.indexOf(row_id) >= 0){
                this._addBodyRowAfter(row_id);
            } else {
                this._addFootRowAfter(row_id);
            }
        },
        _addHeadRowAfter: function(row_id){
            for(let i = 0; i < this.head_row_ids.length; i++){
                if(this.head_row_ids[i] === row_id){
                    let new_row_id = self.getRowId('head');
                    this.head_row_ids.splice(i+1, 0, new_row_id);
                    this.table.splice(i+1, 0, this.generateRow(new_row_id));
                    break;
                }
            }
        },
        _addBodyRowAfter: function(row_id){
            let self = this;
            for(let i = 0; i < this.body_row_ids.length; i++){
                if(this.body_row_ids[i] === row_id){
                    let new_row_id = self.getRowId('body');
                    this.body_row_ids.splice(i+1, 0, new_row_id);
                    let row_pos = this.head_row_ids.length + i;
                    this.table.splice(row_pos+1, 0, this.generateRow(new_row_id));
                    break;
                }
            }
        },
        _addFootRowAfter: function(row_id){
            for(let i = 0; i < this.foot_row_ids.length; i++){
                if(this.foot_row_ids[i] === row_id){
                    let new_row_id = self.getRowId('foot');
                    this.foot_row_ids.splice(i+1, 0, new_row_id);
                    let row_pos = this.head_row_ids.length + this.body_row_ids.length + i;
                    this.table.splice(row_pos+1, 0, this.generateRow(new_row_id));
                    break;
                }
            }
        },
        initTableData: function(){
            this.generateHeadRow();
            this.generateBodyRow();
            this.generateFootRow();
            this.store.dispatch({
                type: Action.INIT_TABLE_DATA,
                table: this.mapStateToTableData()
            });
        },
        generateHeadRow: function(){
            let self = this;
            this.head_row_ids.forEach(function (row_id) {
                self.table.push(self.generateRow(row_id));
            });
        },
        generateBodyRow: function(){
            let self = this;
            this.body_row_ids.forEach(function (row_id) {
                self.table.push(self.generateRow(row_id));
            });
        },
        generateFootRow: function(){
            let self = this;
            this.foot_row_ids.forEach(function (row_id) {
                self.table.push(self.generateRow(row_id));
            });
        },
        generateRow: function(row_id){
            let self = this;
            let row = [];
            this.column_ids.forEach(function (col_id) {
                let cell_id = self.getCellId();
                row.push({id: cell_id,
                          row_id: row_id,
                          col_id: col_id,
                          name: 'Cell'});
            });
            return row;
        },
        mapTableDataToState: function(){
            let self = this;
            let configs = this.store.getState().configs;
            let table_keys = Object.keys(configs.table || {});
            table_keys.forEach(function(key){
                self[key] = configs.table[key] || self[key];
            });
        },
        mapStateToTableData: function(){
            return {
                column_ids: this.column_ids,
                head_row_ids: this.head_row_ids,
                body_row_ids: this.body_row_ids,
                foot_row_ids: this.foot_row_ids,
                table: this.table
            }
        },
        getCellId: function(){
            let cell_id = 'cell_'+_.random(0, 2147483647);
            while(this.cell_ids.indexOf(cell_id) >= 0){
                cell_id = 'cell_'+_.random(0, 2147483647);
            }
            this.cell_ids.push(cell_id);
            return cell_id;
        },
        getColId: function(){
            let col_id = 'col_'+_.random(0, 2147483647);
            while(this.column_ids.indexOf(col_id) >= 0){
                col_id = 'col_'+_.random(0, 2147483647);
            }
            this.column_ids.push(col_id);
            return col_id;
        },
        getRowId: function(type){
            let row_id = 'cell_'+_.random(0, 2147483647);
            while(this.cell_ids.indexOf(row_id) >= 0){
                row_id = 'cell_'+_.random(0, 2147483647);
            }
            if(type === 'head'){
                this.head_row_ids.push(row_id);
            } else if (type === 'body'){
                this.body_row_ids.push(row_id);
            } else {
                this.foot_row_ids.push(row_id);
            }
            return row_id;
        },
    });

    return TableDesign;

});