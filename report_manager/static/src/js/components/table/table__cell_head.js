odoo.define('report_manager.TableCellHead', function(require){

    var TableCell = require('report_manager.TableCell');
    var InputTableCellHeadName = require('report_manager.InputTableCellHeadName');

    var TableCellHead = TableCell.extend({
        tag: 'th',
        init: function(controller, cell){
            this._super.apply(this, arguments);
        },
        renderElement: function(){
            this._super.apply(this, arguments);
            if(!_.isEqual(this.cell.row_id, 'fix_row') && !_.isEqual(this.cell.col_id, 'fix_col')){
                let tableCellHeadName = new InputTableCellHeadName(this.controller, this.cell);
                tableCellHeadName.appendTo(this.$el);
            }
        }
    });

    return TableCellHead;

});