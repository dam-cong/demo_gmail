odoo.define('report_manager.ReportView', function (require) {
    "use strict";

    var AbstractAction = require('web.AbstractAction');
    var core = require('web.core');
    var rpc = require('web.rpc');

    var _lt = core._lt;

    var BoxFilterView = require('report_manager.BoxFilterView');
    var TabViewTable = require('report_manager.TabViewTable');
    var TabViewPivot = require('report_manager.TabViewPivot');
    var TabViewBirt = require('report_manager.TabViewBirt');
    var TabDesignPerspective = require('report_manager.TabDesignPerspective');
    var ButtonToggleReportView = require('report_manager.ButtonToggleReportView');
    var ButtonSwitchTab = require('report_manager.ButtonSwitchTab');

    var Store = require('report_manager.Store');
    var Action = require('report_manager.Action');


    var ReportView = AbstractAction.extend({
        hasControlPanel: false,
        template: 'RMReportView',
        configs: null,
        init: function (parent, action, options) {
            this._super.apply(this, arguments);
            this.action_manager = parent;
            action['menu_id'] = options.hasOwnProperty('menu_id') ? options['menu_id'] : null;
            this.action = action;
            this.store = Store;
            this.tableData = [];
            this.report_types = [];
            this.display_tab = '';
            this.store.subscribe(this, 'toggleViewContent', [Action.TOGGLE_REPORT_VIEW_CONTENT])
        },
        willStart: function(){
            this._super(arguments);
            var self = this;
            let res_id = this.action.context.id;
            let def = rpc.query({
                model: 'report.config',
                method: 'search_read',
                args: [[['id', '=', res_id]], ['configs']]
            }, {
                shadow: true,
            }).then(function(response){
                if(Array.isArray(response)){
                    response = self.convertDataServerToDataClient(response[0]);
                    self.configs = response.configs;
                    self.getReportTypes();
                    self.store.dispatch({
                        type: Action.UPDATE_STORE,
                        data: response
                    })
                }
            });
            return $.when(this._super.apply(this, arguments), def);
        },
        getReportTypes: function(){
            let self = this;
            let menu_id = this.action.menu_id;
            let menus = this.configs.menus || [];
            if(menus.length === 1){
                self.report_types = menus[0].report_types;
                return
            }
            menus.forEach(function (menu) {
                if(menu.id === menu_id){
                    self.report_types = menu.report_types;
                }
            });
        },
        renderElement: function(){
            this._super.apply(this, arguments);
            this.renderSidebarContent();
            this.renderViewHead();
            this.renderViewToolbar();
            this.renderViewContent();
        },
        toggleViewContent: function(){
            this.$el.find('.rview__view').toggleClass('w-100');
        },
        start: function(){
            this._super.apply(this, arguments);
            this.store.dispatch({
                type: Action.UPDATE_DISPLAY_TAB,
                display_tab: this.display_tab,
            });
        },
        renderSidebarContent: function(){
            let filterEl = this.$el.find('.rview__sidebar');
            let boxFilterView = new BoxFilterView(this, this.configs);
            boxFilterView.appendTo(filterEl);
        },
        renderViewHead: function(){
            let toggleViewEl = this.$el.find('.rview__toggle-view');
            let btnToggle = new ButtonToggleReportView(this);
            btnToggle.appendTo(toggleViewEl);

            let reportNameEl = this.$el.find('.rview__name');
            let res_id = this.action.menu_id;
            rpc.query({
                model: 'ir.ui.menu',
                method: 'search_read',
                args: [[['id', '=', res_id]], ['name']]
            }, {
                shadow: true,
            }).then(function(response){
                try{
                    if(Array.isArray(response)){
                        reportNameEl.html(response[0].name);
                    } else {
                        reportNameEl.html('');
                    }
                } catch (e){
                    reportNameEl.html('');
                }
            });
        },
        renderViewToolbar: function(){
            let self = this;
            this.report_types.forEach(function (report_type) {
                self.renderSwitchTabButton(report_type);
            })
        },
        renderSwitchTabButton: function(report_type){
            if(report_type.value === 'table'){
                this.addViewToolbarItem(new ButtonSwitchTab(this, 'fa fa-table', _lt('Table'), 'tab_view_table'));
                this.display_tab = this.display_tab || 'tab_view_table';
            }
            if (report_type.value === 'pivot'){
                this.addViewToolbarItem(new ButtonSwitchTab(this, 'fa fa-bar-chart', _lt('Pivot'), 'tab_view_pivot'));
                this.display_tab = this.display_tab || 'tab_view_pivot';
            }
            if (report_type.value === 'birt'){
                this.addViewToolbarItem(new ButtonSwitchTab(this, 'fa fa-area-chart', _lt('Birt'), 'tab_view_birt'));
                this.display_tab = this.display_tab || 'tab_view_birt';
            }
            if (report_type.value === 'perspective'){
                this.addViewToolbarItem(new ButtonSwitchTab(this, 'fa fa-area-chart', _lt('Perspective'), 'tab_design_perspective'));
                this.display_tab = this.display_tab || 'tab_view_perspective';
            }
        },
        addViewToolbarItem: function(item){
            let reportToolbarEl = this.$el.find('.rview__toolbar');
            item.appendTo(reportToolbarEl);
        },
        renderViewContent: function(){
            let self = this;
            this.report_types.forEach(function (report_type) {
                self.renderTabViews(report_type);
            })
        },
        renderTabViews: function(report_type){
            if(report_type.value === 'table'){
                this.addViewContentItem(new TabViewTable(this));
            }
            if (report_type.value === 'pivot'){
                this.addViewContentItem(new TabViewPivot(this));
            }
            if (report_type.value === 'birt'){
                this.addViewContentItem(new TabViewBirt(this));
            }
            if (report_type.value === 'perspective'){
                this.addViewContentItem(new TabDesignPerspective(this));
            }
        },
        addViewContentItem: function(item){
            let reportContentEl = this.$el.find('.rview__content');
            item.appendTo(reportContentEl);
        },
        convertDataServerToDataClient: function(data){
            let configs = data.configs;
            configs = configs.replace(/\'/g, '\"')
                .replace(/True/g, 'true')
                .replace(/False/g, 'false')
                .replace(/None/g, 'null');
            try{
                data.configs = JSON.parse(configs);
            } catch (e){
                data.configs = {};
            }
            return data;
        }
    });

    core.action_registry.add('report_view', ReportView);

    return ReportView;
});