odoo.define('report_manager.ListRenderer', function(require) {

    var ListRenderer = require('web.ListRenderer');

    ListRenderer.include({
        _onRowClicked: function (event) {
            if(this.state.model === 'report.config'){
                var id = $(event.currentTarget).data('id');
                this.trigger_up('open_report_config', {id: id});
            } else {
                this._super(event);
            }
        }
    });

    return ListRenderer;

});