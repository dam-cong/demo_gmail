odoo.define('report_manager.ListController', function(require){
"use strict";

    var ListController = require('web.ListController');

    ListController.include({
        custom_events: _.extend({}, ListController.prototype.custom_events, {
            open_report_config: '_onOpenReportConfig',
        }),
        _onCreateRecord: function (event) {
            event.stopPropagation();
            if(this.modelName == 'report.config'){
                return this.trigger_up('switch_view', {view_type: 'rconfig',
                                                       res_id: undefined,
                                                       model: this.modelName});
            }
            this._super(event)
        },
        _onOpenReportConfig: function(event){
            var record = this.model.localData[event.data.id];
            if(record !== undefined){
                return this.trigger_up('switch_view', {view_type: 'rconfig',
                                                       res_id: record.res_id,
                                                       model: this.modelName});
            }
        }
    });

    return ListController;

});