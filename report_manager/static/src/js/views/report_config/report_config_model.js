odoo.define('report_manager.ReportConfigModel', function(require){
"use strict";

    var BasicModel = require('web.BasicModel');
    var Store = require('report_manager.Store');
    var Action = require('report_manager.Action');

    var ReportConfigModel = BasicModel.extend({
        init: function(){
            this.store = Store;
            this._super.apply(this, arguments);
        },
        _fetchRecord: function (record, options) {
            var self = this;
            var defs = [this._super(record, options)];
            return $.when.apply($, defs).then(function () {
                self.prepareStoreData(record.data);
                return record;
            });
        },
        prepareStoreData: function(data){
            data = this.convertDataServerToDataClient(data);
            this.store.dispatch({type: Action.UPDATE_STORE,
                                 data: data});
        },
        convertDataServerToDataClient: function(data){
            let configs = data.configs;
            configs = configs.replace(/\'/g, '\"')
                .replace(/True/g, 'true')
                .replace(/False/g, 'false')
                .replace(/None/g, 'null');
            try{
                data.configs = JSON.parse(configs);
            } catch (e){
                data.configs = {};
            }
            return data;
        }
    });

    return ReportConfigModel;

});