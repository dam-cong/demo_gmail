odoo.define('report_manager.ReportConfigRenderer', function(require){
"use strict";

    var BasicRenderer = require('web.BasicRenderer');

    var ReportConfigRenderer = BasicRenderer.extend({
        init: function(parent, state, params){
            this._super.apply(this, arguments);
        }
    });

    return ReportConfigRenderer;

});