odoo.define('report_manager.ReportConfigController', function(require){
"use strict";

    var BasicController = require('web.BasicController');
    var Action = require('report_manager.Action');
    var ButtonSaveRecord = require('report_manager.ButtonSaveRecord');
    var InputRecordName = require('report_manager.InputRecordName');
    var FragmentSQL = require('report_manager.FragmentSQL');
    var FragmentParam = require('report_manager.FragmentParam');
    var FragmentArgument = require('report_manager.FragmentArgument');
    var FragmentDesign = require('report_manager.FragmentDesign');
    var FragmentMenu = require('report_manager.FragmentMenu');
    var ButtonSwitchFragment = require('report_manager.ButtonSwitchFragment');

    var ReportConfigController = BasicController.extend({
        template: 'RConfigView',
        init: function(parent, model, renderer, params){
            this.store = model.store;
            this._super.apply(this, arguments);
            this.context = parent.userContext;
            this.prepareStoreData();
        },
        prepareDataToSave: function(){
            var data = this.store.getState();
            let recordId = this.handle;
            let options = {notifyChange: true,
                           viewType: 'rconfig'};
            this.model.notifyChanges(recordId, data, options);
        },
        _update: function () {
            var title = this.getTitle();
            this.set('title', title);
            this.prepareStoreData();
            return this._super.apply(this, arguments);
        },
        prepareStoreData: function(){
            let env = this.model.get(this.handle, {env: true});
            if(!env.currentId){
                this.store.dispatch({
                    type: Action.UPDATE_STORE,
                    data: null
                });
            }
        },
        start: function(){
            this.renderRConfigToolbar();
            this.renderRConfigSidebar();
            this.renderRConfigView();
            this._super.apply(this, arguments);
        },
        _pushState: function (state) {
            state = state || {};
            var env = this.model.get(this.handle, {env: true});
            if(env.currentId){
                state.id = env.currentId;
            }
            this._super(state);
            this.controlPanelElements.$buttons.remove();
            this.controlPanelElements.$pager.remove();
            this.controlPanelElements.$sidebar.remove();
            this.controlPanelElements.$switch_buttons.remove();
            this._controlPanel.$el.find('.o_cp_searchview').remove();
            this._controlPanel.$el.find('.o_cp_right').remove();
            this._controlPanel.$el.css({'width': '100%'});
            this._controlPanel.$el.find('.o_control_panel > div').css({'min-height': 'auto'});
        },
        /** Cấu hình toolbar **/
        renderRConfigToolbar: function(){
            this.addRConfigToolbarItem(new InputRecordName(this));
            this.addRConfigToolbarItem(new ButtonSaveRecord(this));
        },
        addRConfigToolbarItem: function(item){
            let toolbarEl = this.$el.find('.rconfig__toolbar');
            item.appendTo(toolbarEl);
        },
        /** Cấu hình sidebar **/
        renderRConfigSidebar: function(){
            this.addRConfigSidebarItem(new ButtonSwitchFragment(this, 'fa fa-database', 'Câu try vấn', 'fragment_sql'));
            this.addRConfigSidebarItem(new ButtonSwitchFragment(this, 'fa fa-cog', 'Tham số', 'fragment_param'));
            this.addRConfigSidebarItem(new ButtonSwitchFragment(this, 'fa fa-cogs', 'Đối số', 'fragment_argument'));
            this.addRConfigSidebarItem(new ButtonSwitchFragment(this, 'fa fa-paint-brush', 'Thiết kế báo cáo', 'fragment_design'));
            this.addRConfigSidebarItem(new ButtonSwitchFragment(this, 'fa fa-bars', 'Cấu hình menu', 'fragment_menu'));
        },
        addRConfigSidebarItem: function(item){
            let sidebarEl = this.$el.find('.rconfig__sidebar');
            item.appendTo(sidebarEl);
        },
        /** Cấu hình view **/
        renderRConfigView: function(){
            this.addRConfigViewItem(new FragmentSQL(this));
            this.addRConfigViewItem(new FragmentParam(this));
            this.addRConfigViewItem(new FragmentArgument(this));
            this.addRConfigViewItem(new FragmentDesign(this));
            this.addRConfigViewItem(new FragmentMenu(this));
        },
        addRConfigViewItem: function(item){
            let sidebarEl = this.$el.find('.rconfig__view');
            item.appendTo(sidebarEl);
        },
        /**
         * @override method from AbstractController
         * @returns {string}
         */
        getTitle: function () {
            return this.model.getName(this.handle);
        },
        setTitle: function (title) {
            $('.breadcrumb-item.active').text(title);
        }
    });

    return ReportConfigController;

});