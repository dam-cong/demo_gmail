odoo.define('report_manager.ReportConfigView', function(require){
"use strict";

    var view_registry = require('web.view_registry');
    var BasicView = require('web.BasicView');
    var core = require('web.core');

    var ReportConfigModel = require('report_manager.ReportConfigModel');
    var ReportConfigRenderer = require('report_manager.ReportConfigRenderer');
    var ReportConfigController = require('report_manager.ReportConfigController');

    var _lt = core._lt;

    var ReportConfigView = BasicView.extend({
        display_name: _lt('Report config'),
        icon: 'fa-file-text',
        config: _.extend({}, BasicView.prototype.config, {
            Renderer: ReportConfigRenderer,
            Controller: ReportConfigController,
            Model: ReportConfigModel,
        }),
        searchable: false,
        groupable: false,
        multi_record: false,
        viewType: 'rconfig',
        init: function (viewInfo, params) {
            this._super.apply(this, arguments);
            this.loadParams.type = 'record';
        },
    });

    view_registry.add('rconfig', ReportConfigView);
});