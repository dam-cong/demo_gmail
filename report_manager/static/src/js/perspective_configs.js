window.addEventListener("WebComponentsReady", function() {
    let viewer = null;

    function initPerspective(data){
        try{
            let perspectiveViewParent = $('perspective-viewer').parent();
            let width = perspectiveViewParent.width();
            let height = perspectiveViewParent.height();
            if(!$('.rview')){
                height -= 60;
            }
            const viewer = document.getElementsByTagName("perspective-viewer")[0];
            viewer.style.width = width + 'px';
            viewer.style.height = height + 'px';
            if(!data || data.length <= 0){
                return;
            }
            viewer.load(data);
        } catch (e) {
            console.error(e);
        }
    }
    function updatePerspective(data){
		if(!data || data.length <= 0){
		    return;
        }
		viewer.update(data);
    }
    document.addEventListener('InitPerspectiveView', function(event){
        initPerspective(event.detail);
    });
    document.addEventListener('UpdatePerspectiveData', function(event){
        updatePerspective(event.detail);
    });
});