# -*- coding: utf-8 -*-
# Created by hoanglv at 5/15/2020

from odoo import http, SUPERUSER_ID
from odoo.http import request
from odoo.tools.misc import xlsxwriter

import json
import io
import re


class ExportDataTableController(http.Controller):

    @http.route(route='/report_manager/datatable/export/xlsx', auth='user', type='http', method='get')
    def export_xlsx(self, data, token):
        jdata = json.loads(data)
        id = jdata.get('id')
        params = jdata.get('params')
        columns = jdata.get('columns')

        config = request.env['report.config'].browse(id)
        file_data_xlsx = self.get_export_xlsx_data(config, params, columns)

        response = request.make_response(
            file_data_xlsx,
            headers=[('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'),
                     ('Content-Disposition', 'attachment; filename="%s.xlsx"' % self.remove_unicode(config.name))],
            cookies={'fileToken': token})
        return response

    def get_export_xlsx_data(self, config, params, columns):
        output = io.BytesIO()
        workbook = xlsxwriter.Workbook(output, options={'in_memory': True})
        # Create a workbook and add a worksheet.
        worksheet = workbook.add_worksheet(config.name)
        # Set title for file
        title_format = workbook.add_format({
            'bold': True,
            'align': 'center',
            'valign': 'vcenter',
            'font_size': '16',
            'border': 1,
        })
        worksheet.merge_range(0, 0, 0, len(columns)-1, config.name.upper(), title_format)
        # Set column title
        col_title = 0
        title_format = workbook.add_format({
            'bold': True,
            'align': 'center',
            'valign': 'vcenter',
            'font_color': '#ffffff',
            'bg_color': '#233c98',
            'border': 1
        })
        for col in columns:
            worksheet.write(1, col_title, col[1].upper(), title_format)
            col_title += 1
        # Get data
        data = config.exec_query(params)
        # Iterate over the data and write it out row by row.
        row = 2
        row_format = workbook.add_format({
            'border': 1
        })
        for item in data:
            col = 0
            for column in columns:
                worksheet.write(row, col, item[column[0]], row_format)
                col += 1
            row += 1

        workbook.close()
        output.seek(0)
        return output.read()

    def remove_unicode(self, text):
        text = text or ''
        text = text.lower()
        text = re.sub(r'à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ', "a", text)
        text = re.sub(r'è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ', "e", text)
        text = re.sub(r'ì|í|ị|ỉ|ĩ', "i", text)
        text = re.sub(r'ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ', "o", text)
        text = re.sub(r'ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ', "u", text)
        text = re.sub(r'ỳ|ý|ỵ|ỷ|ỹ', "y", text)
        text = re.sub(r'đ', "d", text)
        text = re.sub(r'!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\', "-", text)
        text = re.sub(r' + ', "-", text)
        text = text.strip()
        return text
