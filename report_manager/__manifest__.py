# -*- coding: utf-8 -*-
{
    'name': "Report manager",
    'summary': """""",
    'author': "Hoanglv",
    'website': "https://leeviho.com",
    'category': 'Extra Tools',
    'version': '4.0',
    'depends': ['web'],
    'data': [
        'security/ir.model.access.csv',
        'views/report_config.xml',
        'views/templates.xml',
    ],
    'qweb': [
        'static/src/xml/*.xml'
    ]
}