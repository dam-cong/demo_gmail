# -*- coding: utf-8 -*-
# Created by hoanglv at 1/21/2020

from odoo import api, fields, models


class View(models.Model):
    _inherit = 'ir.ui.view'

    type = fields.Selection(selection_add=[('report', "Report"), ('rconfig', "Report Config")])
