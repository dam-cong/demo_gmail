# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.http import request

import json
import base64
from urllib.parse import unquote
from odoo.tools.misc import xlsxwriter
import io


class ReportConfig(models.Model):
    _name = 'report.config'
    _description = 'report config'

    name = fields.Char(string='Name')
    configs = fields.Text(string='Configs')

    @api.model
    def create(self, vals):
        res = super(ReportConfig, self).create(vals)
        res.generate_menu()
        return res


    def write(self, vals):
        vals = self.generate_menu(vals)
        res = super(ReportConfig, self).write(vals)
        return res


    def unlink(self):
        self.delete_menu()
        return super(ReportConfig, self).unlink()

    def generate_menu(self, vals={}):
        action = self.get_client_action()
        if not vals:
            configs = self.convert_config_string_to_dict(self.configs)
        else:
            configs = vals.get('configs')
            if isinstance(configs, str):
                configs = self.convert_config_string_to_dict(configs)
        active_menu_ids = []
        menus = configs['menus'] or []
        for menu in menus:
            group_ids = [item['id'] for item in menu.get('groups', [])]
            if isinstance(menu['id'], int):
                menu_obj = self.env['ir.ui.menu'].browse(menu['id'])
                menu_obj.write({
                    'name': menu.get('name', self.name),
                    'parent_id': menu.get('parent', {}).get('id', False),
                    'action': action,
                    'groups_id': [(6, 0, group_ids)]
                })
                active_menu_ids.append(menu['id'])
            else:
                menu_obj = self.env['ir.ui.menu'].create({
                    'name': menu.get('name', self.name),
                    'parent_id': menu.get('parent', {}).get('id', False),
                    'action': action,
                    'groups_id': [(6, 0, group_ids)]
                })
                menu['id'] = menu_obj.id
                active_menu_ids.append(menu_obj.id)

        menus = self.env['ir.ui.menu'].search([('action', '=', action)])
        for item in menus:
            if item.id not in active_menu_ids:
                item.unlink()

        if vals:
            vals['configs'] = configs
            return vals
        self.configs = configs

    def delete_menu(self):
        context = {'model': 'report.config', 'id': self.id}
        act_client = self.env['ir.actions.client'].search([('tag', '=', 'report_view'),
                                                           ('context', '=', context)])
        if act_client:
            action = 'ir.actions.client,%s' % act_client.id
            menus = self.env['ir.ui.menu'].search([('action', '=', action)])
            menus.unlink()
            act_client.unlink()

    def get_client_action(self):
        context = {'model': 'report.config', 'id': self.id}
        act_client = self.env['ir.actions.client'].search([('tag', '=', 'report_view'),
                                                           ('context', '=', context)])
        if not act_client:
            act_client = self.env['ir.actions.client'].create({
                'name': self.name,
                'tag': 'report_view',
                'context': context
            })
        return 'ir.actions.client,%s' % act_client.id

    @staticmethod
    def convert_config_string_to_dict(str_configs):
        str_configs = str_configs.replace('\'', '\"') \
            .replace('None', 'null') \
            .replace('True', 'true') \
            .replace('False', 'false')
        return json.loads(str_configs)


    def exec_query(self, params, opts=[]):
        try:
            str_configs = self.configs.replace('\'', '\"') \
                .replace('None', 'null') \
                .replace('True', 'true') \
                .replace('False', 'false')
            configs = json.loads(str_configs)
            sql_bytes = base64.b64decode(configs['sql'])
            sql = unquote(sql_bytes.decode('utf-8'))
            if len(opts) == 4 and opts[3]:
                sql, max_limit = self.prepare_sql_offset_limit(sql)
                opts[3] = max_limit if max_limit < opts[3] or opts[3] == -1 else opts[3]
                params += [opts[2], opts[3]]
            sql = sql.replace('?', '%s')
            for i in range(0, len(params)):
                if isinstance(params[i], list):
                    params[i] = tuple(params[i])
            self._cr.execute(sql, tuple(params))
            raw_data = self._cr.dictfetchall()
            return self.cook_raw_data(raw_data, configs)
        except Exception as e:
            return []

    def cook_raw_data(self, raw_data, configs):
        cook_data = []
        if not raw_data or len(raw_data) <= 0:
            return cook_data
        if 'sql_columns' not in configs:
            return cook_data

        columns = self.get_column_by_groups(configs)
        for item in raw_data:
            cook_item = {}
            for k, v in item.items():
                v = '***' if k in columns and not columns[k] else v
                cook_item.update({k: v})
            cook_data.append(cook_item)
        return cook_data

    def get_column_by_groups(self, configs):
        columns = {}
        sql_columns = configs['sql_columns']
        for sql_column in sql_columns:
            groups = 'groups' in sql_column and sql_column['groups'] or []
            gids = []
            for group in groups:
                gids.append(group['id'])
            if len(gids) > 0:
                columns.update({sql_column['key']: self.has_groups(gids)})
        return columns

    def has_groups(self, gids):
        if isinstance(gids, int):
            gids = [gids]
        if isinstance(gids, tuple):
            gids = list(gids)
        if isinstance(gids, str) or not gids or len(gids) < 1:
            return False
        query = '''SELECT 1 FROM res_groups_users_rel WHERE uid = %s and gid in %s'''
        self._cr.execute(query, (self.env.uid, tuple(gids)))
        row = self._cr.dictfetchone()
        if row:
            return True
        return False

    @staticmethod
    def prepare_sql_offset_limit(sql):
        sqlArr = sql.split(' ')
        max_limit = 1000000000
        sqlArr.insert(1, 'ROW_NUMBER () OVER() row_number,')
        sqlArr.insert(1, 'COUNT (*) OVER() as records_total,')
        if sqlArr[-2].upper() == 'LIMIT':
            max_limit = int(sqlArr[-1])
            sqlArr = sqlArr[0: len(sqlArr)-2]
        if sqlArr[-2].upper() == 'OFFSET':
            sqlArr = sqlArr[0: len(sqlArr)-2]
        sqlArr.append('OFFSET')
        sqlArr.append('?')
        sqlArr.append('LIMIT')
        sqlArr.append('?')
        sql = ' '.join(sqlArr)
        return sql, max_limit
