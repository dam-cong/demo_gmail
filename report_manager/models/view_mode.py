# -*- coding: utf-8 -*-
# Created by hoanglv at 1/21/2020

from odoo import api, fields, models


class ActWindowView(models.Model):
    _inherit = 'ir.actions.act_window.view'

    view_mode = fields.Selection(selection_add=[('report', "Report"), ('rconfig', "Report Config")])
