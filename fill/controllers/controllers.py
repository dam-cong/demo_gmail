# -*- coding: utf-8 -*-
from odoo import http
from odoo.http import request
from odoo.addons.web.controllers.main import serialize_exception, content_disposition
from io import BytesIO, StringIO
import os
import zipfile
import logging

_logger = logging.getLogger(__name__)


class Fill(http.Controller):
    @http.route('/fill/<string:id>', type='http', auth="public")
    def download(self, id=False, **kwargs):
        reponds = BytesIO()
        archive = zipfile.ZipFile(reponds, 'w', zipfile.ZIP_DEFLATED)
        document = request.env['thongtin.thongtin'].browse(int(id))
        invoice = request.env['thongtin.thongtin']

        # finalDoc = invoice.download_thongtin(document)
        # demo = u'fill'
        # archive.write(finalDoc.name, r'%s\(1).docx' % demo)
        # os.unlink(finalDoc.name)

        # archive.close()
        # reponds.flush()
        # ret_zip = reponds.getvalue()
        # reponds.close()

        # finalDoc.name = 'FILL.zip'
        # return request.make_response(ret_zip,
        #                              [('Content-Type', 'application/zip'),
        #                               ('Content-Disposition', content_disposition(finalDoc.name))])

        # reponds.close()
        return request.make_response(invoice.download_thongtin(document),
                                     headers=[('Content-Disposition',
                                               content_disposition('DSLD.docx')),
                                              ('Content-Type',
                                               'application/vnd.openxmlformats-officedocument.wordprocessingml.document')])
