# -*- coding: utf-8 -*-
{
    'name': "fill",
    'depends': ['base'],
    'data': [
        'views/dulieu.xml',
        'views/thongtin.xml',
        'views/views.xml',
        'views/templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}