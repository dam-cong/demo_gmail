# -*- coding: utf-8 -*-
from odoo import http
from odoo import models, fields, api
import logging
from tempfile import TemporaryFile, NamedTemporaryFile
import zipfile
from odoo.http import request
from odoo.addons.web.controllers.main import serialize_exception, content_disposition

_logger = logging.getLogger(__name__)
import logging

_logger = logging.getLogger(__name__)


class DuLieu(models.Model):
    _name = 'dulieu.dulieu'
    _rec_name = 'name'

    name = fields.Char('Tên tài liệu')
    file_name = fields.Char('Tên')
    data = fields.Binary()
