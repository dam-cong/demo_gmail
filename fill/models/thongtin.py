# -*- coding: utf-8 -*-
import codecs
import datetime
import logging
from builtins import print
from io import BytesIO
from tempfile import NamedTemporaryFile
from docxtpl import DocxTemplate, Listing
from odoo import models, fields, api
import win32com.client
from pywintypes import com_error
from pathlib import Path

_logger = logging.getLogger(__name__)

class ThongTin(models.Model):
    _name = 'thongtin.thongtin'
    _rec_name = 'name'

    name = fields.Char(string='Họ và tên')
    address = fields.Char(string='Địa chỉ')
    birthday = fields.Date(string='Ngày sinh')

    @api.multi
    def download_button(self):
        return {
            'type': 'ir.actions.act_url',
            'url': '/fill/%s' % (self.id),
            'target': 'self', }

    @api.multi
    def download_thongtin(self, document):
        docs = self.env['dulieu.dulieu'].search([('name', '=', "file_1")], limit=1)
        if docs:
            stream = BytesIO(codecs.decode(docs[0].data, "base64"))
            docdata = DocxTemplate(stream)
            context = {}

            context['hhjp_0193'] = document.name
            context['hhjp_0010'] = document.address
            context['hhjp_0014'] = document.birthday
            context['hhjp_0014_1'] = ''
            context['hhjp_0097'] = ''
            context['hhjp_0103_1'] = ''
            context['hhjp_0103'] = ''

            docdata.render(context)
            tempFile = NamedTemporaryFile(delete=False)
            docdata.save(tempFile)
            tempFile.seek(0)
            # tempFile.flush()
            # tempFile.close()
            return tempFile
        return None

    @api.multi
    def download_excel(self, document):
        docs = self.env['dulieu.dulieu'].search([('name', '=', "file_2")], limit=1)
        if docs:
            REPLACE_TXTS = {
                '{{hhjp_0193}}': 'こんにちは。',
                '{{hhjp_0010}}': 'おはようございます。'
            }
            nowdir = Path(__file__).absolute().parent
            excel = win32com.client.Dispatch('Excel.Application')
            excel.Visible = False

            wb = excel.Workbooks.Add(docs[0].data)
            sheet = wb.WorkSheets(1)
            sheet.Activate()

            # Replace text in cell
            rg = sheet.Range(sheet.usedRange.Address)
            for search_txt, replace_Txt in REPLACE_TXTS.items():
                rg.Replace(search_txt, replace_Txt)

            # save as
            wb.SaveAs(str('new.xlsx'))

            # docdata.render(context)
            # tempFile = NamedTemporaryFile(delete=False)


            # docdata.save(tempFile)
            wb.flush()
            wb.close()
            return wb
        return None