# -*- coding: utf-8 -*-

from odoo import models, fields, api


class xinghiep(models.Model):
    _name = 'xinghiep.xinghiep'

    name_jp_enterprise = fields.Char("Tên xí nghiệp - Tiếng Hán", required=True)
    name_romaji_enterprise = fields.Char("Tên xí nghiệp - Tiếng Romaji", required=True)
    address_jp_enterprise = fields.Char("Địa chỉ làm việc - Tiếng Hán")
    address_romoji_enterprise = fields.Char("Địa chỉ làm việc - Tiếng Romaji")
    phone_number_enterprise = fields.Char("Số điện thoại")
    fax_number_enterprise = fields.Char("Số fax")
    name_of_responsive_jp_enterprise = fields.Char("Tên người đại diện - Tiếng Anh")
    name_of_responsive_en_enterprise = fields.Char("Tên người đại diện - Tiếng Nhật")

    # note = fields.Char("Select the enterprise")

    @api.multi
    def name_get(self):
        res = []
        for record in self:
            res.append((record.id, record.name_romaji_enterprise))

        return res
